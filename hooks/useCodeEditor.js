
// 'use client'
import { useCallback, useEffect, useRef, useState } from 'react'
import hljs from 'highlight.js/lib/core'


import { CodeJar } from '@lib/codejar/codejar'
import { getCaretOffset, setCurrentCursorPosition } from '@lib/helpers/caret'
import { getParser } from '@lib/helpers/parsers'

// type CodejarOptions = {
//   tab: string
//   indentOn: RegExp
//   spellcheck: boolean
//   catchTab: boolean
//   preserveIdent: boolean
//   history: boolean
//   window: Window
//   addClosing: boolean
// }

const useCodeEditor = ({
  language,
  codeJarOptions,
  code,
  onUpdate,
}) => {
  const jar = useRef(null)
  const [editorRef, setEditorRef] = useState(null)
  const [cursorOffset, setCursorOffset_] = useState(0)
  const cursorOffsetRef = useRef(0)
  const setCursorOffset = n => {
    cursorOffsetRef.current = n
    setCursorOffset_(n)
  }

  useEffect(() => {
    let parser = getParser(language)
    if (parser) {
      hljs.registerLanguage(language, parser)
    }
  }, [language])

  const highlight = useCallback(htmlElem => {
    const code = htmlElem.textContent || ''
    // const highlightedCode = code
    const highlightedCode = hljs.highlight(code, {language}).value
    htmlElem.innerHTML = highlightedCode
  }, [language])

  const setEditorRefCb = useCallback(node => {
    setEditorRef(node)
  }, [])

  const handleClick = useCallback(() => {
    if (!editorRef) return
    setCursorOffset(getCaretOffset(editorRef))
  }, [editorRef])

  useEffect(() => {
    if (!editorRef) return
    jar.current = CodeJar(editorRef, highlight, {
      addClosing:false,
      spellcheck: true,
      ...codeJarOptions
    })
    jar.current.onUpdate(txt => {
      if (!editorRef) return
      setCursorOffset(getCaretOffset(editorRef))
      if (typeof onUpdate === 'function') {
        onUpdate(txt, editorRef)
      }
    })
    return () => jar.current?.destroy()
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [editorRef, highlight])

  useEffect(() => {
    if (!jar.current || !editorRef) return
    jar.current.updateCode(code, editorRef)
    setCurrentCursorPosition(editorRef, cursorOffset)
     // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [code, editorRef])

  useEffect(() => {
    if (!jar.current || !codeJarOptions) return
    jar.current.updateOptions(codeJarOptions)
  }, [codeJarOptions])

  return { setEditorRefCb, editorRef, handleClick, cursorOffsetRef }
}

export default useCodeEditor
