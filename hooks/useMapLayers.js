import { useEffect, useCallback, useRef, useState } from 'react'
import L from 'leaflet'
import { 
  setGeojsonFeature,
  isPointInsidePolygon, 
  isPointInsidePolyline
} from '@lib/helpers/leaflet'

const useMapLayers = ({
  mapRef,
  geojson
}) => {
  const mapLayers = useRef(L.geoJSON([]))
  const [overedMarker, setOveredMarker] = useState(null)
  const [mouseOverLayers, setMouseOverLayers] = useState([])
  const nbHovered = useRef(0)

  useEffect(() => {
    mapLayers.current.on('layeradd', e => {
      // console.log('Add layer', e)
      const layer = e.layer
      setGeojsonFeature(layer)
      if (layer instanceof L.Marker) {
        layer.on('click', e => mapRef.current.fire('click'))
        layer.on('mouseover', e => setOveredMarker(e.target))
        layer.on('mouseout', e => setOveredMarker())
        if (layer.feature.properties.name) {
          layer.bindTooltip(layer.feature.properties.name, {direction:'top'})
        }
      }
    })
  }, [])

  useEffect(() => {
    mapLayers.current.addData(geojson)
    setMouseOverLayers([])
    return () => mapLayers.current.clearLayers()
  }, [geojson])


  const onMapMouseMove = useCallback(e => {
    const layers = []
    // Todo: ignore hidden Layer with a display none class ?
    mapLayers.current.eachLayer(l => {
      if (l instanceof L.Polygon) {
        if (isPointInsidePolygon(e.latlng, l)) 
          layers.push(l)
      }
      else if (l instanceof L.Polyline) {
        if (isPointInsidePolyline(mapRef.current, e.latlng, l)) {
          layers.push(l)
        }
      }
    })
    setMouseOverLayers([...layers])
  }, [])

  useEffect(() => {
    nbHovered.current = 0
    if (overedMarker) {
      nbHovered.current += 1
    }
    nbHovered.current += mouseOverLayers?.length
  }, [overedMarker, mouseOverLayers])
  

  return {
    mapLayers,
    overedMarker,
    mouseOverLayers,
    onMapMouseMove,
    nbHovered
  }
}

export default useMapLayers