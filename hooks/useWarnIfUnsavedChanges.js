import Router from 'next/router'
import { useEffect } from 'react'

const useWarnIfUnsavedChanges = (unsavedChanges, confirmCb) => {
  useEffect(() => {
    if (unsavedChanges) {
      const routeChangeStart = () => {
        const ok = confirmCb()
        if (!ok) {
          Router.events.emit('routeChangeError')
          throw 'Abort route change. Please ignore this error.'
        }
      }
      Router.events.on('routeChangeStart', routeChangeStart)

      return () => {
        Router.events.off('routeChangeStart', routeChangeStart)
      }
    }
  }, [unsavedChanges])
}

export default useWarnIfUnsavedChanges