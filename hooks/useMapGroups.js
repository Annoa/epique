import { useCallback, useState } from 'react'

const useMapGroups = ({
  groups
}) => {
  const [visibleGroups, setVisibleGroups] = useState({})

  const onChangeVisibility = useCallback((layerId, visible) => {
    if (layerId === 'ALL' && visible) {
      setVisibleGroups({...Object.fromEntries(groups.map(g => [g.id, visible]))})
    }
    else if (layerId === 'ALL' && !visible) {
      setVisibleGroups({})
    }
    else if (layerId) {
      setVisibleGroups(prev => ({...prev, [layerId]: visible}))
    } else {
      setVisibleGroups(prev => ({...prev}))
    }
  }, [])

  return {
    visibleGroups,
    onChangeVisibility
  }
}

export default useMapGroups