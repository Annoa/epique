import { useRef, useState } from 'react'

const DEBUG = false

const useLongPress = () => {
  const [action, setAction] = useState()
  const [event, setEvent] = useState()

  const timerRef = useRef()
  const isLongPress = useRef()

  const startPressTimer = e => {
    isLongPress.current = false
    timerRef.current = setTimeout(() => {
      isLongPress.current = true
      setAction('longpress')
      setEvent(e)
    }, 500)
  }

  const handleOnClick = e => {
    DEBUG && console.log('handleOnClick')
    if ( isLongPress.current ) {
      DEBUG && console.log('Is long press - not continuing.')
      return
    }
    setAction('click')
    setEvent(e)
    e.stopPropagation()
  }

  const handleOnMouseDown = e => {
    DEBUG && console.log('handleOnMouseDown')
    startPressTimer(e)
    e.stopPropagation()
  }

  const handleOnMouseUp = e => {
    DEBUG && console.log('handleOnMouseUp')
    clearTimeout(timerRef.current)
    e.stopPropagation()
  }

  const handleOnTouchStart = e => {
    DEBUG && console.log('handleOnTouchStart')
    startPressTimer(e)
    e.stopPropagation()
  }

  const handleOnTouchEnd = e => {
    if ( action === 'longpress' ) return
    DEBUG && console.log('handleOnTouchEnd')
    clearTimeout(timerRef.current)
  }

  return {
    action,
    event,
    handlers: {
      onClick: handleOnClick,
      onMouseDown: handleOnMouseDown,
      onMouseUp: handleOnMouseUp,
      onTouchStart: handleOnTouchStart,
      onTouchEnd: handleOnTouchEnd
    }
  }
}

export default useLongPress