import { useEffect, useRef } from 'react'

const useKeySave = ({
  elemRef,
  save, 
  value, 
  dataPath,
  close
}) => {
  const currentValue = useRef()

  useEffect(() => {
    currentValue.current = value
  }, [value])

  useEffect(() => {
    const saveListener = e => {
      if (typeof save !== 'function') {
        return
      }
      const isCtrlS = ((e.ctrlKey || e.metaKey) && e.key === 's')
      if (isCtrlS) {
        e.preventDefault()
      }
      const isChildren = elemRef.current?.contains(document.activeElement)
      if (isCtrlS && isChildren) {
        const params = dataPath ? {
          path: dataPath,
          value: currentValue.current
        } : currentValue.current
        save(params)
      }
    }
    document.addEventListener('keydown', saveListener)
    return () => document.removeEventListener('keydown', saveListener)
  }, [dataPath, elemRef, save])

  useEffect(() => {
    const closeListener = e => {
      if (typeof close !== 'function') {
        return
      }
      const isEscape = ['Escape','Esc'].includes(e.key)
      const isChildren = elemRef.current?.contains(document.activeElement)
      if (isEscape && isChildren) {
        close()
      }
    }
    document.addEventListener('keydown', closeListener)
    return () => document.removeEventListener('keydown', closeListener)
  }, [elemRef, close])

}

export default useKeySave