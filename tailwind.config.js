/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      boxShadow: {
        sm: '0 1px 2px 0 rgba(0, 0, 0, 0.1)',
        DEFAULT: '0 1px 3px 0 rgba(0, 0, 0, 0.2), 0 1px 2px 0 rgba(0, 0, 0, 0.1)',
        md: '0 4px 6px -1px rgba(0, 0, 0, 0.2), 0 2px 4px -1px rgba(0, 0, 0, 0.1)',
        lg: '0 10px 15px -3px rgba(0, 0, 0, 0.2), 0 4px 6px -2px rgba(0, 0, 0, 0.1)',
        xl: '0 20px 25px -5px rgba(0, 0, 0, 0.2), 0 10px 10px -5px rgba(0, 0, 0, 0.1)',
        '2xl': '0 25px 50px -12px rgba(0, 0, 0, 0.5)',
       '3xl': '0 35px 60px -15px rgba(0, 0, 0, 0.3)',
        inner: 'inset 0 2px 4px 0 rgba(0, 0, 0, 0.06)',
        none: 'none',
      },
      colors: {
        parchment: {
          50: '#EEE5CE',
          100: '#EBE0C6',
          200: '#E8DCBD',
          300: '#E5D7B5',
          400: '#E2D2AD',
          500: '#DECEA4',
          600: '#DBC99C',
          700: '#D8C494',
          800: '#D5C08B',
          900: '#D2BB83',
        },
        ink: {
          50: '#EEE5CE',
          100: '#DDCEB9',
          200: '#CDB7A3',
          300: '#BCA18E',
          400: '#AB8A78',
          500: '#9B7363',
          600: '#8A5C4D',
          700: '#794638',
          800: '#692F22',
          900: '#58180D',
        },
      }
    },
  },
  plugins: [],
  important: true
}