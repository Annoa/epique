# Epique

[Website](https://jdr-epique.herokuapp.com/)

## Run development server

Required: node 18.x

```bash
npm install
git clone epique-data.git data
npm run dev
```

Open [http://localhost:8080](http://localhost:8080) 

## Stack

- Next 13
- React 18
- Tailwind CSS 3
- Radix UI 
- Leaflet
- Highlight.js 
- Font Awesome
- Showdown
- AWS SDK
- Socket.IO
- JsonWebToken

## Roadmap

- Migrate from heroku to custom server using Caddy
- Redeploy discord bots