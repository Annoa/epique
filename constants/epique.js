export const RESSOURCES = {
  'endurance': 'Endurance',
  'volonte': 'Volonté',
  'mana': 'Mana',
  'vitae': 'Vitae',
  'extia': 'Extia',
  'ichor': 'Ichor',
  'neant': 'Néant',
  'aether': 'Aether',
  'chaos': 'Chaos',
  'souffle': 'Souffle',
  'geis': 'Geis',
  'gnose': 'Gnose',
  'essence': 'Essence',
  'transcendance': 'Transcendance',
} 

export const PRIMARY_RESSOURCES = [
  'endurance', 'volonte', 'mana'
]

export const HIGH_ADVANCES_RESSOURCES = [
  'vitae', 'extia', 'ichor'
]

export const LOW_ADVANCES_RESSOURCES = [
  'essence', 'souffle', 'geis', 'gnose'
]

export const EQUIPMENT_RESSOURCES = {
  'endurance': 'Endurance',
  'mana': 'Mana',
  'volonte': 'Volonté',
  'charge': 'Charge'
}

export const EQUIPMENT_TYPES = {
  'charme': 'Charme',
  'periapte': 'Périapte',
  'artefact': 'Artefact',
  'foci': 'Foci'
}

export const EMANATION = {
  0: 'Inexistant',
  3: 'Médiocre',
  8: 'Très faible',
  14: 'Faible',
  25: 'Commune',
  40: 'Modérée',
  60: 'Supérieure',
  80: 'Assez puissante',
  100: 'Puissante',
  140: 'Très puissante',
  200: 'Surpuissante',
  300: 'Suprême',
  400: 'Divine',
  999: 'Cosmique',
}

export const CLUTTER_THRESHOLD = {
  0.4: "-",
  0.6: "-10% de VD",
  0.8: "-20% de VD, -10% d'Efficacité",
  0.9: "-30% de VD, -15% d'Efficacité",
  1: "-40% de VD, -20% d'Efficacité",
}

export const OPERATORS = {
  '-': 'Consomme',
  '+': 'Génère'
}

export const RECOVERIES = [
  '20 minutes', '1 heure', '6 heures', '1 jour', '5 jours'
]

export const xpRecyclingBySource = {
  1: ['apprentissage', 'excellence-1', 'illumination'],
  2: ['cration-1']
}

export const CAPACITY_TYPE_EXECUTION = "Exécution"
export const CAPACITY_TYPE_DECLENCHMENT = "Déclenchement"
export const CAPACITY_TYPE_PERMANENCE = "Permanence"

export const CAPACITY_TYPES = [
  "Exécution",
  "Déclenchement",
  "Permanence"
]