import { faBoltLightning, faFeatherPointed, faGear, faUniversalAccess, faWandMagicSparkles } from "@fortawesome/free-solid-svg-icons"

export const RULES = {
  archetypes: 'Archétypes',
  abilities: 'Compétences',
  feats: 'Dons',
  equipments: 'Equipements',
  capacities: 'Racines',
  xp: 'Xp',
  species: 'Espèces'
}

export const SHEET_TITLES = {
  'txt-start': 'Général',
  'health': 'Moniteur de santé',
  'xp': 'Xp',
  'ressources': 'Ressources',
  'recoveries': 'Récupérations & entretiens',
  'roots': 'Racines',
  'abilities': 'Compétences',
  'feats': 'Dons',
  'traits': 'Traits uniques',
  'equipments': 'Équipements',
  'capacities': 'Capacités',
  'inventory': 'Inventaire',
  'notes': 'Notes',
  'archetypes': 'Archétypes'
}

export const FIRST_TAB = [
  'txt-start', 'health', 'xp', 'ressources', 'recoveries',
  'roots', 'abilities', 'feats', 'traits',
]

export const SHEET_TABS = [
  {path: '', name:'Fiche', icon:faUniversalAccess, color:'rgb(168, 63, 55)'},
  {path: '/capacities', name:'Capa.', icon:faBoltLightning, color:'rgb(106, 83, 125)'},
  {path: '/equipments', name:'Équip.', icon:faWandMagicSparkles, color:'rgb(68, 113, 73)'},
  {path: '/notes', name:'Notes', icon:faFeatherPointed, color:'rgb(78, 81, 153)'},
  {path: '/options', name:'Opt.', icon:faGear, color:'rgb(215, 151, 34)'},
]

export const OPTIONS_SECTION = [
  {id: 'limits', name: 'Rangs des limites'},
  {id: 'recoveries', name: 'Rangs des récupérations'},
  {id: 'factors', name: 'Facteurs'},
  {id: 'efficiencies', name: 'Bonus d\'efficience'},
]