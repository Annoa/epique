#!/bin/bash

SCRIPT_DIR=$(pwd)

SSH_FILE=~/.ssh/id_rsa
if [ -f "$SSH_FILE" ]; then
  echo "$SSH_FILE exists."
else 
  echo "~/.ssh/id_rsa does not exist."
  mkdir ~/.ssh 
  echo "$GIT_PRIVATE_KEY" >> ~/.ssh/id_rsa
  echo "Host gitlab.com
  StrictHostKeyChecking no" >> ~/.ssh/config
fi

cd $SCRIPT_DIR/server/data
git init
GIT_EMAIL=$(git config user.email)
if [ -z "$GIT_EMAIL" ]; then
  echo "git config user.email does not exist."
  git config --local user.email "aramile@hotmail.fr"
  git config --local user.name "EpiqueBot"
else 
  echo "$GIT_EMAIL exists."
fi

echo "git remote add"
git remote add origin git@gitlab.com:Annoa/epique-data.git
echo "git remote add"
git fetch
echo "git add ."
git add .
echo "git commit"
git commit -m "Backup"
echo "git checkout main"
git checkout main
echo "git push origin main"
git push origin main

# rm -r $SCRIPT_DIR/server/data/*