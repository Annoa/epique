var fs = require("fs")

const PATH = './src/assets/map'

const toComponentName = (str) => {
  let name = str.replace(/([-_][a-z])/g, (group) => group.toUpperCase()
    .replace('-', '')
    .replace('_', '')
  )
  return name.charAt(0).toUpperCase() + name.slice(1)
}

const main = () => {
  let content = ''
  fs.readdirSync(PATH+'/markers-svg/').forEach(file => {
    let splitedFile = file.split('.') 
    let ext = splitedFile.pop()
    let name = splitedFile.join()

    if (ext !== 'svg')
      return

    let svgContent = fs.readFileSync(PATH+'/markers-svg/'+file, 'utf8')
    svgContent = svgContent.split('svg-inline--fa').join('')
    svgContent = svgContent.split('fa-w-16').join('')
    
    fs.writeFileSync(PATH+'/markers-svg/'+file, svgContent)
      
    content += `export {ReactComponent as ${toComponentName(name)} } from './markers-svg/${file}'\n`
    
    fs.writeFileSync(`${PATH}/markers.js`, content)
  
  })

}

main()