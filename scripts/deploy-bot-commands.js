// url d'invitation avec les permissions :
// https://discord.com/api/oauth2/authorize?client_id=845973701232558091&permissions=380104608832&scope=applications.commands%20bot

require('dotenv').config()

const fs = require('fs')
const path = require('path')
const { REST } = require('@discordjs/rest')
const { Routes } = require('discord-api-types/v9')
const { getFile } = require('../server/helpers/api')

// const { clientId, guildId, token } = require('./config.json')

const main = async () => {
	const commands = []
	const commandsPath = path.join(__dirname, '../server/botCommands')
	const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'))
	const usersFile = await getFile({path:'users.json'})
  const users = JSON.parse(usersFile.body)

	for (const file of commandFiles) {
		const filePath = path.join(commandsPath, file)
		const command = require(filePath)
		const data = command.computeData({users})
		commands.push(data.toJSON())
	}
	
	const rest = new REST({ version: '9' }).setToken(process.env.BOT_TOKEN)
	// guildId Serveur de Elyse = 845984879904948244
	rest.put(Routes.applicationGuildCommands('845973701232558091', '845984879904948244'), { body: commands })
		.then(() => console.log('Serveur de Elyse: Successfully registered application commands.'))
		.catch('Erreur serveur de Elyse', console.error)
	
	// guildId Epique = 817936799027691620
	rest.put(Routes.applicationGuildCommands('845973701232558091', '817936799027691620'), { body: commands })
		.then(() => console.log('Serveur Epique: Successfully registered application commands.'))
		.catch('Erreur serveur Epique', console.error)
}

main()