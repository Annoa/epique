require('dotenv').config()

const fs = require('fs')
const readline = require('readline')

const LOCAL_DATA_FOLDER = __dirname+'/../server/data/'

const getIdsFromFile = name => {
  const feats = JSON.parse(fs.readFileSync(LOCAL_DATA_FOLDER+name+'.json'))
  return feats[name].map(f => [normalize(f.name), f.id])
}

const normalize = str => {
  return str.replace(/[^a-zA-Z ]/g, "").trim()
} 

function check(obj1, obj2) {
  const keys1 = Object.keys(obj1), keys2 = Object.keys(obj2);
  for (let key of keys2) {
    if (!obj1[key]) {
      console.log('[objsEqual] key not in obj1', key)
    }
    if (obj1[key] !== obj2[key]) {
      console.log('[objsEqual] obj1[key] !== obj2[key]', key, obj1[key], obj2[key])
    }
  }
  for (let key of keys1) {
    if (!obj2[key]) {
      console.log('[objsEqual] key not in obj2', key)
    }
    if (obj1[key] !== obj2[key]) {
      console.log('[objsEqual] obj1[key] !== obj2[key]', key, obj1[key], obj2[key])
    }
  }
  return true
}


async function main() {
  const fileName = fs.readdirSync(LOCAL_DATA_FOLDER)
    .filter(f => f.startsWith('livre-de-regles'))
    .sort().pop()
  // const version = (fileName.split('.md')[0]).split('_').pop()

  const ids = []
  ids.push(getIdsFromFile('feats'))
  ids.push(getIdsFromFile('abilities'))
  ids.push(getIdsFromFile('capacities'))
  ids.push(getIdsFromFile('equipments'))
  
  const idsByName = Object.fromEntries(ids.flat())
  // fs.writeFileSync(LOCAL_DATA_FOLDER+'idsByName.json', JSON.stringify(idsByName))

  const regex = /(?<hd>#+)\s?(?<name>[^{}]+)(?:\{(?<id>.+)})?/
  const rl = readline.createInterface({
    input: fs.createReadStream(LOCAL_DATA_FOLDER+fileName),
    crlfDelay: Infinity
  })
  let txt = [] 
  let idsInTxt = {}
  for await (const line of rl) {
    let line_ = line
    if (line.startsWith("###")) {
      // match id, if id not in list warning, else do nothing
      const [, hd, name, id] = line.match(regex)
      let normName = normalize(name)
      if (id && !idsByName[normName]) {
        line_ = hd+' '+name
      }
      if (!id && idsByName[normName]) {
        line_ = hd+' '+name+' {'+idsByName[normName]+'}'
      }
      if (idsByName[normName]) {
        idsInTxt[normName] = idsByName[normName]
      }
    }
    txt.push(line_)
  }
  fs.writeFileSync(LOCAL_DATA_FOLDER+'livre-test.md', txt.join('\n'))
  check(idsByName, idsInTxt)
}

main()