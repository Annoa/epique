require('dotenv').config()

const showdown  = require('showdown')
const fs = require('fs')
const readline = require('readline')
const jsdom = require("jsdom")

const importCapacities = require('./imports/importCapacities')
const importEquipments = require('./imports/importEquipments')
const importFeats = require('./imports/importFeats')
const importAbilities = require('./imports/importAbilities')
const importArchetypes = require('./imports/importArchetypes')
const importSpecies = require('./imports/importSpecies')
const importXp = require('./imports/importXp')
const importsHelper = require('./imports/utils')

const LOCAL_DATA_FOLDER = __dirname+'/../data/'

// conf books
global.IMP_BASE = true
global.IMP_ADVANCED = true
// conf dataset
const IMP_CAPACITIES = true
const IMP_EQUIPMENTS = false
const IMP_SPECIES = true
const IMP_FEATS = true
const IMP_ABILITIES = true
const IMP_XP = true
const IMP_ARCHETYPES = true

const converter =  new showdown.Converter({
  tables: true,
  disableForced4SpacesIndentedSublists: true,
  simpleLineBreaks: false,
  customizedHeaderId: true
})

const version = "1.9.0"

const getBookHtml = async fileName => {
  const rl = readline.createInterface({
    input: fs.createReadStream(LOCAL_DATA_FOLDER+fileName),
    crlfDelay: Infinity
  })
  let txt = []
  let insideIgnored = false 
  let insideDiv = 0
  for await (const line of rl) {
    let line_ = line
    // insideIgnored
    if (
      line.startsWith("{{note") || 
      line.startsWith("{{toc") ||
      line.startsWith("{{wide")
    ) {
      insideIgnored = true
      continue
    } 
    if (insideIgnored && line.startsWith("}}")) {
      insideIgnored = false
      continue
    }
    if (insideIgnored) {
      continue
    }
    // else column-count:
    if (
      line.startsWith('{{column-count:2') ||
      line.startsWith('{{column-count:3') ||
      line.startsWith('{{descriptive')
    ) {
      insideDiv +=1
      if (line.startsWith('{{column-count:2')) {
        line_ = '<div style="columns:2">'
      } else if (line.startsWith('{{column-count:3')) {
        line_ = '<div style="columns:3">'
      } else  {
        line_ = '<div class="descriptive">'
      } 
    } else if (insideDiv && line.startsWith("}}")) {
      line_ = '</div>'
      insideDiv -= 1
    // else
    } else if (
      line.startsWith("{{") ||
      line.startsWith("}}") ||
      line.startsWith("\\") ||
      line.startsWith("___")
    ) {
      continue
    } else if (line.startsWith('#')) {
      line_ = line.replace(/<!--([^(-->)]*)-->/, '{$1}')
    }
    line_ = line_.replace('::', '<br/>')
    txt.push(line_)
  }
  txt = txt.join("\n")

  let html  = converter.makeHtml(txt)
  const window = (new jsdom.JSDOM(html)).window
  const doc = window.document

  // const tagsBG = {}
  // doc.querySelectorAll('inline').forEach(e => { 
  //   tagsBG[e.innerHTML] = window.getComputedStyle(e).backgroundColor
  // })
  // fs.writeFileSync(LOCAL_DATA_FOLDER+'tagsBG.json', JSON.stringify(tagsBG))
  doc.querySelectorAll('div').forEach(e => { 
    e.innerHTML = converter.makeHtml(e.innerHTML)
  })

  return doc.documentElement.outerHTML
}

// fixme : monster block is :
// ---
// >
async function main() {
  const html = global.IMP_BASE ? 
    await getBookHtml(`liber_primis_${version}.md`)
    : ''
  const advHtml = global.IMP_ADVANCED ? 
    await getBookHtml(`liber_mysteriorum_${version}.md`)
    : ''
  
  // fs.writeFileSync(LOCAL_DATA_FOLDER+'test.html', html)
    
  let featsSections, 
    speciesSections, 
    equipmentsSections, 
    componentsSections,
    capacitiesSections,
    abilitiesSections,
    xpSections
    
  let h1 = importsHelper.splitByHeader(html, '<h1')
  h1.sections.forEach(h1section => {
    let h2 = importsHelper.splitByHeader(h1section, '<h2')  
    let title = h2.title.toLowerCase()
    if (title === 'les racines') {
      capacitiesSections = h2.sections
    } else if (title === 'l\'équipement') {
      equipmentsSections = h2.sections
    } else if (title === 'les dons') {
      featsSections = h2.sections
    } else if (title === 'les espèces') {
      speciesSections = h2.sections
    } else if (title === 'les composants') {
      componentsSections = h2.sections
    } else if (title === 'les compétences') {
      abilitiesSections = h2.sections
    } else if (title === 'les espèces') {
      speciesSections = h2.sections
    } else if (title === 'l\'expérience') {
      xpSections = h2.sections
    }
  })

  let featsSectionsAdv, 
    weaknessSectionsAdv,
    speciesSectionsAdv, 
    equipmentsSectionsAdv, 
    componentsSectionsAdv,
    capacitiesSectionsAdv,
    abilitiesSectionsAdv,
    xpSectionsAdv,
    archetypesSectionsAdv

  let h1Adv = importsHelper.splitByHeader(advHtml, '<h1')
  h1Adv.sections.forEach(h1section => {
    let h2 = importsHelper.splitByHeader(h1section, '<h2')
    let title = h2.title.toLowerCase()
    if (title === 'les capacités') {
      capacitiesSectionsAdv = h2.sections
    } else if (title === 'les faiblesses') {
      weaknessSectionsAdv = h2.sections
    } else if (title === 'l\'équipement') {
      equipmentsSectionsAdv = h2.sections
    } else if (title === 'les dons') {
      featsSectionsAdv = h2.sections
    } else if (title === 'les espèces') {
      speciesSectionsAdv = h2.sections
    } else if (title === 'les composants') {
      componentsSectionsAdv = h2.sections
    } else if (title === 'les compétences') {
      abilitiesSectionsAdv = h2.sections
    } else if (title === 'les espèces') {
      speciesSectionsAdv = h2.sections
    } else if (title === 'l\'expérience') {
      xpSectionsAdv = h2.sections
    } else if (title === 'les archétypes') {
      archetypesSectionsAdv = h2.sections
    }
  })

  console.log("[import-data] parsing is starting")
  console.log("[import-data] local_script is", !!process.env.local_script)
  if (IMP_CAPACITIES) {
    importCapacities.parseCapacities({
      capacitiesSections, version, capacitiesSectionsAdv
    })
    console.log("[import-data] capacities parsed")
  }
  if (IMP_EQUIPMENTS) {
    importEquipments.parseEquipements({
      equipmentsSections, 
      componentsSections, 
      equipmentsSectionsAdv, 
      componentsSectionsAdv, 
      version
    })
    console.log("[import-data] equipments parsed")
  }
  if (IMP_FEATS) {
    importFeats.parseFeats({
      featsSections, 
      featsSectionsAdv, 
      speciesSections, 
      speciesSectionsAdv, 
      weaknessSectionsAdv,
      archetypesSectionsAdv,
      version
    })
    console.log("[import-data] feats parsed")
  }
  if (IMP_ABILITIES) {
    importAbilities.parseAbilities({
      abilitiesSections, version, abilitiesSectionsAdv
    })
    console.log("[import-data] abilities parsed")
  }
  if (IMP_SPECIES) {
    importSpecies.parseSpecies({
      speciesSections, version, speciesSectionsAdv
    })
    console.log("[import-data] species parsed")
  }
  if (IMP_XP) {
    importXp.parseXp({
      xpSections, version, xpSectionsAdv
    })
    console.log("[import-data] xp parsed")
  }
  if (IMP_ARCHETYPES) {
    importArchetypes.parseArchetypes({
      archetypesSectionsAdv, version
    })
    console.log("[import-data] archetypes parsed")
  }
  return
}

main()