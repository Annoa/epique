if [ "$#" -ne 1 ]; then
    echo "Usage: 1 argument (fichier dans le bucket /public/data à copier dans /data)"
    exit 9
fi

echo "aws s3 cp s3://bucketeer-3d2fdff7-6d49-4419-8616-748f42f0bce8/public/data/${1} data/${1}"
aws s3 cp s3://bucketeer-3d2fdff7-6d49-4419-8616-748f42f0bce8/public/data/${1} data/${1}