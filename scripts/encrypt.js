const CryptoJS = require('crypto-js')
const sha256 = require('crypto-js/sha256')

const main = () => {
  const str = process.argv[2]
  if (!str) {
    console.log('Usage: npm run encrypt [string]')
    return
  }
  const res = sha256(str).toString(CryptoJS.enc.Base64)
  console.log(res)
}

main()