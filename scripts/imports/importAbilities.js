require('dotenv').config()

const AWS = require('aws-sdk')
const fs = require('fs')
const importsHelper = require('./utils')

const s3  = new AWS.S3({
  accessKeyId: process.env.BUCKETEER_AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.BUCKETEER_AWS_SECRET_ACCESS_KEY,
  region: 'us-east-1',
})

const LOCAL_DATA_FOLDER = __dirname+'/../../data/'
const DATA_PATH = 'public/data/'

const DEPENSES = ['Minimale','Légère', 'Modérée', 'Importante']

function processTags(object) {
  const tags = []
  for (let tag of object.tags) {
    if (tag.trim()) {
      if (DEPENSES.includes(tag)) {
        object.depense = tag
      } else if (tag === 'Mana') {
        object.mana = true
      } else if (tag === 'Volonté') {
        object.volonte = true
      } else if (tag === 'Endurance') {
        object.endurance = true
      } else {
        tags.push(tag)
      }
    }
  }
  object.tags = tags
}

const parseAbility = html => {
  const ability = {}
  const lines = html.split('\n')
  const {name, id} = importsHelper.processNameAndId(lines.shift())
  ability.name = name
  ability.id = id
  ability.tags = []
  ability.desc = ''
  lines.forEach(line => {
    if (line.includes('inline')) {
      ability.tags.push(...importsHelper.stripHtml(line, '*****').split('*****'))
    } else {
      ability.desc += line
    }
  })
  processTags(ability)
  return ability
}

const parseSection = (sections, type, types, advanced) => {
  const abilities = []
  if (!types.includes(type)) types.push(type)
  sections.forEach(h3section => {
    abilities.push({
      ...parseAbility(h3section),
      type,
      advanced
    })
  })
  return abilities
}

function parseAbilities({abilitiesSections, abilitiesSectionsAdv, version}) {
  const result = []
  const types = []
  
  global.IMP_BASE && abilitiesSections.forEach(h2sections => {
    const h3 = importsHelper.splitByHeader(h2sections, '<h3')
    // console.log(h3.title)
    if (h3.title === 'Compétences de déplacement') {
      console.log(' > Compétences de déplacement')
      result.push(...parseSection(h3.sections, 'Déplacement', types))
    } else if (h3.title === 'Connaissances') {
      console.log(' > Connaissances')
      result.push(...parseSection(h3.sections, 'Connaissances', types))
    } else if (h3.title === 'Compétences sociales') {
      console.log(' > Compétences sociales')
      result.push(...parseSection(h3.sections, 'Social', types))
    } else if (h3.title === 'Compétences de perception') {
      console.log(' > Compétences de perception')
      result.push(...parseSection(h3.sections, 'Perception', types))
    } else if (h3.title === 'Compétences d\'armes naturelles') {
      console.log(' > Compétences d\'armes naturelles')
      result.push(...parseSection(h3.sections, 'Armes naturelles', types))
    } else if (h3.title === 'Compétences d\'objet') {
      console.log(' > Compétences d\'objet')
      result.push(...parseSection(h3.sections, 'Objet', types))
    } else if (h3.title === 'Aptitudes') {
      console.log(' > Aptitudes')
      result.push(...parseSection(h3.sections, 'Aptitudes', types))
    } else if (h3.title === 'Talents') {
      console.log(' > Talents')
      result.push(...parseSection(h3.sections, 'Talents', types))
    } 
  }) 

  global.IMP_ADVANCED && abilitiesSectionsAdv.forEach(h2sections => {
    const h3 = importsHelper.splitByHeader(h2sections, '<h3')
    // console.log(h3.title)
    if (h3.title === 'Compétences planaires') {
      console.log(' > Compétences planaires (adv)')
      result.push(...parseSection(h3.sections, 'Planaire', types, true))
    } else if (h3.title === 'Connaissances') {
      console.log(' > Connaissances (adv)')
      result.push(...parseSection(h3.sections, 'Connaissances', types, true))
    } else if (h3.title === 'Historiques') {
      console.log(' > Historiques (adv)')
      result.push(...parseSection(h3.sections, 'Historiques', types, true))
    } 
  }) 

  const ressources = ['mana', 'volonte', 'endurance']

  const content = JSON.stringify({
    data:result, version, 
    types, ressources, depenses: DEPENSES
  })

  fs.writeFileSync(LOCAL_DATA_FOLDER+'abilities.json', content)
  
  if (!process.env.local_script) {
    s3.putObject({
      Key: `${DATA_PATH}abilities.json`,
      Bucket: process.env.BUCKETEER_BUCKET_NAME,
      Body: Buffer.from(content)
    }, err => {
      if (err) {
        console.error('Echec sauvegarde de abilities.json sur AWS', err)
      }
    })
  }
}

module.exports = {
  parseAbilities
}