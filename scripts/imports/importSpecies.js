require('dotenv').config()

const AWS = require('aws-sdk')
const fs = require('fs')
const importsHelper = require('./utils')

const s3  = new AWS.S3({
  accessKeyId: process.env.BUCKETEER_AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.BUCKETEER_AWS_SECRET_ACCESS_KEY,
  region: 'us-east-1',
})

const LOCAL_DATA_FOLDER = __dirname+'/../../data/'
const DATA_PATH = 'public/data/'

const components = {}

// function processTags(object) {
//   const tags = []
//   for (let tag of object.tags) {
//     if (tag.trim()) {
//       if (['Minimale','Légère', 'Modérée', 'Importante'].includes(tag)) {
//         object.depense = tag
//       } else if (tag === 'Mana') {
//         object.mana = true
//       } else if (tag === 'Volonté') {
//         object.volonte = true
//       } else if (tag === 'Endurance') {
//         object.endurance = true
//       } else {
//         tags.push(tag)
//       }
//     }
//   }
//   object.tags = tags
// }

const parseElement = (h3section, typesRegex) => {
  const species = {}
  const lines = h3section.split('\n')
  const {name, id} = importsHelper.processNameAndId(lines.shift())
  species.name = name
  species.id = id
  species.tags = []
  species.desc = ''
  species.type = h3section.match(typesRegex)?.[0]
  lines.forEach(line => {
      species.desc += line
  })
  // processTags(species)
  return species
}

const parseSection = (sections, types, advanced) => {
  const species = []
  const typesRegex = new RegExp(types.join("|"), 'i')
  sections.forEach(h3section => {
    species.push({
      ...parseElement(h3section, typesRegex),
      advanced
    })
  })
  return species
}

function parseSpecies({speciesSections, version, speciesSectionsAdv}) {
  const result = []
  const types = [
    'Reptilien',
    'Insectoïde',
    'Mammifère',
    'Néphilien',
    'Xénomorphe',
    'Draconique',
    'Phytomorphe',
    'Lithomorphe'
  ]
  
  global.IMP_BASE && speciesSections.forEach(h2sections => {
    const h3 = importsHelper.splitByHeader(h2sections, '<h3')
    // console.log(h3.title)
    if (h3.title === 'Description des espèces') {
      console.log('Description des espèces')
      result.push(...parseSection(h3.sections, types))
    }
  }) 

  global.IMP_ADVANCED && speciesSectionsAdv.forEach(h2sections => {
    const h3 = importsHelper.splitByHeader(h2sections, '<h3')
    // console.log(h3.title)
    if (h3.title === 'Description des espèces') {
      console.log('Description des espèces (adv)')
      result.push(...parseSection(h3.sections, types, true))
    }
  }) 

  const content = JSON.stringify({data:result, components, version, types})
  fs.writeFileSync(LOCAL_DATA_FOLDER+'species.json', content)
  
  if (!process.env.local_script) {
    s3.putObject({
      Key: `${DATA_PATH}species.json`,
      Bucket: process.env.BUCKETEER_BUCKET_NAME,
      Body: Buffer.from(content)
    }, err => {
      if (err) {
        console.error('Echec sauvegarde de species.json sur AWS', err)
      }
    })
  }
}

module.exports = {
  parseSpecies
}