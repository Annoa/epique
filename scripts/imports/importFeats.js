require('dotenv').config()

const AWS = require('aws-sdk')
const fs = require('fs')
const importsHelper = require('./utils')

const s3  = new AWS.S3({
  accessKeyId: process.env.BUCKETEER_AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.BUCKETEER_AWS_SECRET_ACCESS_KEY,
  region: 'us-east-1',
})

const LOCAL_DATA_FOLDER = __dirname+'/../../data/'
const DATA_PATH = 'public/data/'

const parseFeat = (html, type) => {
  const res = {}
  const lines = html.split('\n')
  const regex = /(Tier|tier) [IVX]*/
  const {name, id} = importsHelper.processNameAndId(lines.shift())
  res.desc = ''
  res.name = name
  res.id = id
  lines.forEach(line => {
    if (line.startsWith('<strong>Coût')) {
      try {
        let [str0, ...str] = line.match(regex)[0]
        res.tier = str0.toUpperCase()+str.join('')
      } catch {
        console.log('cout witout tier:', res.name, line)
      }
      res.desc += line
    } else {
      res.desc += line
    }
  })
  return res
}

const parseSection = (sections, type, types, advanced) => {
  const results = []
  if (!types.includes(type)) types.push(type)
  sections.forEach(h3section => {
    results.push({
      ...parseFeat(h3section),
      type,
      advanced
    })
  })
  return results
}

async function parseFeats({
  featsSections, 
  featsSectionsAdv, 
  speciesSections, 
  speciesSectionsAdv, 
  weaknessSectionsAdv,
  archetypesSectionsAdv,
  version
}) {
  const result = []
  const types = []

  global.IMP_BASE && featsSections.forEach(h2sections => {
    const h3 = importsHelper.splitByHeader(h2sections, '<h3')
    if (h3.title === 'Dons généraux') {
      console.log(' > Dons généraux')
      result.push(...parseSection(h3.sections, 'Dons généraux', types))
    }
  }) 

  global.IMP_BASE && featsSections.forEach(h2sections => {
    const h3 = importsHelper.splitByHeader(h2sections, '<h3')
    if (h3.title === 'Dons de création') {
      console.log(' > Dons de création')
      result.push(...parseSection(h3.sections, 'Dons de création', types))
    }
  })

  global.IMP_BASE && speciesSections.forEach(h2sections => {
    const h3 = importsHelper.splitByHeader(h2sections, '<h3')
    if (h3.title === 'Dons d\'espèces') {
      console.log(' > Dons d\'espèces')
      result.push(...parseSection(h3.sections, 'Dons d\'espèces', types))
    }
  })

  global.IMP_ADVANCED && featsSectionsAdv.forEach(h2sections => {
    const h3 = importsHelper.splitByHeader(h2sections, '<h3')
    if (h3.title === 'Description des dons') {
      console.log(' > Description des dons (adv)')
      result.push(...parseSection(h3.sections, 'Dons généraux', types, true))
    }
  }) 

  global.IMP_ADVANCED && featsSectionsAdv.forEach(h2sections => {
    const h3 = importsHelper.splitByHeader(h2sections, '<h3')
    if (h3.title === 'Dons de création') {
      console.log(' > Dons de création (adv)')
      result.push(...parseSection(h3.sections, 'Dons de création', types, true))
    }
  }) 

  global.IMP_ADVANCED && speciesSectionsAdv.forEach(h2sections => {
    const h3 = importsHelper.splitByHeader(h2sections, '<h3')
    if (h3.title === 'Dons d\'espèces') {
      console.log(' > Dons d\'espèces (adv)')
      result.push(...parseSection(h3.sections, 'Dons d\'espèces', types, true))
    }
  }) 

  global.IMP_ADVANCED && weaknessSectionsAdv.forEach(h2sections => {
    const h3 = importsHelper.splitByHeader(h2sections, '<h3')
    if (h3.title === 'Liste de faiblesses') {
      console.log(' > Liste de faiblesses (adv)')
      result.push(...parseSection(h3.sections, 'Faiblesses', types, true))
    }
  }) 

  global.IMP_ADVANCED && archetypesSectionsAdv.forEach(h2sections => {
    const h3 = importsHelper.splitByHeader(h2sections, '<h3')
    if (h3.title === 'Dons d\'archétype') {
      console.log(' > Dons d\'archétype (adv)')
      result.push(...parseSection(h3.sections, 'Dons d\'archétypes', types, true))
    }
  })

  // const tiers = [...new Set(result.map(r => r.tier))]
  //   .filter(e => e)
  //   .sort((a, b) => a.localeCompare(b))
  const tiers = ['Tier I', 'Tier II', 'Tier III', 'Tier IV', 'Tier V']
  const content = JSON.stringify({data:result, version, types, tiers})

  fs.writeFileSync(LOCAL_DATA_FOLDER+'feats.json', content)
  if (!process.env.local_script) {
    s3.putObject({
      Key: `${DATA_PATH}feats.json`,
      Bucket: process.env.BUCKETEER_BUCKET_NAME,
      Body: Buffer.from(content)
    }, err => {
      if (err) {
        console.error('Echec sauvegarde de feats.json sur AWS', err)
      }
    })
  }
}

module.exports = {
  parseFeats
}