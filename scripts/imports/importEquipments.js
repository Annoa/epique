/* eslint-disable no-unreachable */
require('dotenv').config()

const AWS = require('aws-sdk')
const fs = require('fs')
const importsHelper = require('./utils')

const s3  = new AWS.S3({
  accessKeyId: process.env.BUCKETEER_AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.BUCKETEER_AWS_SECRET_ACCESS_KEY,
  region: 'us-east-1',
})

const LOCAL_DATA_FOLDER = __dirname+'/../../data/'
const DATA_PATH = 'public/data/'

const components = {}
const types = ['Arme','Armure','Bourdon','Bijou','Consommable', 'Technomagie','Implant']
const rarete = ['Q', 'C', 'U', 'R', 'M', 'E']
const equipmentType = ['charme', 'periapte', 'artefact', 'foci']

function processTags(object) {
  const tags = []
  for (let tag of object.tags) {
    if (tag.trim()) {
      if (rarete.includes(tag)) {
        object.rarete = tag
      } else if (tag === 'Charme') {
        object.charme = true
      } else if (tag === 'Périapte') {
        object.periapte = true
      } else if (tag === 'Artefact') {
        object.artefact = true
      } else if (tag === 'Foci') {
        object.foci = true
      } else if (tag === 'Mana') {
        object.mana = true
      } else if (tag === 'Volonté') {
        object.volonte = true
      } else if (tag === 'Endurance') {
        object.endurance = true
      } else if (tag.startsWith('Maniement')) {
        object.maniement = parseInt(tag.split(':')?.[1]) || 0
      } else {
        tags.push(tag)
      }
    }
  }
  object.tags = tags
}

const processComponentsLinks = desc => {
  const string = Object.keys(components).join('|')
  let re = new RegExp(`\\b(${string})\\b`, 'gi');
  return desc.replaceAll(re, '<a id="$1" href="#">$1</a>')
}

const parseObject = html => {
  // console.log('----')
  // console.log(html)
  // console.log('----')
  const object = {}
  const lines = html.split('\n')
  const {name, id} = importsHelper.processNameAndId(lines.shift())
  object.name = name
  object.id = id
  object.tags = []
  object.desc = ''
  lines.forEach(line => {
    if (line.includes('inline')) {
      object.tags.push(...importsHelper.stripHtml(line, '*****').split('*****'))
      object.tags = object.tags.filter(t => !types.includes(t))
    } else {
      object.desc += processComponentsLinks(line)
    }
  })
  processTags(object)
  return object
}

const parseSection = (sections, title, type) => {
  const objects = []
  sections.forEach(h3section => {
    const h4 = importsHelper.splitByHeader(h3section, '<h4')
    if (h4.title === title) {
      console.log(' > '+title)
      h4.sections.forEach(h4section => {
        const obj = parseObject(h4section)
        obj.type = type
        objects.push(obj)
      })
    }
  })
  return objects
}


function parseEquipements({
  equipmentsSections, 
  componentsSections, 
  equipmentsSectionsAdv, 
  componentsSectionsAdv, 
  version
}) {
  const result = []

  // fs.writeFileSync(LOCAL_DATA_FOLDER+'test.txt', componentsSectionsAdv.join('\n'))
  // return

  global.IMP_BASE && componentsSections.forEach(h2sections => {
    const h3 = importsHelper.splitByHeader(h2sections, '<h3')
    h3.sections.forEach(h3section => {
      if (h3.title === 'Tags') {
        const h4 = importsHelper.splitByHeader(h3section, '<h4')
        if (h4.title === 'Description des matériaux') {
          console.log(' > Description des matériaux')
          h4.sections.forEach(h3section => {
            const component = parseObject(h3section)
            components[component.name.toLowerCase()] = component
          })
        }
      }
    })
  })

  global.IMP_ADVANCED && componentsSectionsAdv.forEach(h2sections => {
    const h3 = importsHelper.splitByHeader(h2sections, '<h3')
    h3.sections.forEach(h3section => {
      // console.log('h3.title', h3.title)
      if (h3.title === 'Les matériaux') {
        console.log(' > Les matériaux (adv)')
        const h4 = importsHelper.splitByHeader(h3section, '<h4')
        if (h4.title === 'Liste des matériaux') {
          h4.sections.forEach(h3section => {
            const component = parseObject(h3section)
            components[component.name.toLowerCase()] = component
          })
        }
      }
      else if (h3.title === 'Ingrédients') {
        const h4 = importsHelper.splitByHeader(h3section, '<h4')
        // console.log(h4.title)
        if (h4.title === 'Liste des ingrédients') {
          console.log(' > Liste des ingrédients (adv)')
          h4.sections.forEach(h3section => {
            const component = parseObject(h3section)
            components[component.name.toLowerCase()] = component
          })
        }
      }
    })
  })

  global.IMP_BASE && equipmentsSections.forEach(h2sections => {
    const h3 = importsHelper.splitByHeader(h2sections, '<h3')
    if (h3.title === 'Armes') {
      result.push(...parseSection(h3.sections, 'Description des armes', 'Arme'))
    } else if (h3.title === 'Armures') {
      result.push(...parseSection(h3.sections, 'Description des armures', 'Armure'))
    } else if (h3.title === 'Bourdons') {
      result.push(...parseSection(h3.sections, 'Description des bourdons', 'Bourdon'))
    } else if (h3.title === 'Bijoux') {
      result.push(...parseSection(h3.sections, 'Description des bijoux', 'Bijou'))
    } else if (h3.title === 'Consommables') {
      result.push(...parseSection(h3.sections, 'Description des consommables', 'Consommable'))
    } else if (h3.title === 'Divers') {
      result.push(...parseSection(h3.sections, 'Description des objets divers', 'Divers'))
    }
  })

  global.IMP_ADVANCED && equipmentsSectionsAdv.forEach(h2sections => {
    const h3 = importsHelper.splitByHeader(h2sections, '<h3')
    if (h3.title === 'Technomagie') {
      result.push(...parseSection(h3.sections, 'Description des équipements technomagiques', 'Technomagie'))
    } else if (h3.title === 'Implants') {
      result.push(...parseSection(h3.sections, 'Description des implants', 'Implant'))
    } else if (h3.title === 'Armures') {
      result.push(...parseSection(h3.sections, 'Description des armures', 'Armure'))
    } else if (h3.title === 'Bijoux') {
      result.push(...parseSection(h3.sections, 'Description des bijoux', 'Bijou'))
    } else if (h3.title === 'Divers') {
      result.push(...parseSection(h3.sections, 'Liste divers', 'Divers'))
    }
  }) 
  const ressources = ['mana', 'volonte', 'endurance']
  const content = JSON.stringify({
    data:result, components, version, 
    types, ressources, rarete, equipmentType
  })

  fs.writeFileSync(LOCAL_DATA_FOLDER+'equipments.json', content)
  if (!process.env.local_script) {
    s3.putObject({
      Key: `${DATA_PATH}equipments.json`,
      Bucket: process.env.BUCKETEER_BUCKET_NAME,
      Body: Buffer.from(content)
    }, err => {
      if (err) {
        console.error('Echec sauvegarde de equipments.json sur AWS', err)
      }
    })
  }
}

module.exports = {
  parseEquipements
}