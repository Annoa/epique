require('dotenv').config()

const AWS = require('aws-sdk')
const fs = require('fs')
const importsHelper = require('./utils')

const s3  = new AWS.S3({
  accessKeyId: process.env.BUCKETEER_AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.BUCKETEER_AWS_SECRET_ACCESS_KEY,
  region: 'us-east-1',
})

const LOCAL_DATA_FOLDER = __dirname+'/../../data/'
const DATA_PATH = 'public/data/'

const RESSOURCES = ['Mana', 'Endurance', 'Volonté', 'Ichor', 'Extia', 'Vitae', 'Aether', 'Chaos', 'Néant', 'Geis', 'Gnose', 'Transcendance']
const ressourcesRegex = new RegExp('^('+RESSOURCES.join('|')+'):?(\\d)?', 'i')

function processTags(branch) {
  const tags = []
  for (let tag of branch.tags) {
    if (tag.trim()) {
      if (['M', 'U', 'R', 'C'].includes(tag)) {
        branch.rarete = tag
      } else if (tag === 'Factorisable') {
        branch.factorisable = true
      } else if (tag === 'Instantané') {
        branch.instant = true
      } else if (tag === 'Cumulatif') {
        branch.cumulative = true
      } else {
        const m = tag.match(ressourcesRegex)
        if (m) {
          const res = m[1], val = m[2]
          branch[res] = parseInt(val) || true
        } else {
          tags.push(tag)
        }
      }
    }
  }
  branch.tags = tags
}

const parseCapacity = (html) => {
  const capacite = {}
  const lines = html.split('\n')
  const {name, id} = importsHelper.processNameAndId(lines.shift())
  capacite.name = name
  capacite.id = id
  capacite.tags = []
  capacite.desc = ''
  lines.forEach(line => {
    if (line.includes('inline')) {
      capacite.tags.push(...importsHelper.stripHtml(line, '*****').split('*****'))
    } else {
      capacite.desc += line
    }
  })
  processTags(capacite)
  return capacite
}

async function parseCapacities({capacitiesSections, version, capacitiesSectionsAdv}) {
  const result = []
  
  global.IMP_BASE && capacitiesSections.forEach(h2sections => {
    const h3 = importsHelper.splitByHeader(h2sections, '<h3')
    // console.log('titre = ', h3.title)
    if (h3.title === 'Les branches génériques') {
      console.log(' > Les branches génériques')
      h3.sections.forEach(h3section => {
        const h4 = importsHelper.splitByHeader(h3section, '<h4')
        if (h4.title === 'Détail des branches génériques') {
          h4.sections.forEach(h4section => {
            result.push({
              ...parseCapacity(h4section),
              generic: true
            })
          })
        }
      })
    }

    if (h3.title === 'Les racines d\'Endurance') {
      console.log(' > Les racines d\'Endurance')
      h3.sections.forEach(h3section => {
        const [racine, ...branchs] = h3section.split('<h4').map(s => '<h4'+s)
        const i = result.push({
          ...parseCapacity(racine),
          generic: false,
          Endurance: true
        })
        result[i-1].branchs = branchs.map(branch => parseCapacity(branch))
      })
    }

    if (h3.title === 'Les racines de Mana') {
      console.log(' > Les racines de Mana')
      h3.sections.forEach(h3section => {
        const [racine, ...branchs] = h3section.split('<h4').map(s => '<h4'+s)
        const i = result.push({
          ...parseCapacity(racine),
          generic: false,
          Mana: true
        })
        result[i-1].branchs = branchs.map(branch => parseCapacity(branch))
      })
    }

    if (h3.title === 'Les racines de Volonté') {
      console.log(' > Les racines de Volonté')
      h3.sections.forEach(h3section => {
        const [racine, ...branchs] = h3section.split('<h4').map(s => '<h4'+s)
        const i = result.push({
          ...parseCapacity(racine),
          generic: false,
          Volonté: true
        })
        result[i-1].branchs = branchs.map(branch => parseCapacity(branch))
      })
    }

    if (h3.title === 'Les racines composées') {
      console.log(' > Les racines composées')
      h3.sections.forEach(h3section => {
        const [racine, ...branchs] = h3section.split('<h4').map(s => '<h4'+s)
        const i = result.push({
          ...parseCapacity(racine),
          generic: false,
          compose: true
        })
        result[i-1].branchs = branchs.map(branch => parseCapacity(branch))
      })
    }
  })

  global.IMP_ADVANCED && capacitiesSectionsAdv.forEach(h2sections => {
    const h3 = importsHelper.splitByHeader(h2sections, '<h3')
    if (h3.title === 'Les racines avancées') {
      console.log(' > Les racines avancées (adv)')
      h3.sections.forEach(h3section => {
        const [racine, ...branchs] = h3section.split('<h4').map(s => '<h4'+s)
        const i = result.push({
          ...parseCapacity(racine),
          generic: false,
          compose: true
        })
        result[i-1].branchs = branchs.map(branch => parseCapacity(branch))
      })
    }
  })
  
  const content = JSON.stringify({data:result, version})
  fs.writeFileSync(LOCAL_DATA_FOLDER+'capacities.json', content)
  if (!process.env.local_script) {
    s3.putObject({
      Key: `${DATA_PATH}capacities.json`,
      Bucket: process.env.BUCKETEER_BUCKET_NAME,
      Body: Buffer.from(content)
    }, err => {
      if (err) {
        console.error('Echec sauvegarde de racines.json sur AWS', err)
      }
    })
  }
}

module.exports = {
  parseCapacities
}