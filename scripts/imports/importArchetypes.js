require('dotenv').config()

const AWS = require('aws-sdk')
const fs = require('fs')
const importsHelper = require('./utils')

const s3  = new AWS.S3({
  accessKeyId: process.env.BUCKETEER_AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.BUCKETEER_AWS_SECRET_ACCESS_KEY,
  region: 'us-east-1',
})

const LOCAL_DATA_FOLDER = __dirname+'/../../data/'
const DATA_PATH = 'public/data/'

// function processTags(object) {
//   const tags = []
//   for (let tag of object.tags) {
//     if (tag.trim()) {
//       if (['Minimale','Légère', 'Modérée', 'Importante'].includes(tag)) {
//         object.depense = tag
//       } else if (tag === 'Mana') {
//         object.mana = true
//       } else if (tag === 'Volonté') {
//         object.volonte = true
//       } else if (tag === 'Endurance') {
//         object.endurance = true
//       } else {
//         tags.push(tag)
//       }
//     }
//   }
//   object.tags = tags
// }

const parseArchetype = html => {
  const archetype = {}
  const lines = html.split('\n')
  const {name, id} = importsHelper.processNameAndId(lines.shift())
  archetype.name = name
  archetype.id = id
  archetype.desc = ''
  lines.forEach(line => {
    if (line.startsWith('<p>Mineur')) {
      archetype.type = 'Mineur'
    } else if (line.startsWith('<p>Majeur')) {
      archetype.type = 'Majeur'
    } else if (line.startsWith('<p>Intermédiaire')) {
      archetype.type = 'Intermédiaire'
    }
    archetype.desc += line
  })
  // processTags(archetype)
  return archetype
}

const parseSection = (sections) => {
  const archetypes = []
  sections.forEach(h3section => {
    archetypes.push({
      ...parseArchetype(h3section),
    })
  })
  return archetypes
}

function parseArchetypes({archetypesSectionsAdv, version}) {
  const result = []
  const types = ['Mineur', 'Majeur', 'Intermédiaire']
  
  global.IMP_ADVANCED && archetypesSectionsAdv.forEach(h2sections => {
    const h3 = importsHelper.splitByHeader(h2sections, '<h3')
      if (h3.title === 'Descriptions des archétypes') {
        console.log(' > Descriptions des archétypes (adv)')
        result.push(...parseSection(h3.sections))
      }
  }) 

  const content = JSON.stringify({data:result, version, types})
  fs.writeFileSync(LOCAL_DATA_FOLDER+'archetypes.json', content)
  
  if (!process.env.local_script) {
    s3.putObject({
      Key: `${DATA_PATH}archetypes.json`,
      Bucket: process.env.BUCKETEER_BUCKET_NAME,
      Body: Buffer.from(content)
    }, err => {
      if (err) {
        console.error('Echec sauvegarde de archetypes.json sur AWS', err)
      }
    })
  }
}

module.exports = {
  parseArchetypes
}