require('dotenv').config()

const AWS = require('aws-sdk')
const fs = require('fs')
const importsHelper = require('./utils')

const s3  = new AWS.S3({
  accessKeyId: process.env.BUCKETEER_AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.BUCKETEER_AWS_SECRET_ACCESS_KEY,
  region: 'us-east-1',
})

const LOCAL_DATA_FOLDER = __dirname+'/../../data/'
const DATA_PATH = 'public/data/'

const components = {}

// function processTags(object) {
//   const tags = []
//   for (let tag of object.tags) {
//     if (tag.trim()) {
//       if (['Minimale','Légère', 'Modérée', 'Importante'].includes(tag)) {
//         object.depense = tag
//       } else if (tag === 'Mana') {
//         object.mana = true
//       } else if (tag === 'Volonté') {
//         object.volonte = true
//       } else if (tag === 'Endurance') {
//         object.endurance = true
//       } else {
//         tags.push(tag)
//       }
//     }
//   }
//   object.tags = tags
// }

const parseElement = (h3section) => {
  const xp = {}
  const lines = h3section.split('\n')
  const {name, id} = importsHelper.processNameAndId(lines.shift())
  xp.name = name
  xp.id = id
  xp.tags = []
  xp.desc = ''
  lines.forEach(line => {
      xp.desc += line
  })
  // processTags(xp)
  return xp
}

const parseSection = (sections, advanced) => {
  const xp = []
  sections.forEach(h3section => {
    xp.push({
      ...parseElement(h3section),
      advanced
    })
  })
  return xp
}

function parseXp({xpSections, version, xpSectionsAdv}) {
  const result = []

  global.IMP_BASE && xpSections.forEach(h2sections => {
    const h3 = importsHelper.splitByHeader(h2sections, '<h3')
    if (h3.title === 'Description des gains d\'XP') {
      console.log(' > Description des gains d\'XP')
      result.push(...parseSection(h3.sections))
    }
  })

  global.IMP_ADVANCED && xpSectionsAdv.forEach(h2sections => {
    const h3 = importsHelper.splitByHeader(h2sections, '<h3')
    if (h3.title === 'Description des gains d\'XP') {
      console.log(' > Description des gains d\'XP (adv)')
      result.push(...parseSection(h3.sections, true))
    }
  }) 

  const content = JSON.stringify({data:result, components, version})
  fs.writeFileSync(LOCAL_DATA_FOLDER+'xp.json', content)
  
  if (!process.env.local_script) {
    s3.putObject({
      Key: `${DATA_PATH}xp.json`,
      Bucket: process.env.BUCKETEER_BUCKET_NAME,
      Body: Buffer.from(content)
    }, err => {
      if (err) {
        console.error('Echec sauvegarde de xp.json sur AWS', err)
      }
    })
  }
}

module.exports = {
  parseXp
}