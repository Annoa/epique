const processNameAndId = html => {
  const id = html.match(/id="(.*?)"/)?.[1]
  const name = stripHtml(html).replace('&amp;', '&')
  return {id, name}
}

function stripHtml(html, separator) {
  return html.replace(/<\/?[^>]+(>|$)/g, separator || '')
}

function removeInsideStyle(html) {
  return html.replace(/style="(.*?)"/gi, "");
}

function processItalicMd(s) {
  return s.replace(/\*.*?\*/g, s => `<i>${s.split('*').join('')}</i>`)
}

const splitByHeader = (string, tag) => {
  // console.log("splitByHeader string", string)
  let sections = string.split(tag)
  // console.log("splitByHeader sections", sections)
  const title = stripHtml(sections.shift().split('\n')[0])
  // console.log("splitByHeader title", title)
  sections = sections.map(s => tag+s)
  return {title, sections}
}

module.exports = {
  stripHtml,
  removeInsideStyle,
  processItalicMd,
  splitByHeader,
  processNameAndId
}