/** @type {import('next').NextConfig} */

const path = require('path');

module.exports = {
  reactStrictMode: true,
  webpack: (config) => {
    config.resolve.fallback = { fs: false };
    config.module.rules.push({
      include: [
        path.join(__dirname, 'public/images/'),
      ],
      test: /\.svg$/,
      use: ["@svgr/webpack"]
    });
    return config;
  }
}
