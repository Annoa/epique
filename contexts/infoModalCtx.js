import { createContext, useCallback, useContext, useEffect, useState } from 'react'
import ModalInfo from '@components/modals/ModalInfo'

const InfoModalCtx = createContext()

const INIT = {
  pjs: [],
  maps: [],
  groups: [],
  pnjs: []
}

export const InfoModalProvider = ({
  authData,
  children
}) => {
  // const router = useRouter()
  const [data, setData] = useState(INIT)
  const [modalInfo, setModalInfo] = useState({})

  const onHtmlClick = useCallback(e => {
    if (e.target.href) {
      const hash = e.target.href.split('#')[1]
      if (hash.startsWith('pnj-')) {
        setModalInfo({ pnj: hash.substring(4) })
      }
    }
  }, [])

  useEffect(() => {
    setData(authData)
  }, [authData])


  return <InfoModalCtx.Provider value={{
    onHtmlClick
  }}>
    {children}
    <ModalInfo
      info={modalInfo} 
      setInfo={setModalInfo}
      {...data}/>
  </InfoModalCtx.Provider>
}

export function useInfoModalCtx() {
  const context = useContext(InfoModalCtx)
  if (context === undefined) {
    throw new Error("Context must be used within a Provider")
  }

  return context
}