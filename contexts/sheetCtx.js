import { createContext, useCallback, useContext, useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { checkStatus } from '@lib/helpers/fetch'
import { ERROR, SAVED, SAVING } from '@constants/app'

const SheetContext = createContext()

export const SheetProvider = ({ children }) => {
  const router = useRouter()
  const { type, id } = router.query
  const [data, setData] = useState({})
  const isPnj = type === 'pnj'

  useEffect(() => {
    const url = `/api/sheet/${id}?pnj=${isPnj}`
    fetch(url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    }).then(checkStatus).then(json => {
      setData(json)
    }).catch(err => console.error(err))
  }, [id, isPnj])

  const save = useCallback(async ({path, value, overwrite, push, setSavingStatus}) => {
    const url = `/api/sheet/${id}?pnj=${isPnj}`
    const body = JSON.stringify({path, value, overwrite, push})
    console.log('SAVE', body)
    setSavingStatus(SAVING)
    try {
      const response = await fetch(url, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body
      })
      const json = await checkStatus(response)
      setSavingStatus(SAVED)
      setData(json)
      return true
    } catch (err) {
      setSavingStatus(ERROR)
      return false
    }
  }, [id, isPnj])

  return <SheetContext.Provider value={{
    sheet: data,
    save,
    id
  }}>
    {children}
  </SheetContext.Provider>
}

export function useSheetCtx(props) {
  const [savingStatus, setSavingStatus] = useState()
  const [data, setData] = useState()
  const {getter, dataPath} = props || {} 
  const context = useContext(SheetContext)
  if (context === undefined) {
    throw new Error("Context must be used within a Provider")
  }

  const func = useCallback(obj => {
    if (!obj) return
    if (getter) {
      return getter
    } else if (dataPath) {
      return dataPath.split('/').reduce((o,i)=> o?.[i], obj)
    }
    return obj
  }, [getter, dataPath])

  useEffect(() => {
    setData(func(context.sheet))
  }, [context.sheet, func])
  
  const save = useCallback(args => {
    return context.save({...args, setSavingStatus})
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [context.save])

  return {...context, save, data, savingStatus}
}