import {ReactComponent as Endurance} from './endurance.svg'
import {ReactComponent as Mana} from './mana.svg'
import {ReactComponent as Volonte} from './volonte.svg'
import {ReactComponent as Ichor} from './ichor.svg'
import {ReactComponent as Extia} from './extia.svg'
import {ReactComponent as Vitae} from './vitae.svg'
import {ReactComponent as Souffle} from './souffle.svg'
import {ReactComponent as Transcendance} from './transcendance.svg'
import {ReactComponent as Essence} from './essence.svg'
import {ReactComponent as Neant} from './neant.svg'
import {ReactComponent as Chaos} from './chaos.svg'
import {ReactComponent as Aether} from './aether.svg'
import {ReactComponent as Gnose} from './gnose.svg'
import {ReactComponent as Geis} from './geis.svg'


const Symboles = {
  "endurance": Endurance,
  "mana": Mana,
  "volonte": Volonte,
  "ichor": Ichor,
  "extia": Extia,
  "vitae": Vitae,
  "souffle": Souffle,
  "transcendance": Transcendance,
  'essence': Essence,
  'neant': Neant,
  'chaos': Chaos,
  'aether': Aether,
  'gnose': Gnose,
  'geis': Geis,
}

export default Symboles