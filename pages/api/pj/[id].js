import { getCookieUser } from '@lib/auth'
import { deletePj, getPjs, updatePj } from '@lib/data/pj'

export default async function handler(req, res) {
  const user = await getCookieUser(req, res)
  if (!user) {
    return res.status(403).json({ message: 'Unauthorized' })
  }
  const pjs = await getPjs(user)
  const { id } = req.query
  const pj = pjs.find(s => s.id === id)
  if (!pj) {
    return res.status(403).json({ message: 'Unauthorized' })
  } 

  if (req.method === 'GET') {
    return res.status(200).json(pj)
  } 
  
  if (req.method === 'PUT') {
    if (!user.isAdmin && user.id !== pj.userId) {
      return res.status(403).json({ message: 'Unauthorized' })
    }
    const newPjs = await updatePj(id, req.body)
    return res.status(201).json(newPjs)
  } 

  if (req.method === 'DELETE') {
    const pj = pjs.find(g => g.id === id)
    if (!pj) {
      return res.status(404).json({ message: 'pj not found' })
    }
    const newPjs = await deletePj(id, req.body.deleteAllFiles)
    return res.status(201).json(newPjs)
  } 

  return res.status(424).json({ message: 'Invalid method' })
}