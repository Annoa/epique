import { getCookieUser } from '@lib/auth'
import { addPj, getPjs } from '@lib/data/pj'

export default async function handler(req, res) {
  const user = await getCookieUser(req, res)
  const pjs = await getPjs(user)
  
  if (req.method === 'GET') {
    return res.status(200).json(pjs)
  } 

  if (!user.isAdmin) {
    return res.status(403).json({ message: 'Unauthorized' })
  }

  if (req.method === 'POST') {
    const { id, name, shortName } = req.body
    if (!id || !name || !shortName) {
      return res.status(400).json({ message: 'Sheet should have id, name and shortName' })
    }
    const exist = pjs.find(m => m.id === req.body.id)
    if (exist)
      return res.status(422).json({ message: "Id already in use!" })
    const newSheetsInfo = await addPj(req.body)
    return res.status(201).json(newSheetsInfo)
  } 
  return res.status(424).json({ message: 'Invalid method' })
}