import { getCookieUser } from '@lib/auth'
import { getPjs } from '@lib/data/pj'
import { getPnjs } from '@lib/data/pnj'
import { getSheetById, processSheet, updateSheet } from '@lib/data/sheets'

export default async function handler(req, res) {
  const user = await getCookieUser(req, res)
  const { id, pnj } = req.query
  const isPnj = (pnj === 'true')
  if (!user || (!user.isAdmin && isPnj)) {
    return res.status(403).json({ message: 'Unauthorized' })
  }
  const persos = await (isPnj? getPnjs(user) : getPjs(user))
  const perso = persos.find(s => s.id === id)
  if (!perso) {
    return res.status(403).json({ message: 'Unauthorized' })
  }
  const sheet = await getSheetById({id, isPnj})
  if (!sheet) {
    return res.status(403).json({ message: 'Unauthorized' })
  }
  
  if (req.method === 'GET') {
    processSheet(sheet)
    return res.status(200).json(sheet)
  } 
  if (req.method === 'PUT') {
    const { path, value, overwrite, push } = req.body
    const newSheet = await updateSheet({
      id, sheet, path, value, overwrite, isPnj, push
    })
    processSheet(newSheet)
    return res.status(201).json(newSheet)
  }
  return res.status(424).json({ message: 'Invalid method' })
}