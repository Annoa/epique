import { getCookieUser } from '@lib/auth'

export default async function handler(req, res) {
  const id = req.query.id
  const user = await getCookieUser(req, res)
  if (!user?.isAdmin) {
    return res.status(403).json({ message: 'Unauthorized' })
  }
 
  try {
    await res.revalidate(`/rules/${id}`)
    return res.json({ revalidated: true })
  } catch (err) {
    // If there was an error, Next.js will continue
    // to show the last successfully generated page
    return res.status(500).send('Error revalidating')
  }
}