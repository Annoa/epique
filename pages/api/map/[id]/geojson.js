import { getCookieUser } from '@lib/auth'
import { getMaps, saveMapGeojson } from '@lib/data/maps'

export default async function handler(req, res) {
  const user = await getCookieUser(req, res)
  if (!user.isAdmin) {
    return res.status(403).json({ message: 'Unauthorized' })
  }
  const maps = await getMaps(user)
  const id = req.query.id
  const map = maps.find(g => g.id === id)
  if (!map) {
    return res.status(404).json({ message: 'Map not found' })
  }
  if (req.method === 'PUT') {
    const geojson = await saveMapGeojson(id, req.body)
    return res.status(201).json(geojson)
  }  
  return res.status(424).json({ message: 'Invalid method' })
}