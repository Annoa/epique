import { getCookieUser } from '@lib/auth'
import { deleteMap, getMaps, updateMap } from '@lib/data/maps'

export default async function handler(req, res) {
  const user = await getCookieUser(req, res)
  const maps = await getMaps(user)
  const id = req.query.id
  if (req.method === 'GET') {
    const map = maps.find(g => g.id === id)
    if (!map) {
      return res.status(403).json({ message: 'Unauthorized' })
    }
    return res.status(200).json(map)
  } 

  if (!user.isAdmin) {
    return res.status(403).json({ message: 'Unauthorized' })
  }

  if (req.method === 'PUT') {
    const map = maps.find(g => g.id === id)
    if (!map) {
      return res.status(404).json({ message: 'Map not found' })
    }
    const newMaps = await updateMap(id, req.body)
    return res.status(201).json(newMaps)
  }

  if (req.method === 'DELETE') {
    const map = maps.find(g => g.id === id)
    if (!map) {
      return res.status(404).json({ message: 'Map not found' })
    }
    const newMaps = await deleteMap(id, req.body.deleteAllFiles)
    return res.status(201).json(newMaps)
  } 
  
  return res.status(424).json({ message: 'Invalid method' })
}