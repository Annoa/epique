import { getCookieUser } from '@lib/auth'
import { addMap, getMaps } from '@lib/data/maps'

export default async function handler(req, res) {
  const user = await getCookieUser(req, res)
  const maps = await getMaps(user)
  if (req.method === 'GET') {
    return res.status(200).json(maps)
  } 

  if (!user.isAdmin) {
    return res.status(403).json({ message: 'Unauthorized' })
  }

  if (req.method === 'POST') {
    if (!req.body.name || !req.body.id) {
      return res.status(400).json({ message: 'Map should have name' })
    }
    const exist = maps.find(m => m.id === req.body.id)
    if (exist)
      return res.status(422).json({ message: "Id already in use!" })
    const newMaps = await addMap(req.body)
    return res.status(201).json(newMaps)
  }
  
  return res.status(424).json({ message: 'Invalid method' })
}