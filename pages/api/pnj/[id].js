import { getCookieUser } from '@lib/auth'
import { deletePnj, getPnjs, processPnj, updatePnj } from '@lib/data/pnj'

export default async function handler(req, res) {
  const user = await getCookieUser(req, res)
  if (!user) {
    return res.status(403).json({ message: 'Unauthorized' })
  }
  const pnjs = await getPnjs(user)
  const { id } = req.query
  const pnj = pnjs.find(s => s.id === id)
  if (!pnj) {
    return res.status(403).json({ message: 'Unauthorized' })
  } 

  if (req.method === 'GET') {
    processPnj(pnj)
    return res.status(200).json(pnj)
  } 
  
  if (req.method === 'PUT') {
    let body
    if (user.isAdmin) {
      body = req.body
    } else {
      body = {groupComments: {}}
      user.groupIds.forEach(id => {
        body.groupComments[id] = req.body.groupComments?.[id]
      })
    }
    const newPnjs = await updatePnj(id, body, req.body.deleteAllFiles)
    return res.status(201).json(newPnjs)
  } 

  if (req.method === 'DELETE') {
    if (!user.isAdmin) {
      return res.status(403).json({ message: 'Unauthorized' })
    }
    const pnj = pnjs.find(g => g.id === id)
    if (!pnj) {
      return res.status(404).json({ message: 'pnj not found' })
    }
    const newPnjs = await deletePnj(id, req.body.deleteAllFiles)
    return res.status(201).json(newPnjs)
  } 

  return res.status(424).json({ message: 'Invalid method' })
}