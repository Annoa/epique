import { getCookieUser } from '@lib/auth'
import { addPnj, getPnjs, processPnjs } from '@lib/data/pnj'

export default async function handler(req, res) {
  const user = await getCookieUser(req, res)
  const pnjs = await getPnjs(user)
  
  if (req.method === 'GET') {
    processPnjs(pnjs)
    return res.status(200).json(pnjs)
  } 

  if (!user.isAdmin) {
    return res.status(403).json({ message: 'Unauthorized' })
  }

  if (req.method === 'POST') {
    const { id, name, shortName } = req.body
    if (!id || !name || !shortName) {
      return res.status(400).json({ message: 'Sheet should have id, name and shortName' })
    }
    const exist = pnjs.find(m => m.id === req.body.id)
    if (exist)
      return res.status(422).json({ message: "Id already in use!" })
    const newPnjs = await addPnj(req.body)
    return res.status(201).json(newPnjs)
  } 
  return res.status(424).json({ message: 'Invalid method' })
}