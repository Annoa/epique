import { getAuthProps } from '@lib/auth'

export default async function handler(req, res) {
  const props = await getAuthProps(req, res)
  return res.status(200).json(props)
}