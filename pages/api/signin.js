import jwt from 'jsonwebtoken'
import { setCookie } from 'cookies-next'
import { getUsers } from '@lib/data/users'


export default async function handler(req, res) {
  const users = await getUsers()
  const { id, password } = req.body
  if (req.method === 'POST') {
    const user = users.find(u => u.id === id)
    if (!user) {
      return res.status(422).json({ message: 'User not found' })
    }
    if (user.password !== password) {
      return res.status(403).json({ message: 'Wrong password' })
    }

    const token = jwt.sign({ userId: id }, process.env.JWT_TOKEN, {
      expiresIn: '60d',
    })

    setCookie('epique-token', token, {
      req,
      res,
      maxAge: 60 * 60 * 24 * 60, // 60 day
      path: '/',
    })

    return res.status(200).json(user)
  } else {
    return res.status(424).json({ message: 'Invalid method!' })
  }
}