import { getCookieUser } from '@lib/auth'
import { addGroup, getGroups } from '@lib/data/groups'

export default async function handler(req, res) {
  const user = await getCookieUser(req, res)
  const groups = await getGroups()
  
  if (req.method === 'GET') {
    return res.status(200).json(groups)
  } 

  if (!user.isAdmin) {
    return res.status(403).json({ message: 'Unauthorized' })
  }

  if (req.method === 'POST') {
    if (!req.body.name || !req.body.id) {
      return res.status(400).json({ message: 'Group should have name and id' })
    }
    const exist = groups.find(g => g.id === req.body.id)
    if (exist)
      return res.status(422).json({ message: "Id already in use!" })
    const newGroups = await addGroup(req.body)
    return res.status(201).json(newGroups)
  } 
  return res.status(424).json({ message: 'Invalid method' })
}