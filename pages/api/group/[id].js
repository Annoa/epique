import { getCookieUser } from '@lib/auth'
import { deleteGroup, getGroups, updateGroup } from '@lib/data/groups'

export default async function handler(req, res) {
  const user = await getCookieUser(req, res)
  const groups = await getGroups()
  const id = req.query.id
  
  if (req.method === 'GET') {
    const group = groups.find(g => g.id === id)
    return res.status(200).json(group)
  } 

  if (!user.isAdmin) {
    return res.status(403).json({ message: 'Unauthorized' })
  }

  if (req.method === 'PUT') {
    const group = groups.find(g => g.id === id)
    if (!group) {
      return res.status(404).json({ message: 'Group not found' })
    }
    const newGroups = await updateGroup(id, req.body)
    return res.status(201).json(newGroups)
  } 

  if (req.method === 'DELETE') {
    const group = groups.find(g => g.id === id)
    if (!group) {
      return res.status(404).json({ message: 'Group not found' })
    }
    const newGroups = await deleteGroup(id, req.body.deleteAllFiles)
    return res.status(201).json(newGroups)
  } 
  
  return res.status(424).json({ message: 'Invalid method' })
}