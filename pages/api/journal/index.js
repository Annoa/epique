import { getGroups } from '@lib/data/groups'

export default async function handler(req, res) {
  const groups = await getGroups()
  
  if (req.method === 'GET') {
    return res.status(200).json(groups)
  } 

  return res.status(424).json({ message: 'Invalid method' })
}