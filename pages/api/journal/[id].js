import { getCookieUser } from '@lib/auth'
import { getGroups } from '@lib/data/groups'
import { 
  addJournalEntry, deleteJournalEntry, 
  getJournal, processJournal, updateJournalEntry 
} from '@lib/data/journals'

export default async function handler(req, res) {
  const user = await getCookieUser(req, res)
  const groups = await getGroups()
  const id = req.query.id

  // if (!user.isAdmin && !user.groupIds?.includes(id)) {
  //   return res.status(403).json({ message: 'Unauthorized' })
  // }

  if (!user.isAdmin) {
    return res.status(403).json({ message: 'Unauthorized' })
  }

  if (!groups.find(g => g.id === id)) {
    return res.status(404).json({ message: 'Journal not found' })
  }
  
  if (req.method === 'GET') {
    const journal = getJournal(user, id)
    return res.status(200).json(journal)
  } 

  if (req.method === 'PUT') {
    if (!req.body.id) {
      return res.status(400).json({ 
        message: 'Entry should have id' 
      })
    }
    const body = user.isAdmin? req.body : {id:req.body.id, content:req.body.content}
    const newJournals = await updateJournalEntry(id, body)
    processJournal(newJournals)
    return res.status(201).json(newJournals)
  } 

  if (req.method === 'POST') {
    const entry = req.body
    const newJournals = await addJournalEntry(id, entry)
    processJournal(newJournals)
    return res.status(201).json(newJournals)
  } 

  if (req.method === 'DELETE') {
    if (!req.body.id) {
      return res.status(400).json({ message: 'No id provided' })
    }
    const newJournals = await deleteJournalEntry(id, req.body)
    processJournal(newJournals)
    return res.status(201).json(newJournals)
  } 
  
  return res.status(424).json({ message: 'Invalid method' })
}