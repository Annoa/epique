import { getCookieUser } from '@lib/auth'
import { deleteUser, getUsers, updateUser } from '@lib/data/users'

export default async function handler(req, res) {
  const cookieUser = await getCookieUser(req, res)
  const users = await getUsers()
  const id = req.query.id
  
  if (req.method === 'GET') {
    let user
    if (!cookieUser.isAdmin && cookieUser.id !== id) {
      const shortUsers = await getUsers(short)
      user = shortUsers.find(u => u.id === id)
    } else {
      user = users.find(u => u.id === id)
    }
    return res.status(200).json(user)
  }

  if (req.method === 'PUT') {
    if (!cookieUser.isAdmin && cookieUser.id !== id) {
      return res.status(403).json({ message: 'Unauthorized' })
    }
    const user = users.find(u => u.id === id)
    if (!user) {
      return res.status(404).json({ message: 'User not found' })
    }
    const newUsers = await updateUser(id, req.body)
    return res.status(201).json(newUsers)
  } 

  if (req.method === 'DELETE') {
    if (!cookieUser.isAdmin) {
      return res.status(403).json({ message: 'Unauthorized' })
    }
    const user = users.find(u => u.id === id)
    if (!user) {
      return res.status(404).json({ message: 'User not found' })
    }
    const newUsers = await deleteUser(id)
    return res.status(201).json(newUsers)
  } 
  
  return res.status(424).json({ message: 'Invalid method' })
}