import { getCookieUser } from '@lib/auth'
import { addUser, getUsers } from '@lib/data/users'

export default async function handler(req, res) {
  const cookieUser = await getCookieUser(req, res)
  const users = await getUsers()
  
  if (req.method === 'GET') {
    let users_ = users
    if (!cookieUser.isAdmin) {
      users_ = await getUsers(short)
    }
    return res.status(200).json(users_)
  } 

  if (!cookieUser.isAdmin) {
    return res.status(403).json({ message: 'Unauthorized' })
  }

  if (req.method === 'POST') {
    if (!req.body.pseudo || !req.body.id) {
      return res.status(400).json({ message: 'User should have pseudo and id' })
    }
    const userExist = users.find(u => u.id === req.body.id)
    if (userExist)
      return res.status(422).json({ message: "Id already in use!" })
    const newUsers = await addUser(req.body)
    return res.status(201).json(newUsers)
  } 
  return res.status(424).json({ message: 'Invalid method' })
}