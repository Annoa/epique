import { useEffect, useState } from 'react'
import dynamic from 'next/dynamic'
import io from 'socket.io-client'

import { getAuthProps } from '@lib/auth'
import { getMap } from '@lib/data/maps'
import { getUsers } from '@lib/data/users'

import Layout from '@components/Layout'

const ClientMap = dynamic(
  () => import('@components/Map'), 
  { ssr:false }
)

export async function getServerSideProps({ req, res, params }) {
  const props = await getAuthProps(req, res)
  const [mapsProps, users] = await Promise.all([
    getMap(params.id, props.user),
    getUsers(true)
  ])
  if (!mapsProps) {
    return {
      redirect: {
        permanent: false,
        destination: "/",
      },
      props: {},
    }
  }
  return { props: { ...props, ...mapsProps, users }} 
}

const MapPage = ({map, geojson, ...props}) => {
  const [socket, setSocket] = useState()

  useEffect(() => {
    fetch('/api/socket').finally(() => {
      let socket_ = io()
      socket_.on('connect', () => {
        console.log('connected')
      })
      setSocket(socket_)
    })
  }, [])

  return <Layout title={map.name} className='p-0' {...props}>
    <ClientMap 
      key={map.id}
      map={map} 
      groups={props.groups} 
      geojson={geojson}
      socket={socket}
      user={props.user}
      users={props.users}
    />
  </Layout> 
}

export default MapPage
