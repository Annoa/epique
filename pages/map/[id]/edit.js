import { useState } from 'react'
import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'

import { getAuthProps } from '@lib/auth'
import { getMap } from '@lib/data/maps'
import { getUsers } from '@lib/data/users'
import { checkStatus } from '@lib/helpers/fetch'

import Layout from '@components/Layout'
import Toast from '@components/Toast'

const EditableMap = dynamic(
  () => import('@components/Map/EditableMap'), 
  { ssr:false }
)

export async function getServerSideProps({ req, res, params }) {
  const props = await getAuthProps(req, res)
  const [mapProps, users] = await Promise.all([
    getMap(params.id, props.user),
    getUsers(true)
  ])
  if (!mapProps || !props.user?.isAdmin) {
    return {
      redirect: {
        permanent: false,
        destination: "/",
      },
      props: {},
    }
  }
  return { props: { ...props, ...mapProps, users }} 
}

const EditMapPage = ({map, groups, geojson, ...props}) => {
  const router = useRouter()
  const [toasts, setToasts] = useState([])
  // const map = mapProps.map
  // const [groups, setGroups] = useState(mapProps.groups)
  // const [geojson, setGeojson] = useState(mapProps.geojson)
  const emitToast = t => setToasts(prev => [...prev, t])

  const saveGeojson = geojson => fetch(
    `/api/map/${map.id}/geojson`, 
    {
      method: 'PUT',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(geojson)
    }).then(checkStatus)
    .then(res => {
      emitToast({
        title: 'Save GeoJSON',
        content: `${res.features.length} features`,
        success: true
      })
      console.log('Save GeoJSON', res)
      router.replace(router.asPath)
    })
    .catch(err => console.log('error', err))

  const saveGroups = groups => fetch(
    `/api/map/${map.id}/groups`, 
    {
      method: 'PUT',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(groups)
    }).then(checkStatus)
    .then(res => {
      console.log('save groups', res)
      emitToast({
        title: 'Save Groups',
        content: `${res?.length||0} groups: ${res?.map(g => g.name)?.join(', ')||'-'}`,
        success: true
      })
      router.replace(router.asPath)
    })
    .catch(err => {
      console.log('save groups', err)
      emitToast({
        title: 'Save Groups',
        content: err.message,
        alert: true
      })
    })

  return <Layout title={map.name} className='p-0 overflow-clip' {...props}>
    <EditableMap 
      key={map.id}
      map={map} 
      groups={groups} 
      geojson={geojson}
      saveGeojson={saveGeojson}
      saveGroups={saveGroups}
    />
    {toasts?.map((t,i) => <Toast key={i} {...t}/>)}
  </Layout> 
}

export default EditMapPage
