import Layout from '@components/Layout'
import { faEdit } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { getAuthProps } from '@lib/auth'
import Link from 'next/link'

export async function getServerSideProps({ req, res }) {
  const props = await getAuthProps(req, res)
  return { props }
}

export default function Maps(props) {
  const maps = props.maps || []
  const user = props.user
  return (
    <Layout title='Cartes' {...props}>
      <div className='stain-10'/>
      <div className='flex items-center justify-center h-full pb-10'>
        <div className='grid grid-cols-2 gap-y-8 gap-x-2 md:gap-10 items-center'>
          {maps.map(({id, name}) => (
            <div key={id} className='relative'>
              <Link href={'map/'+id} >
                <div className={`frame grow text-center WalterTurncoat text-[#524300]`}>
                  {name}
                </div>
              </Link>
              {user.isAdmin && 
                <Link href={'map/'+id+'/edit'} className='btn absolute right-1/2 -bottom-3 px-3 translate-x-1/2'>
                  <FontAwesomeIcon icon={faEdit}/>
                </Link>
              }
            </div>
          ))}
        </div>
      </div>
    </Layout>
  )
}
