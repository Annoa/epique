import { useState } from 'react'
import { useRouter } from 'next/router'
import Layout from '@components/Layout'
import { postFetcher } from '@lib/helpers/fetch'


export default function SignupPage() {
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const router = useRouter()

  const signupHandler = async (e) => {
    e.preventDefault()
    try {
      const rep = postFetcher('/api/signup', {
        body: JSON.stringify({
          name,
          email,
          password,
        })
      })
      router.push("/");
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <Layout>
      <h1>SignUp</h1>

      <p>Only unauthenticated users can access this page.</p>

      <form onSubmit={signupHandler}>
        <input
          type="text"
          placeholder="Name"
          onChange={(e) => setName(e.target.value)}
          value={name}
        /><br/>
        <input
          type="email"
          placeholder="Email"
          onChange={(e) => setEmail(e.target.value)}
          value={email}
        /><br/>
        <input
          type="password"
          placeholder="Password"
          onChange={(e) => setPassword(e.target.value)}
          value={password}
        /><br/>
        <button className='btn '>SignUp</button>
      </form>
    </Layout>
  )
}