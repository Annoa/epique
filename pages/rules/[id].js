import Link from 'next/link'
import { useRouter } from 'next/router'
import { useCallback, useEffect, useState } from 'react'
import useSWR from 'swr'

import { getRulesById } from '@lib/data/rules'
import { normalize } from '@lib/helpers/string'
import { RULES } from '@constants/routes'

import Layout from '@components/Layout'
import DataCard from '@components/Rules/DataCard'
import DataFilter from '@components/Rules/DataFilter'
import { checkStatus } from '@lib/helpers/fetch'

const DEBUG = false

export async function getStaticPaths() {
  const paths = Object.keys(RULES).map(id => ({params:{id}}))
  return {
    paths,
    fallback: false,
  }
}

export async function getStaticProps({params}) {
  const rules = await getRulesById(params.id)
  return {
    props: {rules}
  }
}

const fetcher = (...args) => fetch(...args).then(res => res.json())


const Rules = ({rules}) => {
  const router = useRouter()
  const { data: authProps } = useSWR('/api/auth', fetcher)
  const { id } = router.query
  const [data, setData] = useState(rules[id].data)
  const [filter, setFilter] = useState({})
  const [filtredData, setFiltredData] = useState([])
  const [highlightedId, setHighlightedId] = useState()
  const [favs, setFavs] = useState()

  useEffect(() => {
    const favs = authProps?.user?.[id]
    setFavs(favs)
  }, [authProps?.user, id])

  useEffect(() => {
    let data = rules[id].data
    if (favs && data) {
      const favsIds = favs.map(({id}) => id)
      data = data.map(d => ({
        ...d,
        isFavorite: favsIds.includes(d.id)
      }))
    } 
    setData(data)
  }, [favs, id, rules])

  useEffect(() => {
    const hashtag = router.asPath.split('#')[1]
    setHighlightedId(hashtag)

    const handleRouteChange = () => {
      const hashtag = router.asPath.split('#')[1]
      setHighlightedId(hashtag)
      setFilter({})
    }
    router.events.on('routeChangeComplete', handleRouteChange)
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange)
    }
  }, [router])

  useEffect(() => {
    if (highlightedId) {
      const element = document.getElementById(highlightedId)
      if (element) {
        element.scrollIntoView()
      }
    }
  }, [highlightedId])

  const onInputChange = useCallback(e => {
    const {name, value, id, checked} = e.target
    DEBUG && console.log(name, value, id, checked)
    if (name === 'name') {
      setFilter({...filter, name:normalize(value)})
    } else if (name === 'type') {
      if (value) {
        setFilter({...filter, type: value})
      } else {
        setFilter({...filter, type: false})
      }
      // array checkboxes
    } else if ([
        'tier','ressource','depense','equipmentType','rarete'
      ].includes(name)) {
      const arr = filter[name] || []
      if (checked) {
        setFilter({...filter, [name]: [...arr, id]})
      } else {
        setFilter({...filter, [name]: arr.filter(f => f !== id)})
      }
    } 
    // unique checkboxes
    else if ([
      'generic', 'factorisable', 'cumulative', 'instant', 
      'jds', 'compose', 'single', 'isFavorite'
    ].includes(id)) {
      setFilter({...filter, [id]: e.target.checked})
    }
  }, [setFilter, filter])

  useEffect(() => {
    DEBUG && console.log('filter 1', data, filter)
    let res = data
    if (filter.name) {
      res = res.filter(r => {
        return normalize(r.name).includes(filter.name) || normalize(r.desc).includes(filter.name)
          || r.branchs?.some(b => normalize(b.name).includes(filter.name) || normalize(b.desc).includes(filter.name))
      })
    }
    if (filter.type) {
      res = res.filter(r => filter.type === r.type)
    }
    if (filter.tier?.length) {
      res = res.filter(r => filter.tier.some(tier => r.tier === tier))
    }
    if (filter.ressource?.length) {
      res = res.filter(r => filter.ressource.some(ressource => r[ressource]))
    }
    if (filter.depense?.length) {
      res = res.filter(r => filter.depense.some(depense => r.depense === depense))
    }
    if (filter.equipmentType?.length) {
      res = res.filter(r => filter.equipmentType.some(equipmentType => r[equipmentType]))
    }
    if (filter.rarete?.length) {
      res = res.filter(r => filter.rarete.some(rarete => r.rarete === rarete))
    }
    [
      'generic', 'factorisable', 'cumulative', 
      'instant', 'compose', 'isFavorite'
    ].forEach(f => {
      if (filter[f]) {
        res = res.filter(
          r => r[f] || typeof r[f]==='number' || 
          r.branchs?.find(b => b[f] || typeof b[f]==='number')
        )
      }
    })
    if (filter.single) {
      res = res.filter(r => !r.compose)
    }
    if (filter.jds) {
      res = res.filter(r => {
        return r.tags.find(tag => tag.includes('JdS')) || r.branchs?.includes('JdS')
      })
    }
    DEBUG && console.log('filter 2', res)
    setFiltredData(res)
  }, [data, filter])

  const updateUser = useCallback(value => {
    fetch('/api/user/'+authProps?.user?.id, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(value)
    })
    .then(checkStatus)
    .then(res => {
      const user = res?.find(({id}) => id === authProps?.user?.id)
      setFavs(user[id])
    })
    .catch(err => console.log('error', err))
  }, [authProps?.user?.id, id])

  const removeFav = data => {
    const newFavs = favs.filter(f => f.id !== data.id)
    updateUser({[id]: newFavs})
  }
  
  const addFav = data => {
    const newFavs = [...favs, data]
    updateUser({[id]: newFavs})
  }

  return <Layout title={RULES[id]} className='p-0 pb-2' {...authProps}>
    <div className='flex flex-col'>
      <DataFilter 
        dataSet={id}
        onChange={onInputChange} 
        rules={rules[id]}
        isAdmin={authProps?.user?.isAdmin}
      />
      <section className='flex justify-center flex-wrap p-2'>
        {filtredData.map(data => <Link
          key={data.id}
          href={'#'+data.id}
          className='block mx-1 px-1 my-1 bg-white/30 rounded-md'
          >
            {data.name}
          </Link>
        )}
      </section>
      <section className='xl:grid-cols-2 p-2'>
        {filtredData.map(data => <DataCard
          key={data.id}
          highlighted={data.id === highlightedId}
          favorite={!!favs}
          addFavorite={addFav}
          removeFavorite={removeFav}
          {...data}
        />)}
      </section>
    </div>
  </Layout> 
}

export default Rules
