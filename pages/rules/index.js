import Layout from '@components/Layout'
import { RULES } from '@constants/routes'
import Link from 'next/link'
import useSWR from 'swr'

const fetcher = (...args) => fetch(...args).then(res => res.json())

const rules = Object.keys(RULES).map(e => ({id:e, name:RULES[e]}))


export default function Maps() {
  const { data: authProps = {} } = useSWR('/api/auth', fetcher)

  return (
    <Layout titles='Règles' {...authProps}>
      <div className='stain-7'/>
      <div className='flex items-center justify-center h-full pb-10'>
        <div className='grid grid-cols-2 lg:grid-cols-3 gap-y-8 gap-x-2 md:gap-10 items-center'>
          {rules.map(({id, name}, i) => (
            <Link key={id} href={'rules/'+id} className={i===rules.length-1? 'col-start-2':''}>
              <div className={`frame  text-center WalterTurncoat text-[#524300]`}>
                {name}
              </div>
            </Link>
          ))}
        </div>
      </div>
    </Layout>
  )
}
