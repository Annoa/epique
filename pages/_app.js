import Head from 'next/head'
import { config } from '@fortawesome/fontawesome-svg-core'
import  { Provider as ToastProvider } from '@radix-ui/react-toast'
import { ToastViewport } from '@components/Toast'

import '@public/fonts/fonts.css'
import '@styles/main.css'
import '@fortawesome/fontawesome-svg-core/styles.css'
import 'leaflet/dist/leaflet.css'

config.autoAddCss = false


const App = ({ Component, pageProps }) => {
  const defaultProps = {
    user: null,
    pjs: [],
    maps: [],
    groups: [],
    pnjs: []
  }
  return <>
    <Head>
      <meta name='viewport' content='width=device-width, initial-scale=1' />
    </Head>
    <ToastProvider duration={3000} swipeDirection='right'>
      <Component {...defaultProps} {...pageProps}/>
      <ToastViewport/>
    </ToastProvider>
  </>
}

export default App