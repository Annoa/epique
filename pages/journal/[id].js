import { useCallback, useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { getAuthProps } from '@lib/auth'
import { getJournal, processJournal } from '@lib/data/journals'
import { checkStatus } from '@lib/helpers/fetch'
import { timestamp2display } from '@lib/helpers/date'

import Layout from '@components/Layout'
import Accordion from '@components/Accordion'
import JournalEntry from '@components/JournalEntry'
import JournalModal from '@components/modals/JournalModal'
import Toast from '@components/Toast'
import { faCog } from '@fortawesome/free-solid-svg-icons'

export async function getServerSideProps({ req, res, params }) {
  const props = await getAuthProps(req, res)
  if (!props.user?.isAdmin) {
    return {
      redirect: {
        permanent: false,
        destination: "/",
      },
      props: {},
    }
  }
  const id = params.id
  const group = props.groups?.find(g => g.id === id) || {}
  const journal = await getJournal(props.user, id)
  if (!journal) {
    return {
      redirect: {
        permanent: false,
        destination: "/",
      },
      props: {},
    }
  }
  processJournal(journal)
  return { props: { ...props, group, journal }} 
}


const Journal = ({group, journal, ...props}) => {
  const [openEntries, setOpenEntries] = useState([])
  const [toast, setToast] = useState()
  const [entries, setEntries] = useState(journal)

  const setOpen = useCallback(id => {
    setOpenEntries(prev => {
      const val = new Set([...prev, id])
      return [...val]
    })
  }, [])

  const save = useCallback(entry => {
    setToast()
    const url = `/api/journal/${group.id}`
    fetch(url, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(entry)
    })
    .then(checkStatus)
    .then(res => {
      setToast({
        title: `Sauvegarde réussie`,
        content: `${timestamp2display(entry.timestamp)} - ${entry.title}`,
        success: true
      })
      setEntries(res)
    })
    .catch(err => {
      console.log('error', err)
      setToast({
        title: `Echec de la sauvegarde`,
        content: err.message,
        alert: true
      })
    })
  }, [group.id])

  useEffect(() => {
    console.log('new journal', journal)
  }, [journal])

  return <Layout title={group.name} {...props}>
    <h1 className='flex items-center'>
      Journal: {group.name}
      {props.user.isAdmin && <JournalModal
        entries={entries}
        group={group}
        setEntries={setEntries}
        button={
          <button className='ml-2 btn text-base'>
          <FontAwesomeIcon icon={faCog} />
        </button>
        }
      />}
    </h1>

    <Accordion value={openEntries} onValueChange={setOpenEntries}>
      {entries.map(entry => (
        <JournalEntry 
          key={entry.id} 
          setOpen={setOpen}
          save={save}
          entry={entry} />
      ))}
    </Accordion>
    { toast && <Toast {...toast}/>}
  </Layout> 
}

export default Journal
