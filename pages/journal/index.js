import Layout from '@components/Layout'
import { getAuthProps } from '@lib/auth'
import Link from 'next/link'

export async function getServerSideProps({ req, res }) {
  const props = await getAuthProps(req, res)
  if (!props.user?.isAdmin) {
    return {
      redirect: {
        permanent: false,
        destination: "/",
      },
      props: {},
    }
  }
  return { props }
}

export default function Journals(props) {
  const groups = props.groups || []
  return (
    <Layout name='Journaux de campagne' {...props}>
      <div className='stain-11'/>
      <div className='flex items-center justify-center h-full pb-10'>
        <div className='grid grid-cols-1 gap-10 items-center'>
          {groups.map(({id, name}) => (
            <Link key={id} href={'journal/'+id}>
              <div className={`frame p-5 text-center WalterTurncoat text-[#524300]`}>
                {name}
              </div>
            </Link>
          ))}
        </div>
      </div>
    </Layout>
  )
}
