import Layout from '@components/Layout'
import PnjList from '@components/PnjList'
import { getAuthProps } from '@lib/auth'

export async function getServerSideProps({ query, req, res }) {
  const props = await getAuthProps(req, res)
  const gid = query.gid
  if (
    !props.user?.groupIds?.includes(gid) 
    && !props.user.isAdmin
  ) {
    return {
      redirect: {
        permanent: false,
        destination: '/',
      },
      props: {},
    }
  }
  const pnjs = props.pnjs.filter(p => p.authGroupIds.includes(gid))
  
  
  return { props: {...props, gid, pnjs} }
}

export default function Pnjs({gid, ...props}) {
  const { groups, pnjs, user } = props


  let tabsProps 
  if (user.isAdmin) {
    tabsProps = { rootPath: '/pnjs', tabs: [{path:'', id:'', name:'ALL'}]}
    tabsProps.tabs.push(...groups.map(g => ({path:'/'+g.id, id:g.id, name:g.name})))
  } else if (user.groupIds.length > 1) {
    tabsProps = { rootPath: '/pnjs', tabs: []}
    tabsProps.tabs.push(...user.groupIds.map(id => ({
      path: '/'+id,
      id,
      name: (groups.find(g => id === g.id))?.name
    })))
  }
  
  return (
    <Layout title='Pnjs' {...props} className='px-0' tabsProps={tabsProps}>
      <PnjList 
        gid={gid} 
        pnjs={pnjs} 
        user={user} 
        groups={groups}
        tabs={tabsProps?.tabs}/>
    </Layout>
  )
}
