import Layout from '@components/Layout'
import PnjList from '@components/PnjList'
import { getAuthProps } from '@lib/auth'


export async function getServerSideProps({ req, res }) {
  const props = await getAuthProps(req, res)
  if (!props.user?.isAdmin) {
    const gid = props.user?.groupIds?.[0]
    return {
      redirect: {
        permanent: false,
        destination: gid? `/pnjs/${gid}`:'/',
      },
      props: {},
    }
  }
  return { props }
}

export default function Pnjs(props) {
  const { groups, pnjs, user } = props

  const tabsProps = { rootPath: '/pnjs', tabs: [{path:'', id:'',name:'ALL'}]}
  tabsProps.tabs.push(...groups.map(g => ({path:'/'+g.id, id:g.id, name:g.name})))
  
  return (
    <Layout title='Pnjs' {...props} className='px-0' tabsProps={tabsProps}>
      <PnjList 
        gid=""
        pnjs={pnjs} 
        user={user} 
        groups={groups}
        tabs={tabsProps?.tabs}/>
    </Layout>
  )
}
