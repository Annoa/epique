import Link from 'next/link'
import { deleteCookie } from 'cookies-next'
import { useRouter } from 'next/router'
import Layout from '@components/Layout'
import { getAuthProps } from '@lib/auth'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCrown, faEraser, faFeather, faUserSlash } from '@fortawesome/free-solid-svg-icons'
import { checkStatus } from '@lib/helpers/fetch'

export async function getServerSideProps({ req, res }) {
  const props = await getAuthProps(req, res)
  if (!props.user) {
    return {
      redirect: {
        permanent: false,
        destination: "/signin",
      },
      props: {},
    }
  }
  return { props }
}

const FavCard = ({
  user,
  label,
  dataSet,
  removeFav
}) => <div className='card p-2'>
  <h3>{ label }</h3>
  <ul className='grid grid-cols-2'>
  {user[dataSet].map(({id, name}) => 
    <div key={id} className={'[&:nth-child(4n+1)]:bg-parchment-200/30 [&:nth-child(4n+2)]:bg-parchment-200/30'}>
      <Link href={`/rules/${dataSet}#${id}`}>
        {name}
      </Link>
      <button className='inline-btn ml-1' 
        onClick={() => removeFav(dataSet, id)}>
        <FontAwesomeIcon icon={faEraser}/>
      </button>
    </div>)}
  </ul>
</div>

export default function Profile(props) {
  const router = useRouter()
  
  const signoutHandler = () => {
    deleteCookie("epique-token")
    router.push("/signin")
  }

  const user = props.user || {}
  const groups = props.groups.filter(
    ({id}) => user.groupIds?.includes(id) || user.isAdmin
  )

  const updateUser = value => {
    fetch('/api/user/'+user.id, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(value)
    })
    .then(checkStatus)
    .then(() => {
      router.replace(router.asPath)
    })
    .catch(err => console.log('error', err))
  }

  const removeFav = (dataSet, id) => {
    const newFavs = user[dataSet].filter(f => f.id !== id)
    updateUser({[dataSet]: newFavs})
  }

  const onColorChange = e => {
    updateUser({color: e.target.value})
  }

  return (
    <Layout title={user.pseudo} {...props}>
      <h1 className='flex items-center'>
        {user.isAdmin &&
          <FontAwesomeIcon icon={faCrown} className='mr-1'/>
        }
        {user.pseudo}
        <button className='btn ml-2' onClick={signoutHandler}>
          <FontAwesomeIcon size='xl' icon={faUserSlash}/>
        </button>
      </h1>

      { groups.length && <div>
        <FontAwesomeIcon icon={faFeather} className='mr-1'/>
        {groups.map(g => g.name).join(', ')}
      </div>}

      <div className='flex items-center'>
        <input type='color' defaultValue={user.color} 
          className='mr-1' onChange={onColorChange}/>
        Ping
      </div>

      <div className='cards-container mt-4'>
        <FavCard user={user} removeFav={removeFav}
          label='Racines favorites' dataSet='capacities'/>
        <FavCard user={user} removeFav={removeFav}
          label='Compétences favorites' dataSet='abilities'/>
        <FavCard user={user} removeFav={removeFav}
          label='Dons favoris' dataSet='feats'/>
        <FavCard user={user} removeFav={removeFav}
          label='Équipements favoris' dataSet='equipments'/>
      </div>

      
    </Layout>
  )
}
