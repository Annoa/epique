import Link from 'next/link'
import Layout from '@components/Layout'
import { getAuthProps } from '@lib/auth'
import { faDAndD } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export async function getServerSideProps({ req, res }) {
  const props = await getAuthProps(req, res)
  return { props }
}

// background: 'linear-gradient(180deg, rgb(123,42,28) 0%, rgb(251,204,18) 60%, rgb(251,204,18) 100%)',
// '-webkit-background-clip': 'text',
// '-webkit-text-fill-color': 'transparent'  

// text-[#7B2A1C]  stroke-[10px] stroke-[#7B2A1C]

export default function Home(props) {
  const myId = props.user?.id
  const isAdmin = props.user?.isAdmin
  const groups = props.groups||[]
  const pjs = (props.pjs||[]).filter(
    ({userId}) => userId === myId
  )

  return (
    <Layout {...props}>
      <div className='stain-9'/>
      <div className='full-screen-center'>
        <div className='grid grid-cols-2 gap-y-8 gap-x-2 md:gap-10 items-center'>
          <h1 className='app-title mb-8'>
            <FontAwesomeIcon icon={faDAndD} size='lg'/>
            Epique
          </h1>
          {!myId && <Link href='signin'>
            <div className='frame  text-center WalterTurncoat text-[#524300]'>
              Connexion
            </div>
          </Link>}
          {pjs.map(({id, name}) => (
            <Link key={id} href={'sheet/pj/'+id}>
              <div className='frame  text-center WalterTurncoat text-[#524300]'>
                {name}
              </div>
            </Link>
          ))}
          <Link href='rules'>
            <div className='frame  text-center WalterTurncoat text-[#524300]'>
              Règles
            </div>
          </Link>
          {isAdmin && <Link href='sheet/pj'>
            <div className='frame  text-center WalterTurncoat text-[#524300]'>
              Fiches
            </div>
          </Link>}
          {isAdmin && groups.map(({id, name}) => (
            <Link href={'sheet/'+id} key={id}>
              <div className='frame  text-center WalterTurncoat text-[#524300]'>
                {name}
              </div>
            </Link>
          ))}
          

        </div>
      </div>
    </Layout>
  )
}
