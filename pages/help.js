import Layout from '@components/Layout'
import { getAuthProps } from '@lib/auth'

export async function getServerSideProps({ req, res }) {
  const props = await getAuthProps(req, res)
  return { props }
}

export default function Help(props) {
  return (
    <Layout {...props}>
      <h1>Help</h1>
    </Layout>
  )
}
