/* eslint-disable react/no-unescaped-entities */
import { useState } from 'react'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { getAuthProps } from '@lib/auth'
import { getUsers } from '@lib/data/users'

import Accordion, { AccordionItem } from '@components/Accordion'
import Layout from '@components/Layout'
import GroupModal from '@components/modals/GroupModal'
import MapModal from '@components/modals/MapModal'
import PjModal from '@components/modals/PjModal'
import UserModal from '@components/modals/UserModal'
import DeleteAdminButton from '@components/DeleteAdminAlert'
import PasswordModal from '@components/modals/PasswordModal'
import { 
  faKey, faPencil, faPlus,  
} from '@fortawesome/free-solid-svg-icons'

export async function getServerSideProps({ req, res }) {
  const props = await getAuthProps(req, res)
  if (!props.user?.isAdmin) {
    return {
      redirect: {
        permanent: false,
        destination: "/",
      },
      props: {},
    }
  }
  const [users] = await Promise.all([
    getUsers(false)
  ])
  return { props: { ...props, users } }
}

export default function Home(props) {
  const { groups, users, pjs, maps } = props
  const [openItems, setOpenItems] = useState([
    'Groups', 'Users', 'Fiches', 'Maps'
  ])
  const [selectedItem, setSelectedItem] = useState({})
  return (
    <Layout {...props}>
      <h1>Panneau d'administration</h1>
      <Accordion value={openItems} onValueChange={setOpenItems}>

        <AccordionItem title='Groups' className={{content:'overflow-x-auto'}}>
          <table className='w-full nowrap hover'>
            <thead>
              <tr>
                <th>id</th>
                <th>name</th>
              </tr>
            </thead>
            <tbody>
              {groups?.map(group => <tr key={group.id}>
                <td>{group.id}</td>
                <td>{group.name}</td>
                <td className='text-right'>
                  <button className='btn mr-1' 
                    onClick={() => setSelectedItem({group})}>
                    <FontAwesomeIcon icon={faPencil}/>
                  </button>
                  
                  <DeleteAdminButton 
                    data='group' 
                    id={group.id} 
                    name={group.name}
                    deleteAllFilesOption/>
                </td>
              </tr>)}
            </tbody>
          </table>
          <button className='btn'
            onClick={() => setSelectedItem({newGroup:true})}>
            <FontAwesomeIcon icon={faPlus} className='mr-1'/>
            Nouveau groupe
          </button>
        </AccordionItem>

        <AccordionItem title='Users' className={{content:'overflow-x-auto'}}>
          <table className='w-full nowrap hover'>
            <thead>
              <tr>
                <th>id</th>
                <th>pseudo</th>
                <th>isAdmin</th>
                <th>discordId</th>
                <th>groupIds</th>
              </tr>
            </thead>
            <tbody>
              {users?.map(u => <tr key={u.id}>
                <td>{u.id}</td>
                <td>{u.pseudo}</td>
                <td>{u.isAdmin?'true':''}</td>
                <td>{u.discordId}</td>
                <td>{u.groupIds?.join(', ')}</td>
                <td className='text-right'>
                  <button className='btn mr-1'
                    onClick={() => setSelectedItem({pwd:u})}>
                    <FontAwesomeIcon icon={faKey}/>
                  </button>
                  <button className='btn mr-1' 
                    onClick={() => setSelectedItem({user:u})}>
                    <FontAwesomeIcon icon={faPencil}/>
                  </button>
                  
                  <DeleteAdminButton 
                    data='user' 
                    id={u.id} 
                    name={u.pseudo}/>
                </td>
              </tr>)}
            </tbody>
          </table>
          <button className='btn'
            onClick={() => setSelectedItem({newUser:true})}>
            <FontAwesomeIcon icon={faPlus} className='mr-1'/>
            Nouvel utilisateur
          </button>
        </AccordionItem>

        <AccordionItem title='Fiches' className={{content:'overflow-x-auto'}}>
          <table className='w-full nowrap hover'>
            <thead>
              <tr>
                <th>id</th>
                <th>shortName</th>
                <th>name</th>
                <th>userId</th>
                <th>allowedUsers</th>
              </tr>
            </thead>
            <tbody>
              {pjs?.map(pj => <tr key={pj.id}>
                <td>{pj.id}</td>
                <td>{pj.shortName}</td>
                <td>{pj.name}</td>
                <td>{pj.userId}</td>
                <td>{pj.allowedUsers.join(', ')}</td>
                <td className='text-right'>
                  <button className='btn mr-1'
                    onClick={() => setSelectedItem({pj})}>
                    <FontAwesomeIcon icon={faPencil}/>
                  </button>
                  <DeleteAdminButton 
                    data='pj' 
                    id={pj.id} 
                    name={pj.shortName}
                    deleteAllFilesOption/>
                </td>
              </tr>)}
            </tbody>
          </table>
          <button className='btn'
            onClick={() => setSelectedItem({newPj:true})}>
            <FontAwesomeIcon icon={faPlus} className='mr-1'/>
            Nouvelle fiche
          </button>
        </AccordionItem>

        <AccordionItem title='Maps' className={{content:'overflow-x-auto'}}>
          <table className='w-full nowrap hover'>
            <thead>
              <tr>
                <th>id</th>
                <th>name</th>
                <th>authGroupIds</th>
                <th>width (px)</th>
                <th>height (px)</th>
                <th>bg (public/images/map/bg)</th>
              </tr>
            </thead>
            <tbody>
              {maps?.map(map => <tr key={map.id}>
                <td>{map.id}</td>
                <td>{map.name}</td>
                <td>{map.authGroupIds.join(', ')}</td>
                <td>{map.width}</td>
                <td>{map.height}</td>
                <td>{map.bg}</td>
                <td className='text-right'>
                  <button className='btn mr-1'
                    onClick={() => setSelectedItem({map})}>
                    <FontAwesomeIcon icon={faPencil}/>
                  </button>
                  <DeleteAdminButton 
                    data='map' 
                    id={map.id} 
                    name={map.name}
                    deleteAllFilesOption/>
                </td>
              </tr>)}
            </tbody>
          </table>
          <button className='btn'
            onClick={() => setSelectedItem({newMap:true})}>
            <FontAwesomeIcon icon={faPlus} className='mr-1'/>
            Nouvelle carte
          </button>
        </AccordionItem>
      </Accordion>

      <GroupModal
        isNew={selectedItem.newGroup}
        group={selectedItem.group}
        setSelectedItem={setSelectedItem}
      />
      <UserModal
        isNew={selectedItem.newUser}
        user={selectedItem.user}
        setSelectedItem={setSelectedItem}
        groups={groups}/>
      <PjModal
        isAdmin
        isNew={selectedItem.newPj}
        pj={selectedItem.pj}
        setSelectedItem={setSelectedItem}
        users={users}/>
      <MapModal
        isAdmin
        isNew={selectedItem.newMap}
        map={selectedItem.map}
        setSelectedItem={setSelectedItem}
        groups={groups}/>
      <PasswordModal
        user={selectedItem.pwd}
        setSelectedItem={setSelectedItem}/>
    </Layout>
  )
}
