import { SheetProvider } from '@contexts/sheetCtx.js'
import { getServerSideProps } from './index.js'
import { SHEET_TABS } from '@constants/routes'

import Layout from '@components/Layout'
import Sidebar from '@components/Sheet/Sidebar'
import ArraySection from '@components/Sheet/ArraySection'
import Capacity from '@components/Sheet/Capacity'
import Equipment from '@components/Sheet/Equipment'
import TextSection from '@components/Sheet/TextSection'
import Efficiencies from '@components/Sheet/Options/Efficiencies'
import Factors from '@components/Sheet/Options/Factors'
import LimitsRank from '@components/Sheet/Options/LimitsRank'
import RecoveriesRank from '@components/Sheet/Options/RecoveriesRank'
import RecoveriesIntervals from '@components/Sheet/Options/RecoveriesIntervals.js'


export { getServerSideProps }

export default function SheetTab(props) {
  const { perso, users, user, type, tab } = props
  return <SheetProvider>
    <Layout 
      title={perso.shortName} {...props} 
      sidebar={<Sidebar 
        type={type} 
        tab={tab}
        perso={perso} 
        users={users} 
        user={user}/>}
      tabsProps={{
        rootPath: `/sheet/${type}/${perso.id}`, 
        tabs: SHEET_TABS
      }}
    >
      { tab === 'capacities' ?
        <ArraySection 
          dataPath='capacities'
          renderItem={props => <Capacity {...props}/>}
        />
      : tab === 'equipments'?
        <ArraySection 
          dataPath='equipments'
          renderItem={props => <Equipment {...props}/>}
        />
      : tab === 'notes' ?
        <TextSection 
          dataPath='notes'
        />
      : tab === 'options' ?
        <div className='lg:grid lg:grid-cols-2 items-start'>
          <LimitsRank/>
          <Factors />
          <RecoveriesRank />
          <RecoveriesIntervals />
          <Efficiencies />
        </div>
      : null
      } 
      
    </Layout> 
  </SheetProvider>
}