import { getAuthProps } from '@lib/auth'
import { getUsers } from '@lib/data/users'

import { SheetProvider } from 'contexts/sheetCtx'
import { SHEET_TABS } from '@constants/routes'
import { getRulesById } from '@lib/data/rules'

import Layout from '@components/Layout'
import Sidebar from '@components/Sheet/Sidebar'
import Ressources from '@components/Sheet/Ressources'
import Health from '@components/Sheet/Health'
import TextSection from '@components/Sheet/TextSection'
import Recoveries from '@components/Sheet/Recoveries'
import Xp from '@components/Sheet/Xp'

export async function getServerSideProps({ req, res, params }) {
  const props = await getAuthProps(req, res)
  const users = await getUsers(true)
  const xpRules = await getRulesById('xp')
  const xp = xpRules.xp.data.reduce((a, b) => ({
    ...a, 
    [b.id]: b
  }), {})
  const { type, id } = params
  let redirect = false
  if (type === 'pnj' && !props.user.isAdmin) {
    redirect = true
  }
  const perso = props[type+'s']?.find(s => s.id === id)
  if (!perso) {
    redirect = true
  }
  if (type === 'pnj' && !perso.hasSheet) {
    redirect = true
  }
  if (redirect) {
    return {
      redirect: {
        permanent: false,
        destination: "/",
      },
      props: {},
    }
  }
  return { props: { ...props, perso, users, ...params, xp  }} 
}

export default function Sheet(props) {
  const { perso, users, user, type, xp } = props
  return <SheetProvider>
    <Layout 
      title={perso.shortName} {...props} 
      sidebar={<Sidebar type={type} perso={perso} users={users} user={user}/>}
      tabsProps={{
        rootPath: `/sheet/${type}/${perso.id}`, 
        tabs: SHEET_TABS
      }}
    >
      <h1>{perso?.name}</h1>
      <div className='xl:grid xl:grid-cols-2'>    
        <TextSection dataPath='txt-start'/>
        <Health/>
        <Ressources/>
        <Recoveries/>
        <Xp xpList={xp}/>
        <TextSection dataPath='roots' parser='roots'/>
        <TextSection dataPath='feats' parser='feats'/>
        <TextSection dataPath='traits'/>
        <TextSection
          dataPath='abilities' 
          className='col-span-2'
        />
        <TextSection dataPath='archetypes' parser='archetypes'/>
      </div>
    </Layout> 
  </SheetProvider>
}