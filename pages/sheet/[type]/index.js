import Link from 'next/link'
import Layout from '@components/Layout'
import { getAuthProps } from '@lib/auth'

export async function getServerSideProps({ req, res, params }) {
  const props = await getAuthProps(req, res)
  const { type } = params
  if (type !== 'pj') {
    return {
      redirect: {
        permanent: false,
        destination: '/',
      },
      props: {}
    }
  }
  return { props }
}

export default function Sheets(props) {
  const pjs = props.pjs || []
  const myId = props.user?.id
  return (
    <Layout titles='Fiches' {...props}>
      <div className='stain-3'/>
      <div className='flex items-center justify-center h-full pb-10'>
        <div className='grid grid-cols-2 gap-y-8 gap-x-2 md:gap-10 items-center'>
          {pjs.map(({id, name, userId}) => (
            <Link key={id} href={'/sheet/pj/'+id}>
              <div className={`
                frame  text-center WalterTurncoat text-[#524300]
                ${userId === myId? 'font-extrabold text-shadow':''}
              `}>
                {name}
              </div>
            </Link>
          ))}
        </div>
      </div>
    </Layout>
  )
}