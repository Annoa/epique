import { useRouter } from 'next/router'
import { useReducer, useState } from 'react'
import CryptoJS from 'crypto-js'
import sha256 from 'crypto-js/sha256'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { checkStatus } from '@lib/helpers/fetch'
import { getAuthProps } from '@lib/auth'
import { getUsers } from '@lib/data/users'

import Layout from '@components/Layout'
import Toast from '@components/Toast'
import { faDAndD } from '@fortawesome/free-brands-svg-icons'
import { 
  faEye, faEyeSlash, faUser 
} from '@fortawesome/free-solid-svg-icons'


export async function getServerSideProps({ req, res }) {
  const props = await getAuthProps(req, res)
  if (props.user) {
    return {
      redirect: {
        permanent: false,
        destination: '/',
      },
      props: {},
    }
  }
  props.users = await getUsers(true)
  return { props }
}

export default function SigninPage({ users }) {
  const router = useRouter()
  const [id, setId] = useState('')
  const [password, setPassword] = useState('')
  const [showPwd, toggleShowPwd] = useReducer(prev => !prev, false)
  const [toast, setToast] = useState()

  const signin = async e => {
    e.preventDefault()
    setToast()
    if (!id) return
    if (!password) {
      setToast({
        title: 'Connexion',
        content: 'Le mooot de paaaaasse',
        alert: true
      })
      return
    }
    const body = {
      id,
      password: sha256(password).toString(CryptoJS.enc.Base64)
    }
    fetch('/api/signin', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body)
    }).then(checkStatus)
    .then(() => router.push('/'))
    .catch(err => {
      console.log('error', err)
      setToast({
        title: 'Connexion',
        content: err.message,
        alert: true
      })
    })
  }

  return (
    <Layout title='Connexion' className='p-2 flex flex-col items-center justify-center'>
      <div className='stain-2'/>

      <h1 className='app-title mb-8'>
        <FontAwesomeIcon icon={faDAndD} size='lg'/>
        Epique
      </h1>

      <form onSubmit={signin} className='max-w-[600px] mb-4'>
        <div className='columns-2 mb-4'>
          {users?.map(({id, pseudo})=> <div key={id}>
            <label>
              <input type='radio' name='id' className='mr-1'
                value={id}
                onChange={e => setId(e.target.value)}/>
              { pseudo }
            </label>
          </div>)}
        </div>

        <div className='flex items-center mb-4 p-1 rounded-sm bg-ink-500/20'>
          <input 
            value={password} 
            name='password'
            type={showPwd?'text':'password'} 
            autoComplete='off'
            required
            onChange={e => setPassword(e.target.value)}/>
          <button className='inline-btn ml-1' type='button' 
            onClick={toggleShowPwd}>
            <FontAwesomeIcon icon={showPwd?faEyeSlash:faEye} />
          </button>
        </div>

        <div className='mb-4 text-center'>
          <button type='submit' className='btn' 
            disabled={!id} onClick={signin}>
            <FontAwesomeIcon icon={faUser} className='mr-1'/>
            Se connecter
          </button>
        </div>
      </form>
      { toast && <Toast {...toast}/>}
    </Layout>
  )
}