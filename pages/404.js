import Layout from '@components/Layout'
import useSWR from 'swr'

const fetcher = (...args) => fetch(...args).then(res => res.json())


export default function PageNotFound() {
  const { data: authProps = {} } = useSWR('/api/auth', fetcher)

  return (
    <Layout {...authProps}>
      <div className='stain-5'/>
      <div className='full-screen-center WalterTurncoat text-7xl text-ink-900'>
        404
      </div>
    </Layout>
  )
}
