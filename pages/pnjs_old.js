import Layout from '@components/Layout'
import { getAuthProps } from '@lib/auth'

import { useRouter } from 'next/router'


export async function getServerSideProps({ req, res }) {
  const props = await getAuthProps(req, res)
  const { user, groups } = props
  if (!user) {
    return {
      redirect: {
        permanent: false,
        destination: "/signin",
      },
      props: {},
    }
  }
  let tabsProps
  if (user.isAdmin) {
    tabsProps = { rootPath: '/pnjs', tabs: [{path:'', name:'ALL'}]}
    tabsProps.tabs.push(...groups.map(g => ({path:g.id, name:g.name})))
  }
  return { props: {...props, tabsProps} }
}

export default function Pnjs({tabsProps, ...props}) {
  const { pathname, query } = useRouter()
  const gid = query.id
  

  
  return (
    <Layout title='Pnjs' {...props} className='px-0' tabsProps={tabsProps}>
      
    </Layout>
  )
}
