import { getFile, saveFile } from './api'


export const getUsers = async short => {
  const file = await getFile({path:'users.json'})
  const users = (JSON.parse(file.body))
    .sort((a, b) => a.pseudo.localeCompare(b.pseudo))
  global.USERS = users
  if (short)
    return users.map(u => ({
      id: u.id, 
      pseudo: u.pseudo, 
      isAdmin: !!u.isAdmin,
      color: u.color || '#ffffff'
    }))
  return users
}

export const addUser = async user => {
  if (!user.id || !user.pseudo) return []
  try {
    const file = await getFile({path:'users.json'})
    const users = JSON.parse(file.body)
    users.push({
      discordId: "",
      groupIds: [],
      isAdmin: false,
      color: '#ffffff',
      abilities: [],
      capacities: [],
      equipments: [],
      feats: [],
      ...user
    })
    await saveFile({
      path: 'users.json',
      data: JSON.stringify(users)
    })
    global.USERS = users
    return users
  } catch (err)  {
    console.error(err)
    return []
  }
}

export const updateUser = async (id, user) => {
  try {
    const file = await getFile({path:'users.json'})
    let users = JSON.parse(file.body)
    users = users.map(u => u.id === id ? 
      {...u, ...user} : u
    )
    await saveFile({
      path: 'users.json',
      data: JSON.stringify(users)
    })
    return users
  } catch (err)  {
    console.error(err)
  }
}

export const deleteUser = async id => {
  try {
    const files = await Promise.all([
      getFile({path:'users.json'}),
      getFile({path:'pjs.json'}),
    ])
    let [
      users, pjs
    ] = files.map(f => JSON.parse(f.body))
    users = users.filter(u => u.id !== id)
    pjs = pjs.map(s => ({
      ...s,
      userId: s.userId === id? null : s.userId,
      allowedUsers: s.allowedUsers.filter(u => u.id !== id)
    }))
    await Promise.all([
      saveFile({path:'users.json', data:JSON.stringify(users)}),
      saveFile({path:'pjs.json', data:JSON.stringify(pjs)})
    ])
    return users
  } catch (err)  {
    console.error(err)
    return []
  }
}