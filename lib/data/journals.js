import { getFile, saveFile } from './api'
const showdown  = require('showdown')

const converter =  new showdown.Converter({
  disableForced4SpacesIndentedSublists: true,
  simpleLineBreaks: true, 
  backslashEscapesHTMLTags: true,
  emoji: true
})

// const entry = {
//   id : '',
//   timestamp: '',
//   title: '',
//   content: ''
// }

export const getJournal = async (user, id) => {
  try {
    if (!user.isAdmin && !user.groupIds?.includes(id)) {
      return
    }
    const file = await getFile({path:`journals/${id}.json`})
    const journal = JSON.parse(file.body)
    return journal
  } catch (err) {
    console.error(err)
    return null
  }
}

export const updateJournalEntry = async (id, entry) => {
  try {
    const file = await getFile({path:`journals/${id}.json`})
    let journal = JSON.parse(file.body)
    journal = journal.map(e => e.id === entry.id ? 
      {content:'', ...e, ...entry } : e
    )
    await saveFile({
      path:`journals/${id}.json`,
      data: JSON.stringify(journal)
    })
    return journal
  } catch (err)  {
    console.error(err)
    return []
  }
}

export const addJournalEntry = async (id, entry = {}) => {
  try {
    const file = await getFile({path:`journals/${id}.json`})
    const journal = JSON.parse(file.body)
    journal.push({
      id: ''+Date.now(),
      timestamp: entry.timestamp || Date.now(),
      title: entry.title || 'Titre',
      content: ''
    })
    await saveFile({
      path:`journals/${id}.json`,
      data: JSON.stringify(journal)
    })
    return journal
  } catch (err)  {
    console.error(err)
    return []
  }
}

export const deleteJournalEntry = async (id, entry) => {
  try {
    const file = await getFile({path:`journals/${id}.json`})
    let journal = JSON.parse(file.body)
    journal = journal.filter(e => e.id !== entry.id)
    await saveFile({
      path:`journals/${id}.json`,
      data: JSON.stringify(journal)
    })
    return journal
  } catch (err)  {
    console.error(err)
    return []
  }
}

export const processJournal = journal => {
  journal.sort((a, b) => b.timestamp - a.timestamp)
  journal.forEach(entry => {
    entry._html = converter.makeHtml(entry.content)
  })
}