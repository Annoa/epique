import { getFile } from './api'
import { RULES } from '@constants/routes'

export const getRulesById = async id => {
  const file = await getFile({path: id+'.json'})
  let json = JSON.parse(file.body)
  json.data.sort((a,b) => a.name.localeCompare(b.name)) // todo in parser
  return {[id]: json}
}

export const getRules = async () => {
  const jsons = await Promise.all(
    Object.keys(RULES).map(getRulesById)
  )
  const data = jsons
    .reduce((acc, id) => ({...acc, ...id}), {})

  return data
}

