import { deleteFile, getFile, saveFile } from './api'

export const getGroups = async () => {
  try {
    const file = await getFile({path:'groups.json'})
    const groups = JSON.parse(file.body)
    return groups
  } catch (err) {
    console.error(err)
    return []
  }
}

export const updateGroup = async (id, group) => {
  try {
    const file = await getFile({path:'groups.json'})
    let groups = JSON.parse(file.body)
    groups = groups.map(g => g.id === id ? 
      {...g, ...group} : g
    )
    await saveFile({
      path: 'groups.json',
      data: JSON.stringify(groups)
    })
    return groups
  } catch (err)  {
    console.error(err)
    return []
  }
}

export const addGroup = async group => {
  if (!group.id) return []
  try {
    const file = await getFile({path:'groups.json'})
    const groups = JSON.parse(file.body)
    groups.push(group)
    await Promise.all([
      saveFile({
        path: 'groups.json',
        data: JSON.stringify(groups)
      }),
      saveFile({
        path: `journals/${group.id}.json`,
        data: JSON.stringify([])
      }),
    ])
    return groups
  } catch (err)  {
    console.error(err)
    return []
  }
}

const deleteKey = (obj, key) => {
  delete obj[''+key]
  return obj
}

export const deleteGroup = async (id, deleteAllFiles = false) => {
  try {
    const files = await Promise.all([
      getFile({path:'groups.json'}),
      getFile({path:'maps.json'}),
      getFile({path:'users.json'}),
      getFile({path:'pnjs.json'}),
    ])
    let [
      groups, maps, users, pnj
    ] = files.map(f => JSON.parse(f.body))
    
    groups = groups.filter(g => g.id !== id)
    maps = maps.map(m => ({
      ...m,
      authGroupIds: m.authGroupIds.filter(gid => gid !== id),
    }))
    users = users.map(u => ({
      ...u,
      groupIds: u.groupIds?.filter(gid => gid !== id) || [],
    }))
    pnj = pnj.map(p => ({
      ...p,
      authGroupIds: p.authGroupIds.filter(gid => gid !== id),
      groupComments: deleteKey(p.groupComments, id),
      groupDesc: deleteKey(p.groupDesc, id),
    }))
    await Promise.all([
      saveFile({path:'groups.json', data:JSON.stringify(groups)}),
      saveFile({path:'maps.json', data:JSON.stringify(maps)}),
      saveFile({path:'users.json', data:JSON.stringify(users)}),
      saveFile({path:'pnjs.json', data:JSON.stringify(pnj)}),
    ])
    if (deleteAllFiles) {
      await deleteFile({path: `journals/${id}.json`})
    }
    return pnj
  } catch (err)  {
    console.error(err)
    return []
  }
}