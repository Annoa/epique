const AWS = require('aws-sdk')
const fs = require('fs')
const fsp = fs.promises
const p = require('path')

// doc: https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html

const s3  = new AWS.S3({
  accessKeyId: process.env.BUCKETEER_AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.BUCKETEER_AWS_SECRET_ACCESS_KEY,
  region: 'us-east-1',
})

const DATA_PATH = p.join(process.cwd(), 'data')
const AWS_PATH = 'public/data/'

const local = process.env.NODE_ENV === 'development'

const getFile = async ({path}) => {
  const file = {body:'', lastModified:0, lastAuthor:''}
  if (local) {
    const data = await fsp.readFile(p.join(DATA_PATH,path), {encoding:'utf-8'})
    const stats = fs.statSync(p.join(DATA_PATH,path))
    file.body = data.toString()
    file.lastModified = stats.mtime
  } else {
    const data = await (s3.getObject({
      Key: AWS_PATH+path,
      Bucket: process.env.BUCKETEER_BUCKET_NAME
    }).promise())
    file.body = data.Body.toString()
    file.lastModified = data.LastModified
  }
  return file
}

// todo: lastModified alert ?
const saveFile = async ({
  path, data, lastModified, author, ifNotExist
}) => {
  const ok = ifNotExist? !(await exists({ path })) : true
  if (!ok) return
  if (local) {
    await fsp.writeFile(p.join(DATA_PATH,path), data)
  } else {
    await (s3.putObject({
      Key: AWS_PATH+path,
      Bucket: process.env.BUCKETEER_BUCKET_NAME,
      Body: Buffer.from(data)
    }).promise())
  }
  return {
    body: data,
    lastModified: Date.now()
  }
}

const mkdir = ({path}) => {
  if (local && !fs.existsSync(p.join(DATA_PATH, path))) {
    return fsp.mkdir(p.join(DATA_PATH, path))
  }
  return
}

const exists = async ({path}) => {
  try {
    if (local) {
      await fsp.access(p.join(DATA_PATH, path))
    } else {
      await getFile({path})
    }
    return true
  } catch (err) {
    return false
  }
}

const deleteFolder = async ({path}) => {
  try {
    if (local) {
      return await fsp.rm(
        p.join(DATA_PATH, path), 
        { recursive: true, force: true }
      )
    }
    await (s3.listObjects({
      Prefix: AWS_PATH+path,
      Bucket: process.env.BUCKETEER_BUCKET_NAME,
    }).promise())
    console.log('deleteFolder', objects)
    if (!objects.Contents.length) return new Promise()
    return await (s3.deleteObjects({
      Delete: objects.Contents,
      Bucket: process.env.BUCKETEER_BUCKET_NAME,
    }).promise())
  } catch (err) {
    return console.error(err)
  }
}



const deleteFile = async ({path}) => {
  try {
    if (local) {
      return await fsp.unlink(p.join(DATA_PATH, path))
    }
    return await (s3.deleteObject({
      Key: AWS_PATH+path,
      Bucket: process.env.BUCKETEER_BUCKET_NAME
    }).promise())
  } catch (err) {
    return console.error(err)
  }
}

const rename = async ({oldPath, newPath}) => {
  const pOld = p.join(DATA_PATH, oldPath)
  const pNew = p.join(DATA_PATH, newPath)
  if (local) {
    return await fsp.rename(pOld, pNew)
  } else {
    await (s3.copyObject({
      Bucket: process.env.BUCKETEER_BUCKET_NAME,
      CopySource: `/${process.env.BUCKETEER_BUCKET_NAME}/${pOld}`, 
      Key: pNew
     })).promise()
    return deleteFile({path: pOld})
  }
}

module.exports = {
  getFile,
  saveFile,
  mkdir,
  exists,
  deleteFile,
  deleteFolder,
  rename
}