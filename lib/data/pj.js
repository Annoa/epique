import { deleteFile, getFile, saveFile } from './api'
import { sheetModel } from './models/sheetModel'


export const getPjs = async user => {
  const file = await getFile({path:'pjs.json'})
  const pjs = (JSON.parse(file.body))
    .sort((a, b) => a.name.localeCompare(b.name))
  if (user.isAdmin) {
    return pjs
  }
  return pjs.filter(s => (
    s.userId === user.id || s.allowedUsers.includes(user.id)
  ))
}

export const addPj = async sheet => {
  if (!sheet.id || !sheet.name || !sheet.shortName) return []
  const file = await getFile({path:'pjs.json'})
  let pjs = JSON.parse(file.body)
  pjs.push({
    allowedUsers: [],
    ...sheet
  })
  await Promise.all([
    saveFile({
      path: 'pjs.json', 
      data: JSON.stringify(pjs)
    }),
    saveFile({
      path: `sheets/${sheet.id}.json`,
      data: JSON.stringify(sheetModel), 
      ifNotExist: true
    })
  ])
  return pjs
}

export const updatePj = async (id, body) => {
  const file = await getFile({path:'pjs.json'})
  let pjs = JSON.parse(file.body)
  pjs = pjs.map(s => (
    s.id === id? {...s, ...body} : s
  ))
  if (body.id && body.id !== id) {
    await rename({
      oldPath: `sheets/${id}.json`,
      newPath: `sheets/${body.id}.json`
    })
  }
  await saveFile({
    path: 'pjs.json', 
    data: JSON.stringify(pjs)
  })
  return pjs
}

export const deletePj = async (id, deleteAllFiles = false) => {
  try {
    const file = await getFile({path:'pjs.json'})
    let pjs = JSON.parse(file.body)
    pjs = pjs.filter(m => m.id !== id)
    await saveFile({
      path: 'pjs.json',
      data: JSON.stringify(pjs)
    })
    if (deleteAllFiles) {
      await deleteFile({path: `pjs/${id}.json`})
    }
    return pjs
  } catch (err)  {
    console.error(err)
    return []
  }
}
