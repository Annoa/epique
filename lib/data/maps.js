import { deleteFolder, getFile, mkdir, saveFile } from './api'

const GEOJSON = {"type":"FeatureCollection","features":[]}

export const getMaps = async user => {
  try {
    const file = await getFile({path:'maps.json'})
    const maps = JSON.parse(file.body)
    if (user.isAdmin) {
      return maps
    }
    return maps
      .filter(m => m.authGroupIds.some(g => user.groupIds.includes(g)))
      .sort((a, b) => a.name.localeCompare(b.name))
  } catch (err) {
    console.error(err)
    return []
  }
}

export const getMap = async (id, user) => {
  const maps = await getMaps(user)
  const map = maps.find(m => m.id === id)
  if (!map) return null
  try {
    const [groupsFile, geojsonFile] = await Promise.all([
      getFile({path: `maps/${id}/groups.json`}),
      getFile({path: `maps/${id}/map.geojson`}),
    ])
    const groups = JSON.parse(groupsFile.body)
    const geojson = JSON.parse(geojsonFile.body)
    return { map, groups, geojson }
  } catch (e) {
    return null
  } 
}

export const addMap = async map => {
  if (!map.id || !map?.name) return []
  try {
    const file = await getFile({path:'maps.json'})
    const maps = JSON.parse(file.body)
    const mapExists = maps.find(m => m.id === map.id)
    if (mapExists) return []

    maps.push({
      authGroupIds: [],
      height: 1000,
      width: 1000,
      ...map
    })
    const data = JSON.stringify(maps)
    const dataGroups = JSON.stringify([])
    const dataGeojson = JSON.stringify(GEOJSON)
    const dataPath = `maps/${map.id}/`
    await mkdir( {path: dataPath })
    await Promise.all([
      saveFile({ path: 'maps.json', data }),
      saveFile({ path: dataPath+'groups.json', data: dataGroups, ifNotExist: true }),
      saveFile({ path: dataPath+'map.geojson', data: dataGeojson, ifNotExist: true }),
    ])
    return maps
  } catch (err)  {
    console.error(err)
    return []
  }
}

export const deleteMap = async (id, deleteAllFiles = false) => {
  try {
    const file = await getFile({path:'maps.json'})
    let maps = JSON.parse(file.body)
    maps = maps.filter(m => m.id !== id)
    await saveFile({
      path: 'maps.json',
      data: JSON.stringify(maps)
    })
    if (deleteAllFiles) {
      await deleteFolder({path: `maps/${id}`})
    }
    return maps
  } catch (err)  {
    console.error(err)
    return []
  }
}

export const updateMap = async (id, map) => {
  try {
    const file = await getFile({path:'maps.json'})
    let maps = JSON.parse(file.body)
    maps = maps.map(m => m.id === id ? 
      {...m, ...map} : m
    )
    await saveFile({
      path: 'maps.json',
      data: JSON.stringify(maps)
    })
    return maps
  } catch (err)  {
    console.error(err)
    return []
  }
}

export const saveMapGeojson = async (id, geojson) => {
  try {
    if (!geojson?.type === 'FeatureCollection' || !Array.isArray(geojson?.features)) {
      return
    }
    await saveFile({
      path: `maps/${id}/map.geojson`,
      data: JSON.stringify(geojson)
    })
    return geojson
  } catch (err)  {
    console.error(err)
  }
}

export const saveMapGroups = async (id, groups) => {
  try {
    if (!Array.isArray(groups)) {
      return
    }
    await saveFile({
      path: `maps/${id}/groups.json`,
      data: JSON.stringify(groups)
    })
    return groups
  } catch (err)  {
    console.error(err)
  }
}