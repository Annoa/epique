var toType = function(obj) {
  return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
}

export const compareDocToModel = (doc, model, throwError) => {
  const res = {}
  Object.keys(model).forEach(key => {
    if (toType(model[key]) !== toType(doc[key])) {
      console.log('[helper/sheet] wrong type:', key, doc[key])
      if (throwError) {
        throw new Error('[helper/sheet] wrong type '+ key)
      }
      res[key] = model[key]
    } else {
      res[key] = doc[key]
    }
  })
  return res
}

export const updateObject = ({
  object, path, value, overwrite, push
}) => {
  var stack = path.split('/')
  const firstKey = stack[0]
  const res = object
  if (!object.hasOwnProperty(firstKey)) {
    return object
  }
  while (stack.length>1) {
    const key = stack.shift()
    if (!object.hasOwnProperty(key)) {
      object[key] = {}
    } 
    object = object[key]
  }
  const key = stack.shift()
  if (push) {
    object[key].push(value)
  } else if (!overwrite && typeof object[key] === 'object' && typeof value === 'object') {
    object[key] = {...object[key], ...value}
  } else {
    object[key] = value
  }
  return res
}
