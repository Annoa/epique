import { compareDocToModel } from './utils'

export const sheetModel = {
  'txt-start': {desc:''},
  xp: {desc:'', totalXp:0, totalPc:0},
  xp2: {generique: {max: 0, current: 0}, pc: {max: 0, current: 0}},
  health: {pv: {base: 10, current: 10}, vitality: {base: 1,current: 1}, mind: {base: 1, current: 1}},
  ressources: {},
  roots: {desc:'', xp:0},
  abilities: {desc:'', xp:0, max:0},
  feats: {desc:'', xp:0},
  traits: {desc:'', xp:0},
  archetypes: {desc:'', xp:0},
  capacities: [],
  inventory: {desc:'', kg:0},
  equipments: [],
  options: {
    limits: {root: '8', skill: '10', feat: '20', item: '60', archetype: '150'},
    recoveries: {endurance: '5', volonte: '5', mana: '0'},
    'recoveries-intervals': {endurance:'1 heure', mana:'1 jour', volonte:'6 heures'},
  },
  'notes': {desc:''}
}

export const capacityModel = {
  desc: '',
  name: '',
  pc: 0,
  ner: 0,
  draft: false,
  distribution: {},
  isPermanence: false,
  isTriggered: false,
  capacityType: '',
  activation: {},
  reservation: {},
  meta: {},
  prop: {},
  init: {},
  efficacityBonus: [],
  activationBonus: [],
  isSouffle: false,
  unkeeps: []
}

export const ressourceModel = {
  base: 0,
  bonusFlat: 0,
  percent: [],
  reservation: 0,
  current: 0,
  desc: '',
  convertion: 0,
}

export const equipmentModel = {
  name: '',
  desc: '',
  ner: 0,
  kg: 0,
  stock: {
    mana: {max:0, current: 0},
    volonte: {max:0, current: 0},
    endurance: {max:0, current: 0},
    charge: {max:0, current:0}
  },
  type: '',
  isFoci: false,
  draft: false,
  unkeeps: []
}

export const unkeepModel = {
  operator: '-',
  nb: 1,
  ressource: 'endurance',
  interval: '1 heure',
  condition: ''
}

export const compareSheetToModel = sheet => {
  let res = sheet
  res = compareDocToModel(res, sheetModel)
  Object.keys(res.ressources).forEach(r => {
    res.ressources[r] = compareDocToModel(res.ressources[r], ressourceModel)
  })
  res.capacities.forEach((capa, i) => {
    res.capacities[i] = compareDocToModel(capa, capacityModel)
  })
  res.equipments.forEach((art, i) => {
    res.equipments[i] = compareDocToModel(art, equipmentModel)
  })
  return res
}
