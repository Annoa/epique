const showdown  = require('showdown')
import { getFactor, getKg, getRecoveries, getRessourcesUsedXp, getSaturation } from '@lib/helpers/epique'
import { getFile, saveFile } from './api'
import { compareSheetToModel } from './models/sheetModel'
import { updateObject } from './models/utils'

const converter =  new showdown.Converter({
  disableForced4SpacesIndentedSublists: true,
  simpleLineBreaks: true, 
  backslashEscapesHTMLTags: true,
  emoji: true
})

const makeHtml = sheet => {
  Object.keys(sheet).map(key => {
    if (Array.isArray(sheet[key])) {
      sheet[key].map(item => {
        if (item.desc) {
          item._html = converter.makeHtml(item.desc)
        }
      })
    } else if (sheet[key].desc) {
      sheet[key]._html = converter.makeHtml(sheet[key].desc)
    }
  })
}

const computeStats = sheet => {
  const limits = sheet.options?.limits || {}
  const stats = {}

  stats.pc = {
    used: sheet.capacities.reduce((a,b) => b.draft||b.isSouffle? a : a+(b.pc||0), 0),
    left: 10-(stats.usedPc%10 || 10)
  }
  stats.ps = {
    used: sheet.capacities.reduce((a,b) => b.draft||!b.isSouffle? a : a+(b.pc||0), 0)
  }
  stats.xp = {
    used: getRessourcesUsedXp(sheet) 
      + Object.keys(sheet).filter(a => a !== 'xp').reduce((a,b) => a+(sheet[b]?.xp || 0), 0),
    total: sheet.xp.totalXp || 0
  }
  stats.feats = {
    nb: sheet.feats.nb || 0,
    max: Math.round(stats.xp.total/(limits['feat'] || 20))
  }
  stats.equipments = {
    nb: sheet.equipments.reduce((nb, e) => nb+(
      e.draft ? 0
      : ['periapte', 'artefact'].includes(e.type) ? 1 
      : e.isFoci ? 1
      : e.type === 'charme' ? 0
      : 0
    ), 0),
    max: Math.round(stats.xp.total/(limits.item || 60))
  }
  stats.roots = {
    nb: sheet.roots.nb || 0,
    max: Math.round(stats.xp.total/(limits.root || 8))
  }
  stats.abilities = {
    nb: sheet.abilities.nb || 0,
    max: Math.round(stats.xp.total/(limits.skill || 10))
  }
  stats.ner = {
    used: sheet.capacities.reduce((a,b) => b.draft? a : a+(b.ner||0), 0)
      + sheet.equipments.reduce((a,b) => b.draft? a : a+(b.ner||0), 0)
      + Object.keys(sheet).reduce((a,b) => a+(sheet[b]?.ner || 0), 0),
    max: ['endurance', 'volonte', 'mana'].reduce((acc, v) => {
      return acc + (sheet.ressources[v]?.base || 0)
    }, 0),
  }
  stats.ner.thresholds = stats.ner.max? 
    [Math.round(stats.ner.max*1.4), Math.round(stats.ner.max*1.6),stats.ner.max*2]
    : [0]
  stats.archetypes = {
    nb: sheet.archetypes.nb || 0,
    max: Math.round(stats.xp.total/(limits['archetype'] || 150)),
  }
  stats.kg = getKg(sheet)
  stats.reservation = {
    mana: sheet.capacities.reduce((acc,cap) => !cap.draft ? acc + (cap.reservation?.mana || 0) : acc, 0),
    endurance: sheet.capacities.reduce((acc,cap) => !cap.draft ? acc + (cap.reservation?.endurance || 0) : acc, 0),
    volonte: sheet.capacities.reduce((acc,cap) => !cap.draft ? acc + (cap.reservation?.volonte || 0) : acc, 0),
  }
  stats.saturation = {
    mana: getSaturation(sheet, 'mana'),
    endurance: getSaturation(sheet, 'endurance'),
    volonte: getSaturation(sheet, 'volonte'),
  }
  stats.factor = {
    mana: getFactor(sheet, 'mana'),
    endurance: getFactor(sheet, 'endurance'),
    volonte: getFactor(sheet, 'volonte'),
  }
  stats.recoveries = getRecoveries(sheet)
  sheet._stats = stats
}

export const processSheet = sheet => {
  if (sheet) {
    makeHtml(sheet)
    try {
      computeStats(sheet)
    } catch (err) {
      console.log(err)
      sheet._stats = {}
    }
  }
}

export const getSheetById = async ({id, isPnj}) => {
  try {
    const path = `${isPnj?'pnj':'sheets'}/${id}.json`
    const file = await getFile({path})
    let sheet = JSON.parse(file.body)
    sheet = compareSheetToModel(sheet)
    return sheet
  } catch (e) {
    return null
  } 
}

export const updateSheet = async ({
  id, sheet, path, value, overwrite, isPnj, push
}) => {
  try {
    let newSheet = updateObject({
      object:sheet, path, value, overwrite, push
    })
    newSheet = compareSheetToModel(newSheet)
    await saveFile({
      path:`${isPnj?'pnj':'sheets'}/${id}.json`,
      data: JSON.stringify(newSheet)
    })
    return sheet
  } catch (error) {
    console.error(error)
    return sheet
  }
}
