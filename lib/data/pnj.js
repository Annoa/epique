const showdown  = require('showdown')

import { deleteFile, getFile, rename, saveFile } from './api'
import { PNJ } from './models/pnjModel'
import { sheetModel } from './models/sheetModel'

const converter =  new showdown.Converter({
  disableForced4SpacesIndentedSublists: true,
  simpleLineBreaks: true, 
  backslashEscapesHTMLTags: true,
  emoji: true
})

function filterObjectProperties(obj, propsArray) {
  return Object.keys(obj)
    .filter(key => propsArray.includes(key))
    .reduce((filteredObj, key) => {
      filteredObj[key] = obj[key]
      return filteredObj
    }, {})
}


export const getPnjs = async user => {
  const file = await getFile({path:'pnjs.json'})
  const pnj = (JSON.parse(file.body))
    .sort((a, b) => a.shortName.localeCompare(b.shortName))
  if (user.isAdmin) {
    return pnj
  }
  return pnj.filter(p => (
    p.authGroupIds.some(gid => user.groupIds.includes(gid))
  )).map(p => {
    if (!user.isAdmin) {
      delete p.adminDesc
      delete p.hasSheet
    } else {
      p.groupComments = filterObjectProperties(p.groupComments, user.groupIds)
      p.groupDesc = filterObjectProperties(p.groupDesc, user.groupIds)
    }
    return p
  })
}

export const processPnj = pnj => {
  pnj._htmlDesc = {}
  pnj._htmlComments = {}
  if (pnj.adminDesc) {
    pnj._htmlAdminDesc = converter.makeHtml(pnj.adminDesc)
  }
  Object.keys(pnj.groupDesc).forEach(gid => {
    pnj._htmlDesc[gid] = converter.makeHtml(pnj.groupDesc[gid])
  })
  Object.keys(pnj.groupComments).forEach(gid => {
    pnj._htmlComments[gid] = converter.makeHtml(pnj.groupComments[gid])
  })
}

export const processPnjs = pnjs => {
  pnjs.forEach(pnj => {
    processPnj(pnj)
  })
}

export const addPnj = async pnj => {
  if (!pnj.id || !pnj.name || !pnj.shortName) return []
  const file = await getFile({path:'pnjs.json'})
  const pnjs = JSON.parse(file.body)
  pnjs.push({
    ...PNJ,
    ...pnj
  })
  await saveFile({
    path: 'pnjs.json', 
    data: JSON.stringify(pnjs)
  })
  return pnjs
}

export const updatePnj = async (id, body, deleteAllFiles) => {
  if (!id || !body) return
  const file = await getFile({path:'pnjs.json'})
  let pnjs = JSON.parse(file.body)
  let hadSheet = false
  pnjs = pnjs.map(s => {
    if (s.id === id) {
      hadSheet = s.hasSheet
      return {
        ...s, 
        ...body,
        groupDesc: {
          ...s.groupDesc,
          ...body.groupDesc
        },
        groupComments: {
          ...s.groupComments,
          ...body.groupComments
        }
      }
    }
    return s
  })
  if (!hadSheet && body.hasSheet) {
    await saveFile({
      path: `pnj/${id}.json`,
      data: JSON.stringify(sheetModel), 
      ifNotExist: true
    })
  } else if (hadSheet && body.hasSheet === false && deleteAllFiles) {
    await deleteFile({path: `pnj/${id}.json`})
  } else if (hadSheet && body.id && body.id !== id) {
    await rename({
      oldPath: `pnj/${id}.json`,
      newPath: `pnj/${body.id}.json`
    })
  }
  await saveFile({
    path: 'pnjs.json', 
    data: JSON.stringify(pnjs)
  })
  return pnjs
}

export const deletePnj = async (id, deleteAllFiles) => {
  try {
    const file = await getFile({path:'pnjs.json'})
    let pnjs = JSON.parse(file.body)
    pnjs = pnjs.filter(m => m.id !== id)
    await saveFile({
      path: 'pnjs.json',
      data: JSON.stringify(pnjs)
    })
    if (deleteAllFiles) {
      await deleteFile({path: `pnj/${id}.json`})
    }
    return pnjs
  } catch (err)  {
    console.error(err)
    return []
  }
}
