import { getCookie } from 'cookies-next'
import jwt from 'jsonwebtoken'
import { getUsers } from './data/users'
import { getMaps } from './data/maps'
import { getGroups } from './data/groups'
import { getPjs } from './data/pj'
import { getPnjs, processPnjs } from './data/pnj'

export const getCookieUser = async (req, res) => {
  const token = getCookie('epique-token', { req, res })
  const users = await getUsers()
  try {
    const { userId } = jwt.verify(token, process.env.JWT_TOKEN)
    let user = users.find(u => u.id === userId)
    user = JSON.parse(JSON.stringify(user))
    return user
  } catch (error) {
    return null
  }
}

export const getAuthProps = async (req, res) => {
  const user = await getCookieUser(req, res)
  if (!user) return {}
  const [pjs, maps, groups, pnjs] = await Promise.all([
    getPjs(user),
    getMaps(user),
    getGroups(),
    getPnjs(user)
  ])
  processPnjs(pnjs)
  const props = {
    user, pjs, maps, groups, pnjs
  }
  return props
}

// todo delete me
export const getSheetPagesProps = async ({ req, res, params }) => {
  const props = await getAuthProps(req, res)
  const users = await getUsers(true)
  const pj = props.pjs.find(s => s.id === params.id)
  if (!pj) {
    return {
      redirect: {
        permanent: false,
        destination: "/",
      },
      props: {},
    }
  }
  return { props: { ...props, pj, users }} 
}