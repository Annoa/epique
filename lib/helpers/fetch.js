export const checkStatus = async (response) => {
  if(!response.ok) 
    throw new Error(`${response.status}: ${response.statusText}`)
  if (response.status >= 200 && response.status < 300) 
    return await response.json()
  throw await response.json()
}

export const putFetcher = (url, options) => fetch(url, {
  method: 'PUT',
  headers: {
    'Content-Type': 'application/json',
  },
  ...options
}).then(checkStatus)


export const postFetcher = (url, options) => fetch(url, {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
  ...options
}).then(checkStatus)

export const putSheetFetcher = ({id, path, value, overwrite, isPnj}) => {
  const url = `/api/${isPnj?'pnj/':''}sheet/${id}`
  const body = JSON.stringify({path, value, overwrite})
  return fetch(url, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    body
  }).then(checkStatus)
}