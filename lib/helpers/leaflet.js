import L from 'leaflet'
import tinycolor from 'tinycolor2'
import { MarkerIcon } from '@components/Map/Icon'

export const addControl = (map, ref, options) => {
  const control = L.control(options)
  control.onAdd = () => ref
  control.addTo(map.current)
  L.DomEvent.disableClickPropagation(ref)
}

export const drawControlsOptions = editableLayers => ({
  position: 'topleft',
  draw: {
    polyline: {
      shapeOptions: { color: '#0000FF', weight: 3, opacity: .8 }
    },
    polygon: {
      allowIntersection: false, 
      drawError: {
        color: '#FF0000', 
        message: '<strong>Oh snap!<strong> you can\'t draw that!'
      },
      shapeOptions: { color: '#0000FF', weight: 3, opacity: .8 },
    },
    marker: { icon: MarkerIcon() },
    circle: false,
    rectangle: false,
    circlemarker: false
  },
  edit: {
    featureGroup: editableLayers.current,
    edit: { selectedPathOptions: { maintainColor: true, weight: 3, opacity: .8 } }
  }
})

export const isDarkColor = (colorString) => {
  var color = tinycolor(colorString)
  return (color.getBrightness() < 150)
}


export const setGroupStyle = (layer, groups, visibleGroups) => {
  const groupId = layer.feature.properties?.groupId
  let visible = true, color = '#000'
  let weight = 3
  if (groupId || groupId === 0) {
    visible = visibleGroups[groupId] ?? false
    color = groups.find(g => g.id === groupId)?.color ?? '#000'
  }
  if (layer instanceof L.Polygon) {
    weight = 1
  } 
  if (layer instanceof L.Marker) {
    const { icon, name } = layer.feature.properties
    const className = (!color || isDarkColor(color))? 'dark-map-element':'light-map-element'
    layer.setOpacity(visible? 1 : 0)
    layer.setIcon(MarkerIcon(icon, className, {color}))
    layer.setTooltipContent(name || '')
    return visible
  }
  if (visible) {
    layer.setStyle({color, fillColor:color, opacity: 0.8, fillOpacity: 0.3, weight})
  } else {
    layer.setStyle({opacity:0, fillOpacity:0})
  }
  return visible
}

// Would benefit from https://github.com/Leaflet/Leaflet/issues/4461
// export function addGeojsonToEditableLayers(sourceLayer, targetGroup) {
//   if (sourceLayer instanceof L.LayerGroup) {
//     sourceLayer.eachLayer(function(layer) {
//       addGeojsonToEditableLayers(layer, targetGroup);
//     })
//   } else {
//     targetGroup.addLayer(sourceLayer);
//   }
// }

// only work for contiguous polygons
// https://stackoverflow.com/questions/31790344/determine-if-a-point-reside-inside-a-leaflet-polygon
export function isPointInsidePolygon(latlng, poly) {
  var inside = false
  var x = latlng.lat, y = latlng.lng
  for (var ii=0;ii<poly.getLatLngs().length;ii++){
    var polyPoints = poly.getLatLngs()[ii]
    for (var i = 0, j = polyPoints.length - 1; i < polyPoints.length; j = i++) {
      var xi = polyPoints[i].lat, yi = polyPoints[i].lng
      var xj = polyPoints[j].lat, yj = polyPoints[j].lng

      var intersect = ((yi > y) !== (yj > y))
          && (x < (xj - xi) * (y - yi) / (yj - yi) + xi)
      if (intersect) inside = !inside
    }
  }
  return inside
}

export function isPointInsidePolyline(map, latlng, poly) {
  var p = map.latLngToLayerPoint(latlng)
  for (var i=0;i<poly.getLatLngs().length - 1;i++) {
    var p1 = map.latLngToLayerPoint(poly.getLatLngs()[i]), 
      p2 = map.latLngToLayerPoint(poly.getLatLngs()[i+1])
    if (L.LineUtil.pointToSegmentDistance(p, p1, p2) < 10) 
      return true
  }
  return false;
}

export const setGeojsonFeature = layer => {
  layer.feature = layer.feature || {} 
  layer.feature.type = 'Feature'
  layer.feature.properties = layer.feature.properties || {}
  layer.feature.properties.id = layer._leaflet_id
}