import { 
  PRIMARY_RESSOURCES, RESSOURCES, HIGH_ADVANCES_RESSOURCES, LOW_ADVANCES_RESSOURCES, CLUTTER_THRESHOLD, RECOVERIES, CAPACITY_TYPE_PERMANENCE , CAPACITY_TYPE_DECLENCHMENT
} from '@constants/epique'
import { multipleAll } from './math'

export const _updateRootTypes = (rootTypesOutput, input) => {
  let advances = {}
  HIGH_ADVANCES_RESSOURCES.forEach(i => {
    advances[RESSOURCES[i]] = 0
  })
  LOW_ADVANCES_RESSOURCES.forEach(i => {
    advances[RESSOURCES[i]] = 0
  })
  advances[RESSOURCES['transcendance']] = 0
  if (input.length === 1) {
    if (input[0].toLowerCase().includes('mana')) {
      rootTypesOutput.push(Object.assign({'Mana': 6, "Endurance": 0, "Volonté": 0}, advances))
    } else if (input[0].toLowerCase().includes('endurance')) {
      rootTypesOutput.push(Object.assign({'Mana': 0, "Endurance": 6, "Volonté": 0}, advances))
    } else if (input[0].toLowerCase().includes('volonté')) {
      rootTypesOutput.push(Object.assign({'Mana': 0, "Endurance": 0, "Volonté": 6}, advances))
    } else {
      const key = input[0]
      rootTypesOutput.push({...advances, [key]: 1, 'Mana': 0, "Endurance": 0, "Volonté": 0})
    }
  } else if (input.length === 2) {
    let rootType = {...advances, 'Mana': 0, "Endurance": 0, "Volonté": 0}
    if (input[0].split(':')[1] === '2' || input[1].split(':')[1] === '2') {
      input.forEach(i => {
        if (i.toLowerCase().includes('mana')) {
          rootType['Mana'] = parseInt(i.split(':')[1]) * 2
        }
        if (i.toLowerCase().includes('endurance')) {
          rootType['Endurance'] = parseInt(i.split(':')[1]) * 2
        }
        if (i.toLowerCase().includes('volonté')) {
          rootType['Volonté'] = parseInt(i.split(':')[1]) * 2
        }
      })
    } else {
      input.forEach(i => {
        if (i.toLowerCase().includes('mana')) {
          rootType['Mana'] = 3
        }
        if (i.toLowerCase().includes('endurance')) {
          rootType['Endurance'] = 3
        }
        if (i.toLowerCase().includes('volonté')) {
          rootType['Volonté'] = 3
        }
      })
    }
    rootTypesOutput.push({...advances, ...rootType})
  } else {
    rootTypesOutput.push({...advances, 'Mana': 2, "Endurance": 2, "Volonté": 2})
  }
  return rootTypesOutput
}

export const calculateActivation = (pcs, distribution, instance) => {
  let advances = {}
  HIGH_ADVANCES_RESSOURCES.forEach(i => {
    advances[RESSOURCES[i]] = 0
  })
  LOW_ADVANCES_RESSOURCES.forEach(i => {
    advances[RESSOURCES[i]] = 0
  })
  advances[RESSOURCES['transcendance']] = 0
  let output = {...advances, 'mana': 0, 'volonte': 0, 'endurance': 0, 'transcendance': 0}
  if (instance.capacityType !== CAPACITY_TYPE_PERMANENCE) {
    let cost = pcs
    if (instance.capacityType === CAPACITY_TYPE_DECLENCHMENT) {
      cost = Math.round(cost*0.15)
    } else {
      cost = Math.round(cost*0.5)
    }
    let accTotalCost = 0
    const totalManaCost = distribution.mana > 0 ? Math.round(cost * distribution.mana/distribution.total) : 0
    output.mana = accTotalCost < cost && totalManaCost > 0 ? totalManaCost : 0
    accTotalCost += totalManaCost
    
    const totalVolonteCost = distribution.volonte > 0 ? Math.round(cost * distribution.volonte/distribution.total) : 0
    output.volonte = accTotalCost < cost && totalVolonteCost > 0 ? accTotalCost + totalVolonteCost > cost ? cost -  accTotalCost : totalVolonteCost : 0
    accTotalCost += accTotalCost + totalVolonteCost > cost ? cost -  accTotalCost : totalVolonteCost

    const totalEnduranceCost = distribution.endurance > 0 ? Math.round(cost * distribution.endurance/distribution.total) : 0
    output.endurance = accTotalCost < cost && totalEnduranceCost > 0 ? accTotalCost + totalEnduranceCost > cost ? cost -  accTotalCost : totalEnduranceCost : 0

    output.ichor = distribution.ichor > 0 ? Math.round(cost/12) : 0
    output.vitae = distribution.vitae > 0 ? Math.round(cost/12) : 0
    output.extia = distribution.extia > 0 ? Math.round(cost/12) : 0
    output.essence = distribution.essence > 0 ? Math.round(cost/6) : 0
    output.transcendance = distribution.transcendance > 0 ? Math.round(cost/18) : 0
  }
  return output
}

export const calculateReservation = (pcs, distribution, instance) => {
  let advances = {}
  HIGH_ADVANCES_RESSOURCES.forEach(i => {
    advances[RESSOURCES[i]] = 0
  })
  LOW_ADVANCES_RESSOURCES.forEach(i => {
    advances[RESSOURCES[i]] = 0
  })
  advances[RESSOURCES['transcendance']] = 0
  let output = {...advances, 'mana': 0, 'volonte': 0, 'endurance': 0, 'transcendance': 0}

  if (instance.capacityType === CAPACITY_TYPE_PERMANENCE || instance.capacityType === CAPACITY_TYPE_DECLENCHMENT) {
    let coeff = 0.2
    if (instance.capacityType === CAPACITY_TYPE_DECLENCHMENT) {
      coeff = 0.15
    }
    let accTotalReservation = 0
    const totalReservation = Math.round(pcs * coeff)
    const totalManaReservation = distribution.mana > 0 ? Math.round(totalReservation * distribution.mana/distribution.total) : 0
    output.mana = accTotalReservation < totalReservation && totalManaReservation > 0 ? totalManaReservation : 0
    accTotalReservation += totalManaReservation
    
    const totalVolonteReservation = distribution.volonte > 0 ? Math.round(totalReservation * distribution.volonte/distribution.total) : 0
    output.volonte = accTotalReservation < totalReservation && totalVolonteReservation > 0 ? accTotalReservation + totalVolonteReservation > totalReservation ? totalReservation - accTotalReservation : totalVolonteReservation : 0
    accTotalReservation += accTotalReservation + totalVolonteReservation > totalReservation ? totalReservation -  accTotalReservation : totalVolonteReservation

    const totalEnduranceReservation = distribution.endurance > 0 ? Math.round(totalReservation * distribution.endurance/distribution.total) : 0
    output.endurance = accTotalReservation < totalReservation && totalEnduranceReservation > 0 ? accTotalReservation + totalEnduranceReservation > totalReservation ? totalReservation - accTotalReservation : totalEnduranceReservation : 0

    output.ichor = distribution.ichor > 0 ? Math.max(Math.round(pcs/12*coeff), 1) : 0
    output.vitae = distribution.vitae > 0 ? Math.max(Math.round(pcs/12*coeff), 1) : 0
    output.extia = distribution.extia > 0 ? Math.max(Math.round(pcs/12*coeff), 1) : 0
    output.essence = distribution.essence > 0 ? Math.max(Math.round(pcs/6*coeff), 1) : 0
    output.transcendance = distribution.transcendance > 0 ? Math.max(Math.round(pcs/18*coeff), 1) : 0
  }
  return output
}

export const getFactor = (sheet, ressource) => {
  const sheetRessource = sheet.ressources[ressource] || {}
  const optionFactors = sheet.options?.factors?.[ressource]
  const { base = 0, bonusFlat = 0, convertion = 0, percent = 0} = sheetRessource
  const maxEndurance = Math.ceil((base+bonusFlat)*multipleAll(percent))
  const totalEndurance = maxEndurance-(convertion)
  const factor = Math.round(totalEndurance * multipleAll(optionFactors))
  return factor
}

export const getMaxRessource = ressource => {
  const { base = 0, bonusFlat = 0, convertion = 0, percent = 0} = ressource || {}
  const maxEndurance = Math.ceil((base+bonusFlat)*multipleAll(percent))
  const totalEndurance = maxEndurance-(convertion  || 0)
  return totalEndurance
}

export const getRessourcesUsedXp = sheet => {
  const ressources = sheet?.ressources || {}
  return Object.keys(ressources).reduce((acc, key) => {
    if (PRIMARY_RESSOURCES.includes(key)) {
      return acc + ressources[key].base
    }
    return acc
  }, 0)
}

export const getPrimaryRessources = () => {
  return PRIMARY_RESSOURCES.reduce((obj, r) => ({...obj, [r]:RESSOURCES[r]}), {})
}

export const getSaturation = (sheet, ressource) => {
  const current = sheet.equipments.reduce((nb, e) => {
    if (e.draft || !['periapte', 'artefact'].includes(e.type)) 
      return nb
    return nb + e.stock?.[ressource]?.current
  }, 0)
  const max = getMaxRessource(sheet.ressources[ressource])
  return {
    current,
    thresholds: max ? [
      max*2, 
      Math.round(max*2.5),
      max*3
    ] : [0]
  }
}

export const getInit = (sheet) => {
  const capacities = sheet.capacities || []
  let total = 0
  capacities.forEach(capacity => {
    if (capacity.meta) {
      const speed = capacity.meta["Vitesse de déplacement"] || capacity.meta["VD"]
      if (speed) {
        total += Math.round(speed[0].value / 6)
      }
      if (capacity.meta["Action supplémentaire"]) {
        total += capacity.meta["Action supplémentaire"].reduce((a, b) => a + b.value, 0) * 2
      }
    }
    if (capacity.init && Object.keys(capacity.init).length > 0) {
      for (let key in capacity.init) {
        total += capacity.init[key].reduce((a, b) => a + b)
      }
    }
  })
  return total
}

const getMaxCharge = sheet => {
  let base = 0
  let multiplier = 0
  const capacities = sheet.capacities
  capacities.forEach(capacity => {
    if (capacity.meta["Charge transportable"] && capacity.meta["Charge transportable"][0]) {
      base = capacity.meta["Charge transportable"][0].value * multipleAll(capacity.efficacityBonus)
    }
    if (capacity.meta["Force"] && capacity.meta["Force"][0] && capacity.capacityType === CAPACITY_TYPE_PERMANENCE) {
      multiplier += parseInt(capacity.meta["Force"][0].value)/4
    }
  })
  return Math.round(base * (1+multiplier))
}

const getMalusClutter = (kg, maxCharge) => {
  const clutterKeys = Object.keys(CLUTTER_THRESHOLD).sort()
  let malusClutter = CLUTTER_THRESHOLD['0.4']
  clutterKeys.forEach(threshold => {
    let nextIndex = clutterKeys.indexOf(threshold) + 1
    let nextThreshold = clutterKeys[nextIndex]
    if (kg/maxCharge > threshold && kg/maxCharge <= nextThreshold) {
      malusClutter = CLUTTER_THRESHOLD[nextThreshold]
    } else if (!nextThreshold && kg/maxCharge > threshold) {
      malusClutter = "À la discrétion du MJ"
    }
  })
  return malusClutter
}

export const getKg = sheet => {
  const current = sheet.equipments.reduce((acc, curr) => acc+(curr?.kg||0), 0) + (sheet?.inventory.kg || 0) 
  const maxCharge = getMaxCharge(sheet)
  const malusClutter = getMalusClutter(current, maxCharge) 
  return {current, maxCharge, malusClutter}
}

export const getRecoveries = sheet => {
  const res = {}
  RECOVERIES.forEach(i => {
    res[i] = {}
    PRIMARY_RESSOURCES.forEach(r => {
      const rank = parseInt(sheet.options.recoveries?.[r])
      const interval = sheet.options['recoveries-intervals']?.[r]
      const ressource = sheet.ressources[r]
      if (rank && interval && ressource && interval === i) {
        res[i][r] = []
        const max = Math.ceil((ressource.base+ressource.bonusFlat)
          * multipleAll(ressource.percent)) || 1
        res[i][r].push({
          nb: Math.ceil((max-ressource.convertion) * rank/100),
          operator: '+',
          name: 'Base',
          interval: i,
          ressource: r
        })
      }
    })
  })
  const unkeepsItems = [...sheet.capacities, ...sheet.equipments]
  unkeepsItems?.forEach(item => {
    if (item.draft) return
    item.unkeeps?.forEach(e => {
      res[e.interval][e.ressource] = res[e.interval][e.ressource] || []
      res[e.interval][e.ressource].push({
        ...e,
        name: item.name
      })
    })
  })
  RECOVERIES.forEach(i => {
    if (!Object.keys(res[i]).length) {
      delete res[i]
      return
    } 
    const conditionals = []
    PRIMARY_RESSOURCES.forEach(r => {
      if (!res[i][r]) return
      let total = 0, src = []
      res[i][r].forEach(recorvery => {
        const { operator, nb, condition } = recorvery
        if (condition) {
          conditionals.push(recorvery)
        } else {
          total += nb * (operator==='-'?-1:1)
          src.push(recorvery)
        }
      })
      res[i][r] = {
        total,
        src
      }
    })
    res[i] = {
      always: res[i],
      conditionals
    }
  })
  return res
}