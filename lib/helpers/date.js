export const timestamp2input = timestamp => {
  let date = new Date(timestamp || 0)
  return date.toISOString().substring(0, 10)
}

export const timestamp2display = timestamp => {
  let date = new Date(timestamp || 0)
  return date.toLocaleDateString('fr', {
    day:'numeric', month:'short', year:'numeric'
  })
}

export const input2timestamp = inputDate => {
  let date = new Date(inputDate)
  if (isNaN(date)) {
    date = new Date(0) 
  }
  return date.getTime()
}
