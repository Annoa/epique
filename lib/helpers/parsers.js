/*
https://github.com/highlightjs/highlight.js/blob/main/src/languages/markdown.js

https://highlightjs.readthedocs.io/en/latest/language-guide.html

*/

import { HIGH_ADVANCES_RESSOURCES, LOW_ADVANCES_RESSOURCES, RESSOURCES } from '@constants/epique'
import markdown from 'highlight.js/lib/languages/markdown'
import { calculateActivation, calculateReservation, _updateRootTypes } from './epique'
import markdownParser from './reductedMdParser'


const XP = '(xp|XP|px|PX)(s|S|)'
const PC = '[pP][cC][sS]?'
const NER = '[nN][eE][rR][sS]?'
const KG = '[kK][gG][sS]?'
const BULLET = '^[ \t]*[*+-]'
const META_BRANCHS = [
  '(P|p)oint(s|) de vie',
  'Portée',
  'Zone d\'effet',
  'Résistance interne',
  'Cible',
  'Durée',
  'Dégâts (\\[|\\().*(\\]|\\))',
  'Réduction (\\[|\\().*(\\]|\\))',
  'Bouclier (\\[|\\().*(\\]|\\))',
  'Aplomb',
  'Vitalité',
  'Soin',
  'Façonnage',
  'Protection',
  'Charge transportable',
  '(Vitesse de déplacement|VD)',
  'Guérison',
  'Arme(s|) naturelle(s|) (\\[|\\().*(\\]|\\))',
  'Force',
  'Bloc',
  'Solide',
  'Action supplémentaire',
  'Inamovible',
  'Esquive',
  'Esquive extraordinaire',
  'Coup critique',
  'Coup de grâce',
  'Premier sang',
  'Champ de force',
  'Résiliance magique',
  'Résiliance psionique',
  'Fréquence',
  'Titanesque',
  'Santé de fer',
  'Coup handicapant',
  'Attaque de zone',
]
export const NUMBER = '[+-]?\\d+((\\.|,)\\d+)?'

export const PROPS_BRANCHS = [
  'Chance de toucher',
  'Recharge',
]

export const INIT_BRANCHS = [
  'Organes sensoriels',
  'Type de perception',
  'Perceptions magique',
  'Hyper-compétence',
  'Prévoyance',
  'Sens',
  'Sens psioniques'
]

function xpParser(hljs) {
  const md = {...markdownParser(hljs)}
  md.name = 'xp'
  const TOTAL_XP = {
    begin: [/\d+/, /\//, /\d+/],
    beginScope: {
      1: 'available-xp',
      3: 'total-xp'
    }
  }
  md.contains.push(
    TOTAL_XP
  )
  return md
}

function ressourcesParser(hljs) {
  const md = {...markdownParser(hljs)}
  md.name = 'ressources'

  const TOTAL_RESSOURCE = {
    begin: [/\[/, /\d+/, /\]/],
    beginScope: {
      2: 'xp',
    }
  }
  md.contains.push(
    TOTAL_RESSOURCE
  ) 
  return md
}

function rootsParser(hljs) {
  const md = {...markdownParser(hljs)}
  md.name = 'roots'

  const ROOT_RARETY = {
    begin: [/\(/, /[CURM]/, /\)/],
    beginScope: {
      2: 'xp',
    }
  }
  md.contains.push(
    ROOT_RARETY
  ) 
  return md
}

function abilitiesParser(hljs) {
  const md = {...markdownParser(hljs)}
  md.name = 'abilities'

  const ABILITY_VALUE = {
  // eslint-disable-next-line no-useless-escape
  begin: [/[^-\+\[]\s?/, /[123456789]+\d*/],
    beginScope: {
      2:'xp'
    },
  }

  // const ABILITY = {
  //   begin: BULLET,
  //   scope: 'capacity'
  // }

  md.contains.push(
    // ABILITY,
    ABILITY_VALUE
  ) 
  return md
}

function featsParser(hljs) {
  const md = {...markdownParser(hljs)}
  md.name = 'feats'

  const FEAT_VALUE = {
    begin: [/\(s?/, /\d+/, `( |)${XP}`, /\)/],
    beginScope: {
      2: 'xp',
      3: 'xp-unit'
    }
  }
  md.contains.push(
    FEAT_VALUE
  ) 
  return md
}

function inventoryParser(hljs) {
  const md = {...markdownParser(hljs)}
  md.name = 'equipments'

  const KG_VALUE = {
    begin: [/\d+/, `\\s?${KG}`],
    beginScope: {
      1: 'kg',
      2: 'kg-unit'
    }
  }
  md.contains.push(
    KG_VALUE
  ) 
  return md
}

function capacitiesParser(hljs) {
  const md = {...markdownParser(hljs)}
  md.name = 'capacities'

  const CREATION_POINT = {
    begin: [/(-|)[0-9]+/, '( |)'+PC],
    beginScope : {
      1: 'pc',
      2: 'pc-unit'
    }
  }
  const MULTIPLIER_PC = {
    begin: [/(-|)[0-9]+/, /( |)%/],
    beginScope : {
      1: 'multiplier-pc'
    }
  }
  const NER_VALUE = {
    begin: [/\d+/, `\\s?${NER}`],
    beginScope: {
      1: 'ner',
      2: 'ner-unit'
    }
  }

  const CAPACITY_ROOT_TYPE = {
    begin: [/\(/, /((Mana:?\d?,? ?)|(Endurance:?\d?,? ?)|(Volonté:?\d?,? ?))+|((Essence)|(Ichor)|(Extia)|(Vitae)|(Transcendance))+/, /\)/],
    beginScope: {
      2: 'capacity-root-type',
    }
  }
  
  // Ex: Points de vie : 8pc > 20PV
  const META = {
    begin: [
      `${META_BRANCHS.join('|')}`, // branch
      /.*[^0-9]/, // on s'en foue
      /\d+/, '( |)'+PC, // pc + pc-unit
      /.*[^0-9,\\.]/, // on s'en foue
      NUMBER, /[^0-9\s\t\\-]?.*$/ // value + unit
    ], 
    beginScope: {
      1: 'meta-branch',
      3: 'pc',
      4: 'pc-unit',
      6: 'meta-value',
      7: 'meta-unit'
    },
    contains: [CAPACITY_ROOT_TYPE, CREATION_POINT, NER_VALUE]
  }

  const PROP = {
    begin: [
      `${PROPS_BRANCHS.join('|')}`, // branch
      /.*[^0-9]/, // on s'en foue
      /\d+/, '( |)'+PC, // pc + pc-unit
      /.*[^0-9,\\.]/, // on s'en foue
      NUMBER, /[^0-9\s\t\\-]?.*$/ // value + unit
    ], 
    beginScope: {
      1: 'prop',
      3: 'pc',
      4: 'pc-unit',
      6: 'prop-value',
      7: 'prop-unit'
    },
    contains: [CAPACITY_ROOT_TYPE, CREATION_POINT, NER_VALUE]
  }

  // INIT_BRANCHS
  const INIT = {
    begin: [
      `${INIT_BRANCHS.join('|')}`, // branch
      /.*[^0-9]/, // on s'en foue
      /\d+/, '( |)'+PC, // pc + pc-unit
      /.*/, // on s'en foue
    ], 
    beginScope: {
      1: 'init',
      3: 'pc',
    },
    contains: [CAPACITY_ROOT_TYPE, CREATION_POINT, NER_VALUE]
  }

  const CAPACITY = {
    begin: BULLET,
    end: /$/,
    scope: 'capacity',
    relevance: 10,
    contains: [CAPACITY_ROOT_TYPE, CREATION_POINT, MULTIPLIER_PC, NER_VALUE, META, PROP, INIT]
  }

  md.contains.push(
    CAPACITY,
    NER_VALUE
  )
  return md
}

export const getParser = sectionKey => {
  if (sectionKey === 'xp') {
    return xpParser
  }
  if (sectionKey === 'ressources') {
    return ressourcesParser
  }
  if (sectionKey === 'roots') {
    return rootsParser
  }
  if (sectionKey === 'abilities') {
    return abilitiesParser
  }
  if (['feats', 'traits', 'archetypes', 'txt-end'].includes(sectionKey)) {
    return featsParser
  }
  if (sectionKey === 'capacities') {
    return capacitiesParser
  }
  if (sectionKey === 'inventory') {
    return inventoryParser
  }
  return markdown
}


const computePxForRoots = node => {
  let total = 0
  const rootElems = Array.from(node.getElementsByClassName('hljs-xp'))
  rootElems.forEach(e => {
    if (e.textContent === 'C') total += 1
    if (e.textContent === 'U') total += 2
    if (e.textContent === 'R') total += 3
    if (e.textContent === 'M') total += 4
  })
  return total
}

const reducePlainValue = (node, className) => {
  const elems = Array.from(node.getElementsByClassName(className))
  return elems.reduce((acc, e) => acc+parseInt(e.textContent), 0)
}

const countDiv = (node, className) => {
  const elems = Array.from(node.getElementsByClassName(className))
  return elems.reduce((acc) => acc+1, 0)
}

const computePcForCapacities = (node, distribution) => {
  let pcs = 0, multi = 0
  const cpElems = Array.from(node.getElementsByClassName('hljs-pc'))
  const multiElems = Array.from(node.getElementsByClassName('hljs-multiplier-pc'))
  pcs = cpElems.reduce((acc, e) => {
    return acc+parseInt(e.textContent)
  }, 0)
  multi = multiElems.reduce((acc, e) => {
    let n = parseInt(e.textContent)
    return acc * ((n+100)/100)
  }, 1)
  if (distribution.ichor > 0 || distribution.vitae > 0 || distribution.extia > 0) {
    return Math.max(0, Math.round(pcs*12*multi))
  }
  if (distribution.essence > 0) {
    return Math.max(0, Math.round(pcs*6*multi))
  }
  if (distribution.transcendance > 0) {
    return Math.max(0, Math.round(pcs*18*multi))
  }
  return Math.max(0, Math.round(pcs*multi))  
}

const computeDistributionForCapacities = node => {
  const rootTypes = Array.from(node.getElementsByClassName('hljs-capacity-root-type'))
  let rootTypesOutput = []
  rootTypes.forEach(rootType => {
    const rootTypesInput = rootType.textContent.split(',')
    rootTypesOutput = _updateRootTypes(rootTypesOutput, rootTypesInput)
  });
  const totalMana = rootTypesOutput.reduce((acc, e) => {
    return acc+e['Mana']
  }, 0)
  const totalEndurance = rootTypesOutput.reduce((acc, e) => {
    return acc+e['Endurance']
  }, 0)
  const totalVolonte = rootTypesOutput.reduce((acc, e) => {
    return acc+e['Volonté']
  }, 0)

  let advances = {}
  HIGH_ADVANCES_RESSOURCES.forEach(i => {
    advances[i] = rootTypesOutput.reduce((acc, e) => {
      return acc+e[RESSOURCES[i]]
    }, 0)
  })
  LOW_ADVANCES_RESSOURCES.forEach(i => {
    advances[i] = rootTypesOutput.reduce((acc, e) => {
      return acc+e[RESSOURCES[i]]
    }, 0)
  })

  advances['transcendance'] = rootTypesOutput.reduce((acc, e) => {
    return acc+e['Transcendance']
  }, 0)
  return {
    ...advances,
    'mana': totalMana,
    'endurance': totalEndurance,
    'volonte': totalVolonte,
    'total': totalMana + totalEndurance + totalVolonte + Object.values(advances).reduce((acc, e) => {
      return acc+e
    }, 0)
  }
}

const computeMeta = node => {
  const rootElems = Array.from(node.getElementsByClassName('hljs-capacity'))
  const meta = {}
  rootElems.forEach(rootElem => {
    const branchs = Array.from(rootElem.getElementsByClassName('hljs-meta-branch'))
    if (branchs.length) {
      const branch = branchs[0].textContent
      const value = parseFloat(Array.from(rootElem.getElementsByClassName('hljs-meta-value'))?.[0]?.textContent.replace(',', '.')) || 0
      const unit = Array.from(rootElem.getElementsByClassName('hljs-meta-unit'))?.[0]?.textContent || ''
      if (meta[branch]) {
        meta[branch] = [...meta[branch], {unit, value}]
      } else {
        meta[branch] = [{unit, value}]
      }
    }
  })
  return meta
}

const computeProp = node => {
  const rootElems = Array.from(node.getElementsByClassName('hljs-capacity'))
  const prop = {}
  rootElems.forEach(rootElem => {
    const branchs = Array.from(rootElem.getElementsByClassName('hljs-prop'))
    if (branchs.length) {
      const branch = branchs[0].textContent
      const value = parseFloat(Array.from(rootElem.getElementsByClassName('hljs-prop-value'))?.[0]?.textContent.replace(',', '.')) || 0
      const unit = Array.from(rootElem.getElementsByClassName('hljs-prop-unit'))?.[0]?.textContent || ''
      if (prop[branch]) {
        prop[branch] = [...prop[branch], {unit, value}]
      } else {
        prop[branch] = [{unit, value}]
      }
    }
  })
  return prop
}

const computeInit = node => {
  const rootElems = Array.from(node.getElementsByClassName('hljs-capacity'))
  const prop = {}
  rootElems.forEach(rootElem => {
    const branchs = Array.from(rootElem.getElementsByClassName('hljs-init'))
    if (branchs.length) {
      const branch = branchs[0].textContent
      if (prop[branch]) {
        prop[branch] = [...prop[branch], 1]
      } else {
        prop[branch] = [1]
      }
    }
  })
  return prop
}

export const onParsedDivUpdate = ({
  node, 
  parser,
  setValue,
  instance
}) => {
  const setValue_ = (key, val) => setValue(prev => ({...prev, [key]:val}))
  if (!node || !parser) {
    return () => {}
  }
  if (['ressources','abilities', 'feats', 'traits', 'txt-end', 'archetypes'].includes(parser)) {
    setValue_('xp', reducePlainValue(node, 'hljs-xp'))
    if (['abilities','feats', 'archetypes'].includes(parser)) {
      setValue_('nb', countDiv(node, 'hljs-xp'))
    } 
  } else if (parser === 'roots') {
    setValue_('xp',computePxForRoots(node))
    setValue_('nb', countDiv(node, 'hljs-xp'))
  } else if (parser === 'capacities') {
    const distribution = computeDistributionForCapacities(node)
    setValue_('distribution', distribution)
    const pcs = computePcForCapacities(node, distribution)
    setValue_('pc', pcs)
    setValue_('activation', calculateActivation(pcs, distribution, instance))
    setValue_('reservation', calculateReservation(pcs, distribution, instance))
    setValue_('ner', reducePlainValue(node, 'hljs-ner'))
    const meta = computeMeta(node)
    setValue_('meta', meta)
    const prop = computeProp(node)
    setValue_('prop', prop)
    const init = computeInit(node)
    setValue_('init', init)
  } else if (parser === 'inventory') {
    setValue_('kg', reducePlainValue(node, 'hljs-kg'))
  } else if (parser === 'xp') {
    setValue_('xp', reducePlainValue(node, 'hljs-total-xp'))
  }
}
