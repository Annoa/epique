// src: https://github.com/guilhermelimak/react-codejar/blob/master/src/caret.ts

/*
 * element: HTMLDivElement
 */
export const getCaretOffset = (element) => {
  let caretOffset = 0
  const doc = element.ownerDocument || element.document
  const win = doc.defaultView || doc.parentWindow
  let sel
  if (typeof win.getSelection !== "undefined") {
    sel = win.getSelection()
    if (sel.rangeCount > 0) {
      var range = win.getSelection().getRangeAt(0)
      var preCaretRange = range.cloneRange()
      preCaretRange.selectNodeContents(element)
      preCaretRange.setEnd(range.endContainer, range.endOffset)
      caretOffset = preCaretRange.toString().length
    }
  } else if ((sel = doc.selection) && sel.type !== "Control") {
    var textRange = sel.createRange()
    var preCaretTextRange = doc.body.createTextRange()
    preCaretTextRange.moveToElementText(element)
    preCaretTextRange.setEndPoint("EndToEnd", textRange)
    caretOffset = preCaretTextRange.text.length
  }
  return caretOffset
}

/*
 * el: ChildNode,
 * chars: { count: number },
 * range?: Range
 */
const createRange = (
  el,
  chars,
  range
) => {
  if (!range) {
    range = document.createRange()
    range.selectNode(el)
    range.setStart(el, 0)
  }

  if (chars.count === 0) {
    range.setEnd(el, chars.count)
  } else if (el && chars.count > 0) {
    if (el.nodeType === Node.TEXT_NODE) {
      if (el.textContent?.length < chars.count) {
        chars.count -= el.textContent?.length
      } else {
        range.setEnd(el, chars.count)
        chars.count = 0
      }
    } else {
      for (var i = 0; i < el.childNodes.length; i++) {
        range = createRange(el.childNodes[i], chars, range)

        if (chars.count === 0) {
          break
        }
      }
    }
  }

  return range
}

/*
 * el: HTMLDivElement
 * chars: any
 */
export const setCurrentCursorPosition = (el, chars) => {
  if (chars >= 0) {
    const selection = window.getSelection()

    const range = createRange(el, { count: chars })

    if (range) {
      range.collapse(false)
      selection?.removeAllRanges()
      selection?.addRange(range)
    }
  }
}
