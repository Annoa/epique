export const normalize = str => {
  if (!str) return ""
  let res = str.normalize("NFD").replace(/[\u0300-\u036f]/g, "")
    .replace(/[^\w\s\']|_/g, " ")
  res = res.toLowerCase()
  return res
} 