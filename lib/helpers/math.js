export const multipleAll = arr => {
  if (!arr) return 1
  return arr.map(parseFloat).reduce((acc, curr) => acc * (1+curr/100), 1) || 1
}

export const coeffToPercent = coef => {
  return Math.round(multipleAll(coef)*100-100)
}