import Link from 'next/link'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { faEllipsis, faXmark, faFloppyDisk, faPencil } from '@fortawesome/free-solid-svg-icons'
import { faFileLines } from '@fortawesome/free-regular-svg-icons'

import { useEffect, useState } from 'react'
import TagsInput from 'react-tagsinput'

const PnjItem = ({
  isNew,
  pnj,
  user,
  gid,
  setSelectedPnjId,
  groups,
  api,
  setAddingPnj
}) => {
  const [editing, setEditing] = useState(isNew)
  const isAll = !gid && user.isAdmin
  const [value, setValue] = useState({
    id: '',
    name: '',
    shortName: '',
    tags: [], 
    authGroupIds: []
  })

  useEffect(() => {
    setValue({
      id: pnj.id, 
      name: pnj.name,
      shortName: pnj.shortName,
      tags: pnj.tags?.split(',').filter(t => t),
      authGroupIds: pnj.authGroupIds
    })
  }, [pnj])

  const close = () => {
    if (isNew) {
      setAddingPnj(false)
    } else {
      setEditing(false)
    }
  }

  const onInputChange = e => {
    const { name, value, checked } = e.target || e
    // console.log(name, value)
    if (name === 'authGroupIds') {
      setValue(prev => ({
        ...prev,
        authGroupIds: checked ? [...prev.authGroupIds, value] 
          : prev.authGroupIds.filter(e => e!==value)
      }))
    } else {
      setValue(prev => ({...prev, [name]:value}))
    }
  }

  const handleSave = () => {
    api({
      method: isNew? 'POST':'PUT',
      id: pnj.id,
      body: {
        ...value,
        tags: value.tags.join(', ')
      },
      cb: close
    })
  }

  if (isAll && editing) return <tr>
    <td className='px-0.5'>
      <input type='text' value={value.id} 
        name='id' placeholder='id' onChange={onInputChange}/>
    </td>
    <td className='px-0.5'>
      <input type='text' value={value.shortName} 
        name='shortName' placeholder='shortName' onChange={onInputChange}/>
    </td>
    <td className='px-0.5'>
      <input type='text' value={value.name} 
        name='name' placeholder='name' onChange={onInputChange}/>
    </td>
    <td className='px-0.5'>
      <TagsInput
        className='react-tagsinput -mb-1.5'
        value={value.tags} 
        inputProps={{
          placeholder: 'tags',
          list: 'tags-datalist'
        }}
        onChange={tags => setValue(prev => ({...prev, tags}))} />
    </td>
    <td className='px-0.5'>
      {groups?.map(({id}) => <label className='block' key={id}>
        <input type='checkbox' name='authGroupIds' 
          checked={value.authGroupIds.includes(id)}
          className='mr-1' value={id} onChange={onInputChange}/>
        {id}
      </label>)}
    </td>
    <td className='text-right whitespace-nowrap'>
      <button className='btn mr-1' onClick={handleSave}>
        <FontAwesomeIcon icon={faFloppyDisk}/>
      </button>
      <button className='btn' onClick={close}>
        <FontAwesomeIcon icon={faXmark}/>
      </button>
    </td>
  </tr> 

  return <tr>
    {isAll && <td>
      {pnj.id}
    </td>}
    <td className='whitespace-nowrap'>
      {pnj.hasSheet ? <Link href={`/sheet/pnj/${pnj.id}`} className='w-full font-bold text-ink-800'>
        <FontAwesomeIcon icon={faFileLines} className='text-ink-300 mr-1'/>
        {pnj.shortName}
      </Link>
      :
      pnj.shortName}
    </td>
    <td className='hidden md:table-cell'>{pnj.name}</td>
    <td>{pnj.tags}</td>
    {gid && <td className='hidden md:table-cell'>
      {pnj.groupDesc[gid]?.split('\n')[0]}
    </td>}
    {isAll && 
      <td>
        {pnj.authGroupIds.join(', ')}
      </td>
    }
    <td className='text-right whitespace-nowrap'>
      {user.isAdmin && isAll && 
        <button className='btn mr-1' onClick={() => setEditing(true)}>
          <FontAwesomeIcon icon={faPencil}/>
        </button>
      }
      <button className='btn' onClick={() => setSelectedPnjId(pnj.id)}>
        <FontAwesomeIcon icon={faEllipsis}/>
      </button>
    </td>
  </tr>
}

export default PnjItem