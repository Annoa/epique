import { useRouter } from 'next/router'
import { useCallback, useEffect, useState } from 'react'
import { normalize } from '@lib/helpers/string'
import { PNJ } from '@lib/data/models/pnjModel'
import { checkStatus } from '@lib/helpers/fetch'

import { faMagnifyingGlass, faPlus } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import PnjItem from './PnjItem'
import PnjModal from '@components/modals/PnjModal'
import Toast from '@components/Toast'

const PnjList = ({
  gid,
  pnjs,
  user,
  groups,
  tabs
}) => {
  const router = useRouter()
  const isAll = !gid && user.isAdmin

  const [tags, setTags] = useState({})
  const [inputFilter, setInputFilter] = useState('')
  const [filtredPnjs, setFiltredPnjs] = useState([])
  const [selectedPnjId, setSelectedPnjId] = useState()
  const [addingPnj, setAddingPnj] = useState(false)
  const [toast, setToast] = useState()

  useEffect(() => {
    let tags = pnjs.map(p => p.tags.split(',')).flat()
      .map(t => t.trim()).sort((a,b) => a.localeCompare(b))
      .reduce((acc,curr) => ({...acc, [curr]:false}), {})
    setTags(tags)
  }, [pnjs])

  const onTagClick = tag => {
    setTags(prev => {
      const newTags = {...prev}
      newTags[tag] = !newTags[tag]
      return newTags
    })
  }

  const api = useCallback(({
    id, body, method, cb
  }) => {
    const url = `/api/pnj${id?`/${id}`:''}`
    setToast()
    fetch(url, {
      method,
      headers: {
        'Content-Type': 'application/json',
      },
      ...body && {body: JSON.stringify(body)}
    })
    .then(checkStatus)
    .then(() => {
      setToast({
        title: `${method} pnj`,
        content: `${id || body.id} ${method==='DELETE'?'deleted':'saved'}!`,
        success: true
      })
      router.replace(router.asPath)
      cb && cb()
      method === 'DELETE' && setSelectedPnjId()
    })
    .catch(err => {
      console.log('error', err)
      setToast({
        title: `${method} pnj`,
        content: err.message,
        alert: true
      })
    })
  }, [router])

  useEffect(() => {
    let res = pnjs
    if (inputFilter) {
      const normStr = normalize(inputFilter)
      res = res.filter(r => normalize(r.name).includes(normStr)
        || normalize(r.shortName).includes(normStr)
        || normalize(r.groupDesc[gid]).includes(normStr)
      )
    }
    const selectedTags = Object.keys(tags).filter(t => tags[t])
    if (selectedTags.length) {
      res = res.filter(r => selectedTags.some(t => r.tags.includes(t)))
    }
    setFiltredPnjs(res)
  }, [gid, inputFilter, pnjs, tags])


  return <div className='mb-4'>
    <aside className='bg-ink-500/20 p-2'>
      <div className='relative'>
        <input 
          className='w-full h-6'
          type='text' 
          name='name'
          value={inputFilter}
          onChange={e => setInputFilter(e.target.value)} 
          autoComplete='off'/>
        <FontAwesomeIcon className='absolute top-1/2 -translate-y-1/2 right-2' 
          icon={faMagnifyingGlass}/>
      </div>
      {Object.keys(tags).length>0 &&
        <div className='flex flex-wrap items-center justify-center md:px-8 font-bold mt-2'>
          {Object.keys(tags).filter(t => tags[t]).map(tag => (
            <button key={tag} className='mx-1 px-1 my-1 bg-white/30 rounded-md' onClick={() => onTagClick(tag)}>
              {tag} [×]
            </button>
            ))}
        </div> 
      }
      <div className='flex flex-wrap items-center justify-center md:px-8 mt-2'>
        {Object.keys(tags).filter(t => !tags[t]).map(tag => (
          <button key={tag} className='mx-1 px-1 my-1 bg-white/30 rounded-md' onClick={() => onTagClick(tag)}>
            {tag}
          </button>
          ))}
      </div> 
    </aside>

    <div className='md:p-2'>
      <table className='w-full hover'>
        <tbody>
          {filtredPnjs?.map(pnj => <PnjItem 
            key={pnj.id}
            pnj={pnj}
            user={user}
            gid={gid}
            setSelectedPnjId={setSelectedPnjId}
            groups={groups}
            api={api}
            />)}
          {addingPnj && <PnjItem
            isNew
            pnj={PNJ}
            user={user}
            setAddingPnj={setAddingPnj}
            groups={groups}
            api={api}
          />}
        </tbody>
      </table>

      {isAll && <button className='btn mt-2'
        disabled={addingPnj}
        onClick={() => setAddingPnj(true)}>
        <FontAwesomeIcon icon={faPlus} className='mr-1'/>
        Nouveau Pnj
      </button>}

    </div>
    <PnjModal 
      pnj={pnjs.find(({id}) => id === selectedPnjId)} 
      close={() => setSelectedPnjId()} 
      defaultGid={gid} 
      user={user}
      tabs={tabs}
      api={api}/>

    <datalist id='tags-datalist'>
      {Object.keys(tags).map(t => (
        <option key={t} value={t}/>
      ))}
    </datalist>
    { toast && <Toast {...toast}/>}
  </div>
}

export default PnjList