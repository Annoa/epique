import { useCallback, useEffect, useRef } from 'react'
import useCodeEditor from '@hooks/useCodeEditor'
import Emoji from './Emoji'

const showdown  = require('showdown')

const noop = () => {}

const TextField = ({
  content,
  setContent,
  parser,
  onFocus,
  onBlur,
  onKeyDown,
  onParsedDivUpdate
}) => {
  const contentRef = useRef(content)

  const onUpdate = useCallback((txt, editorRef) => {
    setContent(txt)
    if (typeof onParsedDivUpdate === 'function') {
      setTimeout(() => onParsedDivUpdate(editorRef), 1)
    }
  }, [onParsedDivUpdate, setContent])

  useEffect(() => {
    contentRef.current = content
  }, [content])

  const { 
    setEditorRefCb, 
    handleClick, 
    cursorOffsetRef
  } = useCodeEditor({
    language: parser,
    codeJarOptions: {tab: '  '},
    code: content,
    onUpdate
  })

  const onEmojiClick = useCallback(emoji => {
    const c = [
      contentRef.current.slice(0, cursorOffsetRef.current), 
      emoji, 
      contentRef.current.slice(cursorOffsetRef.current)
    ].join('')
    setContent(c)
  }, [cursorOffsetRef, setContent])

  return <div className='relative'>
    <div 
      className='code-edit-container' 
      onFocus={onFocus||noop}
      onBlur={onBlur||noop}
      onClick={handleClick} 
      onKeyDown={onKeyDown}
      ref={setEditorRefCb}
    />
    <Emoji onEmojiClick={onEmojiClick}/>
  </div>
}

export default TextField