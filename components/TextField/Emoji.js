import { useState } from 'react'
import { Popover, PopoverContent, PopoverTrigger } from '@components/Popover'
import jsonEmojis from './emoji.json'
import Accordion, { AccordionItem } from '@components/Accordion'

const showdown  = require('showdown')
const supportedEmojis = showdown.helper.emojis

const emojisByCat = jsonEmojis.filter(e => supportedEmojis[e.aliases[0]]).reduce((prev, c) => {
  if (!prev[c.category]) { prev[c.category] = []; }
  prev[c.category].push(c);
  return prev;
}, {})

const Emoji = ({
  onEmojiClick
}) => {
  const [open, setOpen] = useState(false)

  const handleEmojiClick = emoji => {
    onEmojiClick(' :'+emoji.aliases[0]+': ')
    setOpen(false)
  }

  return <Popover open={open} onOpenChange={setOpen}>
      <PopoverTrigger asChild>
        <button 
          className='absolute top-0 right-1' 
          onClick={() => setOpen(true)}
          >
          {'\ud83d\ude04'}
        </button>
      </PopoverTrigger>
      <PopoverContent className='w-[500px] p-0 text-sm'>
        <Accordion type='single' defaultValue='Smileys & Emotion'>
          {Object.keys(emojisByCat).map(cat => 
            <AccordionItem key={cat} title={cat} className={{
              trigger:'text-base font-sans font-normal py-1'
            }}>
              {/*<div className='text-center bg-ink-500/20 font-bold'>{cat}</div> */}
              <div className='p-2 mb-2 text-lg'>
                {emojisByCat[cat].map(e => 
                  <button 
                    className='inline-block ml-1 cursor-pointer hover:bg-white' 
                    key={e.emoji} 
                    onClick={() => handleEmojiClick(e)}>
                    {e.emoji}
                  </button>
                )}
              </div>
            </AccordionItem>
          )}
        </Accordion>
      </PopoverContent>
    </Popover>
  
}

export default Emoji