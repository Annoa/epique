import dynamic from 'next/dynamic'
import { useCallback, useEffect, useRef, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import useKeySave from '@hooks/useKeySave'
import { useInfoModalCtx } from '@contexts/infoModalCtx'

import { timestamp2display } from '@lib/helpers/date'

import { AccordionItem } from '@components/Accordion'
import { faFloppyDisk, faPencil, faXmark } from '@fortawesome/free-solid-svg-icons'


const TextField = dynamic(
  () => import('@components/TextField'), 
  { ssr:false }
)


const JournalEntry = ({
  entry,
  setOpen,
  save
}) => {
  const [editing, setEditing] = useState(false)
  const [content, setContent] = useState('')
  
  const elemRef = useRef()

  const { onHtmlClick } = useInfoModalCtx()

  useEffect(() => {
    if (editing) {
      setOpen(entry.id)
    }
  }, [editing, entry.id, setOpen])

  useEffect(() => {
    setContent(entry.content)
  }, [entry.content])

  const onSaveClik = () => {
    save({...entry, content})
    setEditing(false)
  }

  const keySave = useCallback(content => {
    save({...entry, content})
  }, [entry, save])

  useKeySave({
    elemRef,
    close: setEditing,
    save: keySave,
    value: content
  })
  
  return <AccordionItem 
    disabled={editing}
    value={entry.id} title={
    <>
      <div className='truncate shrink'>
        { timestamp2display(entry.timestamp) } - {entry.title}
      </div>
      <div className={`mx-2 grow text-left ${editing?'invisible':''}`}>
        <a className='inline-btn' onClick={() => setEditing(true)}>
          <FontAwesomeIcon icon={faPencil}/>
        </a>
      </div>
    </>
  }>
    {editing ?
    <div ref={elemRef}>
      <div className='absolute top-2 right-2'>
        <a className='inline-btn' onClick={onSaveClik}>
          <FontAwesomeIcon size='lg' icon={faFloppyDisk}/>
        </a>
        <a className='inline-btn' onClick={() => setEditing(false)}>
          <FontAwesomeIcon size='lg' icon={faXmark}/>
        </a>
      </div>
      <TextField
        content={content}
        setContent={setContent}
      />
    </div>
    :
    <div 
      className={`inner-html-container`}
      dangerouslySetInnerHTML={{ __html: entry._html }}
      onClick={onHtmlClick}
    />
    }
    
  </AccordionItem>
}

export default JournalEntry