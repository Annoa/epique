import * as PopoverPrimitive from '@radix-ui/react-popover'

import { faXmark } from '@fortawesome/free-solid-svg-icons'
import styles from './popover.module.css'
import { twMerge } from 'tailwind-merge'
import { forwardRef } from 'react';


export const Popover = PopoverPrimitive.Root
export const PopoverTrigger = PopoverPrimitive.Trigger

export const PopoverContent = forwardRef(({
  children,
  className, 
  ...props
}, forwardedRef) => (
    <PopoverPrimitive.Portal>
      <PopoverPrimitive.Content 
        sideOffset={5} 
        className={twMerge(styles.PopoverContent, className)} 
        {...props} 
        ref={forwardedRef}>
        {children}
        <PopoverPrimitive.Arrow className={styles.PopoverArrow}/>
      </PopoverPrimitive.Content>
    </PopoverPrimitive.Portal>
  )
)

PopoverContent.displayName = 'PopoverContent'