import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { checkStatus } from '@lib/helpers/fetch'

import Alert from './modals/Alert'
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons'

const DeleteAdminButton = ({
  id,
  data,
  name,
  deleteAllFilesOption,
  deleteFunc,
  label
}) => {
  const router = useRouter()
  const [open, setOpen] = useState(false)
  const [deleteAllFiles, setDeleteAllFiles] = useState(false)

  useEffect(() => setDeleteAllFiles(false), [id])

  const url = `/api/${data}/${id}`

  const handleDelete = () => fetch(url, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    },
    ...deleteAllFiles && { 
      body: JSON.stringify({deleteAllFiles})
    }
  })
  .then(checkStatus)
  .then(() => {
    router.replace(router.asPath)
    setOpen(false)
  })
  .catch(err => console.log('error', err))

  const onClick = () => {
    if (deleteFunc) {
      deleteFunc(deleteAllFiles)
    } else {
      handleDelete()
    }
  }
  
  return <Alert
    button={<button className='btn btn-red'>
      <FontAwesomeIcon 
        className={label?'mr-1':''}
        icon={faTrashAlt}/>
      {label}
    </button>}
    title={label||`Supprimer "${name}" ?`}
    okTxt='Oui, supprimer'
    open={open}
    onOpenChange={setOpen}
    onOkClick={onClick}
  >
    {deleteAllFilesOption &&
      <div className='mb-4'>
        <input className='mr-1'
          type='checkbox' id='deleteAllFiles' 
          checked={deleteAllFiles}
          onChange={e => setDeleteAllFiles(e.target.checked)}
          />
        <label htmlFor='deleteAllFiles'>
          Supprimer également tous les fichiers associés ?
        </label>
      </div>
    }
  </Alert>
}

export default DeleteAdminButton
