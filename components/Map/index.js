// @refresh reset

import { useEffect, useRef, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import L from 'leaflet'

import styles from './map.module.css'
import { setGroupStyle } from '@lib/helpers/leaflet'

import useMapLayers from '@hooks/useMapLayers'
import useMapGroups from '@hooks/useMapGroups'

import PropsControl from './Controls/PropsControl'
import GroupsControl from './Controls/GroupsControl'
import { PingIcon } from './Icon'
import { faEdit } from '@fortawesome/free-solid-svg-icons'
import Link from 'next/link'

const pingMap = ({latlng, map, color}) => {
  const ping = L.marker(latlng).setIcon(PingIcon(color)).addTo(map)
  setTimeout(() => {
    ping.removeFrom(map)
  }, 5000)
}
  

const MapContainer = ({
  map, groups, geojson, socket, user, users
}) => {
  const mapRef = useRef()
  const [mapInit, setMapInit] = useState(false)
  
  const {
    mapLayers,
    mouseOverLayers,
    onMapMouseMove
  } = useMapLayers({ geojson, mapRef })

  const {
    visibleGroups,
    onChangeVisibility
  } = useMapGroups({ groups })

  useEffect(() => {
    mapLayers.current.eachLayer(layer => {
      setGroupStyle(layer, groups, visibleGroups)
    })
  }, [geojson, visibleGroups, groups])
  

  useEffect(() => {
    if (mapRef.current) return
    mapRef.current = L.map('map', {
      crs: L.CRS.Simple,
      minZoom: -5
    })

    const bg = `/images/map/bg/${map.bg}`
    const bounds = [[0,0], [map.height, map.width]]
    L.imageOverlay(bg, bounds).addTo(mapRef.current)

    mapRef.current.invalidateSize()
    const maxBound = Math.max(bounds[1][0], bounds[1][1])
    mapRef.current.fitBounds([
      [0, 0],
      [maxBound/2, maxBound/2]
    ])
    mapRef.current.setView( [bounds[1][0]/2, bounds[1][1]/2])
    mapLayers.current.addTo(mapRef.current)
    mapRef.current.on('mousemove', onMapMouseMove)
    setMapInit(true)
  }, [map])

  useEffect(() => {
    if (!socket || !mapInit || !user.id || !users) return
    const clickHandler = e => {
      let color = users.find(({id}) => id === user.id).color
      pingMap({map:mapRef.current, latlng:e.latlng, color})
      socket.emit('ping', {
        latlng:e.latlng, 
        color:color.current, 
        userId: user.id
      })
    }
    mapRef.current.on('click', clickHandler)
    return () => {
      mapRef.current.off('click', clickHandler)
    }
  }, [socket, mapInit, user.id, users])

  
  useEffect(() => {
    if (!socket || !users || !mapInit) return
    const pingHandler = data => {
      let color = users.find(({id}) => id === data.userId).color
      pingMap({map:mapRef.current, latlng:data.latlng, color})
    }
    socket.on('ping', pingHandler)
  }, [socket, users, map, mapInit])

  return <>
    <div id='map' className='full-screen'/>
    {user.isAdmin && <div className={`${styles['top-left-container']}`}>
      <div className={`${styles['control-panel']} rounded-sm`}>
        <div className={`${styles['control']} rounded-sm`}>
          <Link href={`/map/${map.id}/edit`} className={styles['control-button']}>
            <FontAwesomeIcon icon={faEdit}/>
          </Link>
        </div>
      </div>
    </div>}
    <div className={styles['top-right-container']}>
      {groups?.length > 0 && <div className={styles['control-panel']}>
        <GroupsControl
          groups={groups}
          visibleGroups={visibleGroups}
          onChangeVisibility={onChangeVisibility}
          readOnly/>
      </div>}
    </div>
    <div className={styles['bottom-right-container']}>
      <div className={styles['control-panel']}>
        <PropsControl
          polyLayers={mouseOverLayers} 
          groups={groups}
          readOnly/>
        </div>
    </div>
  </> 
}

export default MapContainer