// @refresh reset
import { useCallback, useEffect, useRef, useState } from 'react'
import L from 'leaflet'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import 'leaflet-draw'
import 'leaflet-draw/dist/leaflet.draw.css'
import styles from './map.module.css'

import { drawControlsOptions, setGroupStyle } from '@lib/helpers/leaflet'
import useMapLayers from '@hooks/useMapLayers'
import useMapGroups from '@hooks/useMapGroups'
import useWarnIfUnsavedChanges from '@hooks/useWarnIfUnsavedChanges'

import PropsControl from './Controls/PropsControl'
import GroupsControl from './Controls/GroupsControl'
import { MarkerIcon } from './Icon'
import { faFloppyDisk, faXmark } from '@fortawesome/free-solid-svg-icons'
import Link from 'next/link'

const EditableMap = ({
  map, groups, geojson, saveGroups, saveGeojson
}) => {
  const mapRef = useRef()
  const [unsavedChanges, setUnsavedChanges] = useState(false)
  const [propsEditing, setPropsEditing] = useState(false)
  const [layersEditing, setLayersEditing] = useState(false)
  const editableLayers = useRef(new L.FeatureGroup())

  useWarnIfUnsavedChanges(unsavedChanges, () => {
    return confirm("Warning! You have unsaved changes.")
  })
  
  const {
    mapLayers,
    overedMarker,
    mouseOverLayers,
    onMapMouseMove,
    nbHovered
  } = useMapLayers({ 
    geojson:geojson,
    mapRef
  })

  const {
    visibleGroups,
    onChangeVisibility
  } = useMapGroups({ groups })

  useEffect(() => {
    mapLayers.current.eachLayer(layer => {
      const visible = setGroupStyle(layer, groups, visibleGroups)
      if (visible) {
        editableLayers.current.addLayer(layer)
      }
    })
    return () => editableLayers.current?.clearLayers()
  }, [geojson, visibleGroups, groups])

  const saveGeojsonLayers = useCallback(newProps => {
    if (newProps) {
      mapLayers.current.eachLayer(layer => {
        if (newProps.id === layer.feature.properties.id) {
          layer.feature.properties = newProps
        }
      })
    }
    saveGeojson(mapLayers.current.toGeoJSON())
    setUnsavedChanges(false)
  }, [])

  useEffect(() => {
    if (mapRef.current) return
    mapRef.current = L.map('map', {
      crs: L.CRS.Simple,
      minZoom: -5
    })
    L.Marker.prototype.options.icon = MarkerIcon()
    
    const bg = `/images/map/bg/${map.bg}`
    const bounds = [[0,0], [map.height, map.width]]
    L.imageOverlay(bg, bounds).addTo(mapRef.current)
    mapRef.current.invalidateSize()
    const maxBound = Math.max(bounds[1][0], bounds[1][1])
    mapRef.current.fitBounds([
      [0, 0],
      [maxBound/2, maxBound/2]
    ])
    mapRef.current.setView( [bounds[1][0]/2, bounds[1][1]/2])
    mapLayers.current.addTo(mapRef.current)
    
    mapRef.current.on('mousemove', onMapMouseMove)
    setUnsavedChanges(false)
    setLayersEditing(false)
    setPropsEditing(false)

    /* draw */ 
    const drawControl = new L.Control.Draw(drawControlsOptions(editableLayers))
    mapRef.current.addControl(drawControl)
    mapRef.current.on('draw:drawstart draw:editstart draw:deletestart', e => {
      mapLayers.current.removeFrom(mapRef.current)
      editableLayers.current.addTo(mapRef.current)
      setLayersEditing(true)
    })
    mapRef.current.on('draw:created draw:edited draw:deleted', e => {
      editableLayers.current.removeFrom(mapRef.current)
      mapLayers.current.addTo(mapRef.current)
      if (e.type === 'draw:created') {
        mapLayers.current.addLayer(e.layer)
      } else if (e.type === 'draw:deleted') {
        e.layers.eachLayer(layer => mapLayers.current.removeLayer(layer))
      }
      setUnsavedChanges(true)
      setLayersEditing(false)
    })
    mapRef.current.on('draw:drawstop draw:editstop draw:deletestop', e => {
      editableLayers.current.removeFrom(mapRef.current)
      mapLayers.current.addTo(mapRef.current)
      setLayersEditing(false)
    })
  }, [map])

  useEffect(() => {
    if (!mapRef.current) return
    if (layersEditing) {
      setPropsEditing(false)
    }
    const onClick = e => {
      if (propsEditing) {
        setPropsEditing(false)
      } else if (!layersEditing && nbHovered.current > 0) {
        setPropsEditing(true)
      }
    }
    mapRef.current.on('click', onClick)
    return () => mapRef.current?.off('click', onClick)
  }, [layersEditing, propsEditing])

  return <>
    <div id='map' className='full-screen'/>
    <div className={`${styles['top-left-container']} top-[266px]`}>
      <div className={`${styles['control-panel']} rounded-sm`}>
        <div className={`${styles['control']} rounded-sm`}>
          <button 
            onClick={() => saveGeojsonLayers()}
            disabled={!unsavedChanges}
            className={`
              ${styles['control-button']} ${unsavedChanges?styles['unsaved-changes']:''}
            `}>
            <FontAwesomeIcon icon={faFloppyDisk}/>
          </button>
          <Link href={`/map/${map.id}`} className={styles['control-button']}>
            <FontAwesomeIcon icon={faXmark}/>
          </Link>
        </div>
      </div>
    </div>
    <div className={styles['top-right-container']}>
      <div className={styles['control-panel']}>
        <GroupsControl
          groups={groups}
          visibleGroups={visibleGroups}
          onChangeVisibility={onChangeVisibility}
          save={saveGroups}/>
      </div>
      <div className={`${styles['control-panel']} mt-2`}>
        <PropsControl
          markerLayer={overedMarker}
          polyLayers={mouseOverLayers} 
          editing={propsEditing}
          groups={groups}
          save={saveGeojsonLayers}
          readOnly={layersEditing}/>
      </div>
    </div>
  </> 
}

export default EditableMap