import Icon from '@components/Icon'
import list from '@components/Icon/list'

const IconInput = ({defaultValue, onChange}) => {
  return <div className='grid grid-cols-10 gap-1 mb-2'>
    { Object.keys(list).map(name => (
      <label key={name}>
        <input type='radio' 
          name='icon' 
          className='mr-1'
          value={name} 
          checked={name === defaultValue}
          onChange={onChange}/>
        <Icon name={name}/>
      </label>
    ))}
  </div>
}

export default IconInput