import { useCallback, useEffect, useRef, useState } from 'react'
import IconInput from './IconInput'
import { faEdit, faFloppyDisk, faXmark } from '@fortawesome/free-solid-svg-icons'
import { isDarkColor } from '@lib/helpers/leaflet'
import styles from '../../map.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Icon from '@components/Icon'
import useKeySave from '@hooks/useKeySave'

const INIT = {
  name: '',
  groupId: ''
}

const EditPropsForm = ({ 
  properties, 
  save, 
  groups=[], 
  isMarker, 
  readOnly 
}) => {
  const [editing, setEditing] = useState(false)
  const [props, setProps] = useState(INIT)
  const elemRef = useRef()

  useEffect(() => {
    setProps({...INIT,...properties})
  }, [properties])

  const handleSave = () => {
    save(props)
    setEditing(false)
  }

  const onInputChange = e => {
    const { name, value } = e.target
    let v = value
    if (name === 'groupId') {
      v = parseInt(v) || undefined 
    }
    setProps(prev => ({...prev, [name]:v}))
  }

  useKeySave({
    elemRef,
    close: setEditing,
    save,
    value: props
  })

  if (editing) {
    return (
      <div className='p-2' ref={elemRef}>

        <div className='flex items-center mb-1'>
          <label htmlFor='name' className='font-bold mr-1'>Name</label>
          <input id='name' type='text' placeholder='name' name='name'
            value={props.name} onChange={onInputChange}/>
        </div>
        <div className='flex items-center mb-2'>
          <label htmlFor='group' className='font-bold mr-1'>Groupe</label>
          <select id='group' name='groupId' value={''+props.groupId} onChange={onInputChange}>
            <option value=''></option>
            {groups.map(g => <option key={g.id} value={''+g.id}>
              {g.name}
            </option>)}
          </select> 
        </div>

        { isMarker &&
          <IconInput defaultValue={props.icon||'mapMarkerAlt'} onChange={onInputChange}/>
        }
        <div className='text-center'>
          <button className='btn btn-gray p-1' onClick={() => handleSave(props)}>
            <FontAwesomeIcon icon={faFloppyDisk} />
          </button>
          <button className='btn btn-gray p-1 mx-1' onClick={() => setEditing(false)}>
            <FontAwesomeIcon icon={faXmark} />
          </button>
        </div> 
      </div>
    )
  }

  const group = groups.find(g => g.id === properties.groupId) || {}

  if (readOnly) {
    return <div className='shadow m-1 p-1'>
      <div className='text-center'>
        {group.name && <label 
          className={styles['group-label-'+(isDarkColor(group.color)?'dark':'light')]} 
          style={{color: group.color}}>
          {group.name}<br/>
        </label>}
        {properties.name || '??'}
      </div>
    </div>
  }

  return (
    <div className='shadow border m-1 p-1 flex items-center justify-between'>
      <div className='mr-2'>
        <label className='font-bold'>Name</label> {properties.name || '??'} <br/>
        <label className='font-bold'>Groupe</label> {group.name || '??'}<br/>
        {isMarker && <>
          <label className='font-bold mr-2'>Icon</label>
          <Icon name={properties.icon} fallback='mapMarker'/>
        </>}
      </div>
      <div>
        <button className='btn btn-gray p-1' onClick={() => setEditing(true)}>
          <FontAwesomeIcon icon={faEdit}/>
        </button>
      </div>
    </div>
  )
}

export default EditPropsForm