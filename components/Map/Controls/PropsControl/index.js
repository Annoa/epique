import { useCallback, useEffect, useState } from 'react'
import { faLock, faLockOpen } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import EditProps from './EditProps'
import styles from '../../map.module.css'


const PropsControl = (
  { polyLayers, markerLayer, save, groups, editing, readOnly }
) => {
  const [layers, setLayers] = useState([])
  const [marker, setMarker] = useState(false)
  
  useEffect(() => {
    if (!editing) {
      polyLayers && setLayers(Object.values(polyLayers).filter(l => l?.feature?.properties))
      setMarker(markerLayer)
    }
  }, [polyLayers, markerLayer, editing])
  
  const visible = marker || layers.length

  const save_ = useCallback((props) => {
    if (props.icon) {
      setMarker(prev => {
        let m = { ...prev }
        m.feature.properties = props
        return m
      })
    } else {
      setLayers(prev => prev.map(layer => {
        if (layer.feature.properties.id === props.id) {
          let l = { ...layer }
          l.feature.properties = props
          return l
        }
        return layer
      }))
    }
    save(props)
  }, [save])

  return (
    <div className={`${styles['control']} ${visible?'':'hidden'}`}>
      {!readOnly && 
        <div className={styles['control-header']}>
          <FontAwesomeIcon icon={editing? faLock:faLockOpen} className='mr-1'/>
          Properties
        </div>
      }
      { marker?.feature?.properties && <EditProps 
          properties={marker.feature.properties}
          save={save_}
          groups={groups}
          readOnly={readOnly}
          isMarker />
      }
      {
        layers.map(l => <EditProps key={l.feature.properties.id} 
          properties={l.feature.properties}
          save={save_}
          groups={groups}
          readOnly={readOnly} />
        )
      }
    </div>
  )
}

export default PropsControl