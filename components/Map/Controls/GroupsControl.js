import { useState } from 'react'

import { isDarkColor } from '@lib/helpers/leaflet'

import EditGroupsControl from './EditGroupsControl'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { 
  faMaximize, faMinimize, faEyeSlash, faEdit
} from '@fortawesome/free-solid-svg-icons'

import styles from '../map.module.css'


const GroupsControl = ({ 
  groups, 
  onChangeVisibility, 
  visibleGroups, 
  save,
  readOnly
}) => {
  const [editing, setEditing] = useState(false)
  const [isMinimized, setIsMinimized] = useState(false)

  const handleVisibilityChange = (e) => {
    const id = e.target.name
    const visible = e.target.checked
    onChangeVisibility(id, visible)
  }

  const toggleMinimize = () => {
    setIsMinimized(!isMinimized)
  }

  const startEditing = () => {
    if (!readOnly) setEditing(true)
  }

  if (editing) 
    return <EditGroupsControl 
      groups={groups} 
      setEditing={setEditing}
      save={save}/>

  return (
    <div className={styles['control']}>
      
      <div className={`${styles['control-header']} ${isMinimized? 'rounded':''}`}>
          {!isMinimized && <>
            <input type='checkbox' name='ALL' 
              className='mr-1'
              checked={groups.every(g => visibleGroups[g.id])} 
              onChange={handleVisibilityChange}/> 
            <label className='grow'>Groupes</label>
          </>}
          {readOnly ?
            <button className='btn btn-gray p-1 ml-2' onClick={toggleMinimize}>
              <FontAwesomeIcon icon={isMinimized? faMaximize:faMinimize}/>
            </button>
            :
            <button className='btn btn-gray p-1 ml-2' onClick={startEditing}>
              <FontAwesomeIcon icon={faEdit}/>
            </button>
          }
      </div>

      <div className={`${isMinimized? 'hidden':''} p-2`}>
        {groups.map(g => 
          <div key={'group'+g.id}>
            <input type='checkbox' name={g.id} checked={!!visibleGroups[g.id]} 
              onChange={handleVisibilityChange} className='mr-1'/>
            <label 
              className={styles['group-label-'+(isDarkColor(g.color)?'dark':'light')]} 
              style={{color: g.color}}>
              {g.name}
            </label>
            {!!g.admin && <FontAwesomeIcon className='ml-1' icon={faEyeSlash}/>}
          </div>
        )}
      </div>
    </div>
  )
}

export default GroupsControl