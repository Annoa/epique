/* eslint-disable jsx-a11y/anchor-is-valid */

import { useParams } from 'react-router'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { getAppUrl } from '@helpers/app'

import { 
  faFloppyDisk, faDownload, faUpload
} from '@fortawesome/free-solid-svg-icons'

import styles from '../map.module.css'

const ExportButton = (
  { groups, editableLayers, exportFunc }
) => {
  let { id } = useParams()

  const handleExportClick = () => {
    exportFunc(editableLayers.current.toGeoJSON(), groups)
  }
  
  return (
    <div className="top-left-button-container">
      <a className={style['control-button']} onClick={handleExportClick} title="Sauvegarder (envoyer au serveur)" href="#" role="button">
        <FontAwesomeIcon icon={faFloppyDisk}/>
      </a>
      <a className={style['control-button']} title="Télécharger les groupes (depuis le serveur)" 
        href={ getAppUrl() + id +"/groups.json" } target="_blank" rel="noreferrer" >
        <FontAwesomeIcon icon={faDownload}/><span>G</span>
      </a>
      <a className={style['control-button']} title="Télécharger les données géographiques (depuis le serveur)" 
        href={ getAppUrl() + id +"/map" } target="_blank" rel="noreferrer" >
        <FontAwesomeIcon icon={faDownload}/><span>D</span>
      </a>
      <a className={style['control-button']} onClick={() => {}} title="Téléverser (WIP)" href="#" role="button">
        <FontAwesomeIcon icon={faUpload}/>
      </a>
    </div>
  )
}

export default ExportButton