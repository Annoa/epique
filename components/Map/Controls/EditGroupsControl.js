import { useEffect, useRef, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { 
  faEye, faEyeSlash, faFloppyDisk, 
  faPlus, faTrashCan, faXmark 
} from '@fortawesome/free-solid-svg-icons'

const DEFAULT_COLOR = '#000'

import styles from '../map.module.css'
import useKeySave from '@hooks/useKeySave'

const EditGroupsControl = ({ 
  groups = [],
  save,
  setEditing
}) => {
  const [groups_, setGroups_] = useState([])
  const elemRef = useRef()

  useEffect(() => {
    setGroups_(groups)
  }, [groups])

  const updateGroup = ({id, name, color, admin}) => {
    const newGroups = groups_.map(g => ({
      ...g, 
      name: (typeof name === 'string' && g.id === id)? name:g.name,
      color: (color && g.id === id)? color:g.color,
      admin: (typeof admin === 'boolean' && g.id === id)? admin:g.admin
    }))
    setGroups_(newGroups)
  }
  
  const addGroup = () => {
    const id = groups_.reduce((acc,item)=> Math.max(acc, item.id), 0) + 1
    setGroups_([...groups_, {
      id,
      name: '',
      color: DEFAULT_COLOR
    }])
  }

  const deleteGroup = (id) => {
    setGroups_(groups_.filter(g => g.id !== id))
  }

  const handleSave = () => {
    save(groups_)
    setEditing(false)
  }

  useKeySave({
    elemRef,
    close: setEditing,
    save,
    value: groups_
  })

  return (
    <div className={styles['control']} ref={elemRef}>
      <div className={styles['control-header']}>
        <label>Groupes</label>
      </div>
      <div className='p-2'>
        {groups_.map(({ id, color, name, admin }) => 
          <div key={'group'+id} className='flex items-center mb-1'>
            <input type='color' className='h-6' value={color} 
              onChange={e => updateGroup({id, color:e.target.value})}/>
            <input type='text' className='mx-1' value={name} 
              placeholder='Nom' autoFocus
              onChange={e => updateGroup({id, name:e.target.value})}/>
            <button className='btn p-1 mr-1'  
              onClick={() => updateGroup({id, admin:!admin})}>
              <FontAwesomeIcon icon={admin?faEyeSlash:faEye}/>
            </button>
            <button className='btn p-1' 
              onClick={() => deleteGroup(id)}>
              <FontAwesomeIcon icon={faTrashCan}/>
            </button>
          </div>
        )}

        <div className='text-center my-2'>
          <button className='btn p-1'  onClick={addGroup}>
            <FontAwesomeIcon icon={faPlus} />
          </button>
          <button className='btn p-1 mx-1' onClick={handleSave}>
            <FontAwesomeIcon icon={faFloppyDisk} />
          </button>
          <button className='btn p-1' onClick={() => setEditing(false)}>
            <FontAwesomeIcon icon={faXmark} />
          </button>
        </div>

      </div>
    </div>
  )
}

export default EditGroupsControl