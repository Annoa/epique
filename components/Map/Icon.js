import ReactDOMServer from 'react-dom/server'
import L from 'leaflet'
import Icon from '@components/Icon'

// import * as Markers from '@assets/map/markers'


const getIconAnchor = (name) => {
  const bottomAnchor = ['mapMarkerAlt', 'mapMarker', 'mapPin']
  return bottomAnchor.includes(name)? [9, 22] : [9, 11]
}

export const MarkerIcon = (name, className, style) => {
  return L.divIcon({ 
    html: ReactDOMServer.renderToStaticMarkup(
      <Icon name={name} 
        fallback='mapMarkerAlt'
        style={style}
        className={className}/>
    ), 
    iconSize: [18, 22], 
    iconAnchor: getIconAnchor(name||'mapMarkerAlt'), 
    tooltipAnchor: [0, -25],
    className: 'map-svg-icon'
  })
}

export const PingIcon = (color = '#fff') => {
  return L.divIcon({ 
    html: ReactDOMServer.renderToStaticMarkup(<div className="ping-container">
      <div className="ping" style={{
        background: color,
        borderColor: color
      }}></div>
    </div>),
    className: 'dummy'
  })
}