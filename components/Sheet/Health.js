import { useEffect, useState, useReducer, useCallback } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { getInit, getMaxRessource } from '@lib/helpers/epique'
import { useSheetCtx } from '@contexts/sheetCtx'
import { 
  faBrain, faFloppyDisk, faHeart, faHeartPulse, 
  faPenToSquare, faXmark, faWeightHanging 
} from '@fortawesome/free-solid-svg-icons'
import { EMANATION } from '@constants/epique'

import SavingStatus from '@components/SavingStatus'
import CountInput from './CountInput'
import RessourceIcon from '@components/RessourceIcon'
import Barometer from './Barometer'


const HealthItem = ({
  value, 
  editing,
  name,
  updateItem,
  saveItemCurrent
}) => {
  const { base, current } = value || {}

  const onChange = e => {
    const {name: fieldName, value} = e.target
    let val_ = value
    if (['fieldName'].includes(fieldName)) {
      val_ = value.trim()
    } else if(e.target.type === 'number') {
      val_ = parseInt(value)
    }
    updateItem(name, { [fieldName]: val_})
  }

  const saveCurrent = useCallback(current => {
    saveItemCurrent(name, current)
  }, [name, saveItemCurrent])

  return editing?
    <div>
      <input name="base" type="number" value={base} onChange={onChange} />
    </div>
    :
    <CountInput 
      value={current} 
      max={base} 
      min={0} 
      onChangeCb={saveCurrent}/>
}

const Health = () => {
  const { sheet, data, save, savingStatus } = useSheetCtx({dataPath:'health'})
  const [editing, toggleEditig] = useReducer(prev => !prev, false)
  const [value, setValue] = useState()
  const { saturation = {}, kg = {}, factor = {}} = sheet._stats || {}
  
  useEffect(() => {
    setValue(data)
  }, [data, editing])

  const saveAll = () => {
    save({
      path: 'health', 
      value
    })
    toggleEditig()
  }

  const saveItemCurrent = useCallback((item, current) => {
    save({
      path: `health/${item}/current`, 
      value: current || 0
    })
  }, [save])
  
  const updateItem = (name, val) => {
    setValue(prev => ({
      ...prev,
      [name]: {...prev[name], ...val}
    }))
  }
  const maxEndurance = getMaxRessource(sheet.ressources?.endurance)
  const maxVolonte = getMaxRessource(sheet.ressources?.volonte)
  const maxMana = getMaxRessource(sheet.ressources?.mana)
  const init = getInit(sheet || {})

  let enduranceEmanation = ""
  let volonteEmanation = ""
  let manaEmanation = ""

  const keys = Object.keys(EMANATION)
  keys.forEach(threshold => {
    let nextIndex = keys.indexOf(threshold) + 1
    let nextThreshold = keys[nextIndex]
    if (maxEndurance > threshold && maxEndurance <= nextThreshold) {
      enduranceEmanation = EMANATION[nextThreshold]
    }
    if (maxVolonte > threshold && maxVolonte <= nextThreshold) {
      volonteEmanation = EMANATION[nextThreshold]
    }
    if (maxMana > threshold && maxMana <= nextThreshold) {
      manaEmanation = EMANATION[nextThreshold]
    }
  })


  return (
    <div className=''>
      <h2 id="health" className='flex items-center'>
        Attributs secondaires
        
        {editing ?
        <>
          <button className='btn mx-1' onClick={saveAll}>
            <FontAwesomeIcon icon={faFloppyDisk}/>
          </button>
          <button className='btn' onClick={toggleEditig}>
            <FontAwesomeIcon icon={faXmark}/>
          </button>
        </>
        :
        <button className='btn mx-1' onClick={toggleEditig}>
          <FontAwesomeIcon icon={faPenToSquare}/>
        </button>
        }

        <SavingStatus status={savingStatus}/>
      </h2>
      <div className="card bg-ink-500/20 p-2">
        <div className='background-parchment-img'/>
        <div className="grid grid-cols-[min-content_1fr_min-content] gap-4">
          <div><FontAwesomeIcon icon={faHeart} color="#c12929"/></div>
          <span className='font-bold'>PV</span> 
          <HealthItem 
            name="pv" 
            editing={editing}
            updateItem={updateItem}
            value={value?.pv}
            saveItemCurrent={saveItemCurrent}/>

          <div><FontAwesomeIcon icon={faHeartPulse} color="#7c29c1"/></div>
          <span className='font-bold'>Vitalité</span> 
          <HealthItem 
            name="vitality" 
            editing={editing}
            updateItem={updateItem}
            value={value?.vitality}
            saveItemCurrent={saveItemCurrent}/>

          <div><FontAwesomeIcon icon={faBrain} color="#295dc1"/></div>
          <span className='font-bold'>Aplomb</span> 
          <HealthItem 
            name="mind" 
            editing={editing}
            updateItem={updateItem}
            value={value?.mind}
            saveItemCurrent={saveItemCurrent}/>
        </div>

        <div className=' p-1 mt-2 bg-white/20'>
          <div className=''>
            <span className='font-bold'>Émanations :</span>
            <RessourceIcon name='endurance' className='mx-1'/>
            {enduranceEmanation}, 
            <RessourceIcon name='volonte' className='mx-1'/>
            {volonteEmanation}, 
            <RessourceIcon name='mana' className='mx-1'/>
            {manaEmanation}
          </div>
          <div className='border-t border-ink-100'>
            <span className='font-bold'>Facteurs :</span> 
            <RessourceIcon name='endurance' className='mx-1'/>
            {factor.endurance}, 
            <RessourceIcon name='volonte' className='mx-1'/>
            {factor.volonte}, 
            <RessourceIcon name='mana' className='mx-1'/>
            {factor.mana}
          </div>
          <div className='border-t border-ink-100'>
            <span className='font-bold'>Saturations :</span> 
            <RessourceIcon name='endurance' className='mx-1'/>
            <Barometer {...(saturation.endurance||{})}/>
            <RessourceIcon name='volonte' className='mx-1'/>
            <Barometer {...(saturation.volonte||{})}/>
            <RessourceIcon name='mana' className='mx-1'/>
            <Barometer {...(saturation.mana||{})}/>
          </div>
          <div className='border-t border-ink-100'>
            <FontAwesomeIcon className='mr-1' icon={faWeightHanging} />
            {sheet._stats?.kg?.current}/{sheet._stats?.kg?.maxCharge}kg 
            {' '}
            {sheet._stats?.kg?.malusClutter && 
            <span>
              ({sheet._stats?.kg?.malusClutter}<RessourceIcon name='endurance'/>)
            </span>
            }
          </div>
          <div>
            Init : {init}
          </div>
        </div>

      </div>
    </div>
  )
}

export default Health