import RessourceIcon from '@components/RessourceIcon'
import { Fragment } from 'react'

const RessourceTag = ({
  value,
  type,
  bonus
}) => 
  Object.keys(value).some(r => value[r]) ?
    <div className={`badge-${type}`}>
      {type === 'reservation' && <span className='mr-1'>Réserve</span>}
      {Object.keys(value).map((ressource, i) => {
        if (!value[ressource]) return null
        const v = Math.round(value[ressource] * (bonus||1))
        return <Fragment key={ressource}>
          {type==='activation' && ressource==='mana'? Math.max(v,1):v}
          <RessourceIcon name={ressource} className='mx-1' fallback/>
        </Fragment>
      })}
    </div>
  : null

export default RessourceTag