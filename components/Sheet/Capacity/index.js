/* eslint-disable react/no-unescaped-entities */
import dynamic from 'next/dynamic'
import { useCallback, useEffect, useReducer, useRef, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import TagsInput from 'react-tagsinput'

import { onParsedDivUpdate } from '@lib/helpers/parsers'
import { coeffToPercent, multipleAll } from '@lib/helpers/math'
import { useSheetCtx } from '@contexts/sheetCtx'
import { capacityModel } from '@lib/data/models/sheetModel'
import useKeySave from '@hooks/useKeySave'

import { Checkbox } from '@components/Fields'
import SavingStatus from '@components/SavingStatus'
import MetaPanel from './MetaPanel'
import RessourceTag from './RessourceTag'
import Alert from '@components/modals/Alert'
import RessourceIcon from '@components/RessourceIcon'
import Unkeeps from './Unkeeps'
import hljs from 'highlight.js/lib/core'

import styles from './capacity.module.css'
import { CAPACITY_TYPES, CAPACITY_TYPE_EXECUTION, CAPACITY_TYPE_DECLENCHMENT, CAPACITY_TYPE_PERMANENCE } from '@constants/epique'

import { 
  faFloppyDisk, faMask, faPenToSquare, 
  faXmark, faTrashAlt, faCircleHalfStroke, faHandSparkles, 
} from '@fortawesome/free-solid-svg-icons'

const TextField = dynamic(
  () => import('@components/TextField'), 
  { ssr:false }
)

const Capacity = ({
  editing,
  index,
  toggleEdit
}) => {
  const { 
    data = capacityModel, 
    save, savingStatus, sheet
   } = useSheetCtx({dataPath: `capacities/${index}`})
  const [isOpen, toggleOpen] = useReducer((prevIsOpen) => !prevIsOpen, false)
  const [value, setValue] = useState(data)
  const [capacityType, setCapacityType] = useState(value.capacityType || CAPACITY_TYPE_EXECUTION)
  const options = sheet.options
  const elemRef = useRef()

  const isActivable = Object.keys(data?.activation||{}).filter(r => data?.activation[r]).length > 0
  
  useEffect(() => {
    setValue(data)
  }, [data, editing])
  
  useKeySave({elemRef, save, value, dataPath:`capacities/${index}`})

  const handleSave = useCallback(async () => {
    const saved = await save({
      path: `capacities/${index}`,
      value
    })
    if (saved) {
      toggleEdit()
    }
  }, [index, save, toggleEdit, value])

  const useCapacity = () => {
    const cost = Object.keys(data?.activation||{}).filter(r => data?.activation[r])
    const value = {...sheet.ressources}
    cost.forEach(r => {
      let x = (Math.round(data.activation[r] * multipleAll(data.activationBonus))) || 1
      value[r].current = value[r].current - x
    })
    save({
      path: 'ressources',
      value
    })
  }

  const handleDelete = () => {
    const capacities = [...sheet.capacities]
    capacities.splice(index, 1)
    save({
      path: 'capacities',
      value: capacities,
      overwrite: true
    }).then(() => toggleEdit())
  }

  const onCheckboxChange = e => {
    const { name, checked } = e.target
    setValue(prev => ({
      ...prev,
      [name]: checked
    }))
  }

  const onTagsInputChange = (key, arr) => {
    const parsedArr = arr.map(parseFloat)
    setValue(prev => ({...prev, [key]: parsedArr})) 
  }

  const onStrChange = (key, str) => {
    setValue(prev => ({...prev, [key]: str}))
  }

  const onCapacityTypeChange = (key, str) => {
    setCapacityType(str)
    setValue(prev => ({...prev, [key]: str}))

    const language = 'capacities'
    const highlightedCode = hljs.highlight(value.desc, {language}).value
    const div = document.createElement('div')
    div.innerHTML = highlightedCode
    value.capacityType = str

    onParsedDivUpdate({
      node: div,
      parser: 'capacities',
      setValue,
      instance: value
    })
  }

  const onDivUpdate = useCallback((node, instance) => {
    onParsedDivUpdate({
      node,
      parser: 'capacities',
      setValue,
      instance
    })
  }, [])

  let efficacityBonusPercent = data.efficacityBonus
  let activationBonusPercent = data.activationBonus
  let activationBonus = multipleAll(data.activationBonus)
  let efficacityBonus = multipleAll(data.efficacityBonus)

  if (!!data.distribution && !!data.distribution.total && !!data.reservation && data.capacityType === CAPACITY_TYPE_DECLENCHMENT) {
    efficacityBonusPercent = Math.round(coeffToPercent(efficacityBonusPercent) * 0.75)
    activationBonusPercent = Math.round(coeffToPercent(activationBonusPercent) * 0.5)
    activationBonus = 1 - (1-activationBonus)*0.5
    efficacityBonus = (efficacityBonus-1)*0.75 + 1
  } else if (!!data.distribution && !!data.distribution.total && !!data.reservation && data.capacityType === CAPACITY_TYPE_PERMANENCE) {
    efficacityBonusPercent = Math.round(coeffToPercent(efficacityBonusPercent) * 0.5)
    efficacityBonus = (efficacityBonus-1)*0.5 + 1
    activationBonusPercent = null
    activationBonus = null
  } else {
    efficacityBonusPercent = coeffToPercent(efficacityBonusPercent)
    activationBonusPercent = coeffToPercent(activationBonusPercent)
  }

  return <div 
    className={`card p-0 relative overflow-visible
      ${editing?'':'cursor-pointer'} ${!isOpen?'card-collapsed':''}
    `} 
    onClick={toggleOpen} ref={elemRef}
  >
    {editing?
      <>
        <div className='bg-ink-300/40 p-2'>

          <div className='flex items-center justify-between gap-1 mb-1'>
            <input 
              type='text' 
              value={value.name}
              placeholder='Nom de la capacité'
              onChange={e => onStrChange('name', e.target.value)}
            />
            <SavingStatus status={savingStatus}/>
            <button className='btn' onClick={handleSave}>
              <FontAwesomeIcon icon={faFloppyDisk}/>
            </button>
            <button className='btn' onClick={toggleEdit}>
              <FontAwesomeIcon icon={faXmark}/>
            </button>
            <Alert
              title={`Supprimer "${data.name}" ?`}
              description="Attention : les champs en cours d'édition seront perdus."
              okTxt='Oui, supprimer'
              onOkClick={handleDelete}
              button={
                <button className='btn'>
                  <FontAwesomeIcon icon={faTrashAlt}/>
                </button>
              }
            />
          </div>

          <div className='flex items-center justify-between gap-1 mb-1'>
            <div>
              {value.pc !== data.pc && <span className='line-through mx-1'>{data.pc}</span>}
              {value.pc}{value.isSouffle?'ps':'pc'}
              {value.ner > 0 && 
                <span className='mx-1'>
                  {value.ner !== data.ner && <span className='line-through mx-1'>{data.ner||0}</span>}
                  {value.ner}ner
                </span>
              }
            </div>
            
            <div>
              <Checkbox 
                id='draft' 
                className='inline-block px-1 mr-1 rounded bg-ink-300/30'
                label={<FontAwesomeIcon icon={faMask} />} 
                defaultChecked={value.draft} 
                onChange={onCheckboxChange}
              />

              <Checkbox 
                id='isSouffle' 
                className='inline-block px-1 rounded bg-ink-300/30'
                label={<RessourceIcon name='souffle'/>} 
                defaultChecked={value.isSouffle} 
                onChange={onCheckboxChange}
              />
            </div>
            
          </div>

          <div className='grid grid-cols-2 gap-1 mb-1'>
            <div className='px-1 rounded bg-ink-300/30'>
              <label htmlFor='efficacity-bonus'>Bonus d'efficacité </label>
              <TagsInput
                value={value.efficacityBonus} 
                validationRegex={/^-?\d+$/}
                inputProps={{placeholder: '%'}}
                onChange={arr => onTagsInputChange('efficacityBonus', arr)} />
            </div>

            <div className='px-1 rounded bg-ink-300/30'>
              <label htmlFor='activation-bonus'>Bonus d'activation </label>
              <TagsInput 
                value={value.activationBonus} 
                validationRegex={/^-?\d+$/}
                inputProps={{placeholder: '%'}}
                onChange={arr => onTagsInputChange('activationBonus', arr)} />
            </div>

            <div className='px-1 rounded bg-ink-300/30'>
              <select name='ressource' onChange={e => onCapacityTypeChange('capacityType', e.target.value)} value={value.capacityType}>
                {CAPACITY_TYPES.map(r => <option key={r} value={r}>
                  {r}
                </option>)}
              </select>
            </div>
          </div>

          <Unkeeps 
            value={value.unkeeps}
            setValue={v => setValue(prev => ({...prev, unkeeps:v}))}/>

        </div>
        <div className='p-2'>
          <TextField
            content={value.desc}
            setContent={desc => onStrChange('desc', desc)}
            // onKeyDown={handleKeySave}
            parser='capacities'
            onParsedDivUpdate={(node) => onDivUpdate(node, value)}
          />    
        </div>
      </>
      :
      <>
        <h3 id={data.name || 'Nom de la capacité'} className='mb-2 pt-2'>
          {data.name || 'Nom de la capacité'}
          <button className='btn ml-1' onClick={toggleEdit}>
            <FontAwesomeIcon icon={faPenToSquare}/>
          </button>
          {isActivable && <button className='btn ml-1' onClick={useCapacity}>
            <FontAwesomeIcon icon={faHandSparkles}/>
          </button>}
        </h3>

        <div className='p-2 mb-2 flex gap-1 flex-wrap-reverse'>
          <div className={`badge-${data.isSouffle? 'souffle':'pc'} ${data.draft?'draft':''}`}>
            {data.draft && <FontAwesomeIcon icon={faMask} className='mr-1' />}
            {data.pc} {data.isSouffle? 'PS':'PC'}
          </div>
          {data.ner > 0 &&
            <div className='badge-ner'>{data.ner} NER</div>
          }
          {/* todo: ask tristan ! */}
          {!!data.distribution && !!data.distribution.total && data.capacityType !== CAPACITY_TYPE_PERMANENCE && !!data.activation &&
            <RessourceTag type='activation' value={data.activation} bonus={activationBonus}/>
          }
          {!!data.distribution && !!data.distribution.total && !!data.reservation && (data.capacityType === CAPACITY_TYPE_PERMANENCE || data.capacityType === CAPACITY_TYPE_DECLENCHMENT) &&
            <RessourceTag type='reservation' value={data.reservation}/>
          }

          {data.unkeeps.map((e,i) => <div className='badge bg-indigo-400' key={i}>
            {e.operator}{e.nb}
            <RessourceIcon name={e.ressource} className='mx-1' fallback/>
            / {e.interval}
            {!!e.condition && 
              <FontAwesomeIcon icon={faCircleHalfStroke} className='ml-1'/>
            }
          </div>)}

        </div>

        <MetaPanel
          meta={data.meta}
          prop={data.prop}
          bonus={efficacityBonus}
          efficiencies={options.efficiencies}
        /> 
        
        <div className={styles['ribbons-container']}>
          {efficacityBonusPercent && 
            <div className={styles['efficacity-bonus']}>
              <div className={styles['css-ribbon']}/>
              <div>Eff.</div>
              <div>{efficacityBonusPercent}%</div>
            </div>
          }
          {activationBonusPercent && 
          <div className={styles['activation-bonus']}>
            <div className={styles['css-ribbon']}/>
            <div>Act.</div>
            <div>{activationBonusPercent}%</div>
          </div>
          }
        </div> 
        
        <div 
          className='inner-html-container p-2'
          dangerouslySetInnerHTML={{ __html: data._html }}
        />
      </>
    }
  </div>
}

Capacity.displayName = 'Capacity'

export default Capacity