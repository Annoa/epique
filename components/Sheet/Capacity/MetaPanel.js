import { multipleAll } from '@lib/helpers/math'

const MetaPanel = ({
  meta, // les valeurs où le bonus d'efficacité s'applique
  prop, // les valeurs où le bonus d'efficience s'applique
  bonus,
  efficiencies
}) => {
  if (!meta || !Object.keys(meta)?.length) {
    return null
  }

  // const RELATION_EFFICIENCY_BRANCH = {
  //   "attackContact": ["Coup précis", "Distension"],
  //   "attackProjectile": ["Coup précis", "Distension"],
  //   "damageEndurance": [],
  //   "damageMana": [],
  //   "damageVolonte": [],
  //   "dodge": ["Esquive", "Distension"],
  //   "resistanceEndurance": [],
  //   "resistanceMana": ["Résiliance magique"],
  //   "resistanceVolonte": ["Résiliance psionique"],
  //   "shieldEndurance": [],
  //   "shieldMana": [],
  //   "shieldVolonte": [],
  // }

  const RELATION_BRANCH_EFFICIENCY = {
    "Esquive": "dodge",
    "Résiliance magique": "resistanceMana",
    "Résiliance psionique": "resistanceVolonte",
    "Chance de toucher (projectile)": "attackProjectile",
    "Chance de toucher (contact)": "attackContact",
    "Chance de toucher": "attackContact",
    "Dégâts [Tranchant]": "damageEndurance",
    "Dégâts [Perforant]": "damageEndurance",
    "Dégâts [Contondant]": "damageEndurance",
    "Points de vie": "pvs",
    "Réduction [Tranchant]": "resistanceEndurance",
    "Réduction [Perforant]": "resistanceEndurance",
    "Réduction [Contondant]": "resistanceEndurance",
    "Bloc": "shieldEndurance",
    "Force": "damageEndurance",
  }



  const renderProp = () => {
    if (!prop || !Object.keys(prop)?.length) {
      return null
    }
    return (
      Object.keys(prop).map((branch,i, arr) => (
        <div key={branch} className={`${arr.length===1?'w-full':'w-1/2'} text-center shrink-0`}>
          {branch} : {prop[branch].map((obj,i) => (<span key={i}>
            <span className={`${RELATION_BRANCH_EFFICIENCY[branch] && 'bold-blue'}`}>
              {Math.round(obj.value * multipleAll(efficiencies?.[RELATION_BRANCH_EFFICIENCY[branch]]))}
            </span>
            {obj.unit}
          </span>
        ))}
        </div>
      )
      )
    )
  }

  return <div 
    className='bg-gray-300 py-1 px-2 mx-[-3px] shadow-lg text-gray-700 flex flex-wrap'>
    {Object.keys(meta).map((branch,i, arr) => (
      <div key={branch} className={`${arr.length===1?'w-full':'w-1/2'} text-center shrink-0`}>
        {branch} : {meta[branch].map((obj,i) => (<span key={i}>
          <span>
            {Math.round(
              obj.value * (bonus||1) * multipleAll(efficiencies?.[RELATION_BRANCH_EFFICIENCY[branch]])
            )}
          </span>
          {obj.unit}
        </span>
      ))}
      </div>
    ))}
    {renderProp()}
  </div>
}

export default MetaPanel