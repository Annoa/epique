import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowDown, faArrowUp, faEraser, faPlus } from '@fortawesome/free-solid-svg-icons'

import { OPERATORS, PRIMARY_RESSOURCES, RECOVERIES, RESSOURCES } from '@constants/epique'
import { unkeepModel } from '@lib/data/models/sheetModel'

const Unkeeps = ({
  value,
  setValue
}) => {

  const onChange = (index, event) => {
    let val = event.target.value
    if (event.target.name === 'nb') {
      val = parseInt(val) || 0
    }
    setValue(value.map((e, i) => i === index ?
      ({...e, [event.target.name]: val}) : e
    ))
  }

  const up = i => {
    if (i < 1) return
    let newValue = [...value]
    let tmp = newValue[i-1]
    newValue[i-1] = newValue[i]
    newValue[i] = tmp
    setValue(newValue)
  }

  const down = i => {
    if (i >= value.length-1) return
    let newValue = [...value]
    let tmp = newValue[i+1]
    newValue[i+1] = newValue[i]
    newValue[i] = tmp
    setValue(newValue)
  }

  
  return <div className='p-1 rounded bg-ink-300/30'>
      {value.map((e,i) => <div key={i} className='mb-2'>
        <div className='flex items-center gap-1'>
          <select name='operator' onChange={e => onChange(i, e)} value={e.operator}>
            {Object.keys(OPERATORS).map(o => <option key={o} value={o}>
              {OPERATORS[o]}
            </option>)}
          </select>
          <input 
            name='nb' 
            onChange={e => onChange(i, e)} 
            type='number' 
            min={0}
            value={e.nb}/>
          <span>de</span>
          <select name='ressource' onChange={e => onChange(i, e)} value={e.ressource}>
            {PRIMARY_RESSOURCES.map(r => <option key={r} value={r}>
              {RESSOURCES[r]}
            </option>)}
          </select>
          <button className='inline-btn'
            disabled={i===0}
            onClick={() => up(i)}>
            <FontAwesomeIcon icon={faArrowUp}/>
          </button>
        </div>
  
        <div className='flex items-center gap-1'>
          <span className='whitespace-nowrap'>tous les</span>
          <select name='interval' onChange={e => onChange(i, e)} value={e.interval}>
            {RECOVERIES.map(recovery => <option key={recovery} value={recovery}>
              {recovery}
            </option>)}
          </select>

          <button className='inline-btn'
            onClick={() => setValue(value.filter((e,j) => j!==i))}>
            <FontAwesomeIcon icon={faEraser}/>
          </button>
          
        </div>

        <div className='flex items-center gap-1'>
          <span className='whitespace-nowrap'>Condition:</span>
          <input 
            name='condition' 
            onChange={e => onChange(i, e)} 
            type='text' 
            placeholder='Laisser vide si aucune condition.'
            value={e.condition}/>
          <button className='inline-btn'
            disabled={i>=value.length-1}
            onClick={() => down(i)}>
            <FontAwesomeIcon icon={faArrowDown}/>
          </button>
        </div>
      </div>
      )}
    
    <div className='text-center'>
      <button className='outline-btn' 
        onClick={() => setValue([...value, unkeepModel])}>
        <FontAwesomeIcon icon={faPlus} className='mr-1'/> 
        Ajouter
      </button>
    </div>
  </div>
}

export default Unkeeps