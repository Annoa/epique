/* eslint-disable react/no-unescaped-entities */
import { useEffect, useState } from 'react'
import TagsInput from 'react-tagsinput'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFloppyDisk } from '@fortawesome/free-solid-svg-icons'

import SavingStatus from '@components/SavingStatus'
import { useSheetCtx } from '@contexts/sheetCtx'

const ROWS = [
  {label: 'Esquive', key: 'dodge'},
  {label: 'JdA (contact)', key: 'attackProjectile'},
  {label: 'JdA (projectile)', key: 'attackContact'},
  {label: 'Résiliances & Résistances (Endurance)', key: 'resistanceEndurance'},
  {label: 'Résiliances & Résistances (Mana)', key: 'resistanceMana'},
  {label: 'Résiliances & Résistances (Volonté)', key: 'resistanceVolonte'},
  {label: 'Bouclier (lié Endurance)', key: 'shieldEndurance'},
  {label: 'Bouclier (lié Mana)', key: 'shieldMana'},
  {label: 'Bouclier (lié Volonté)', key: 'shieldVolonte'},
  {label: 'Dégâts (lié Endurance)', key: 'damageEndurance'},
  {label: 'Dégâts (lié Mana)', key: 'damageMana'},
  {label: 'Dégâts (lié Volonté)', key: 'damageVolonte'},
  {label: 'PVs', key: 'pvs'},
]

let ROWS1 = ROWS.slice(0, Math.ceil(ROWS.length/2))
let ROWS2 = ROWS.slice(Math.ceil(ROWS.length/2), ROWS.length)

const Efficiencies = () => {
  const { 
    data, save, savingStatus 
  } = useSheetCtx({dataPath:'options/efficiencies'})
  const [efficiencies, setEfficiencies] = useState({})

  useEffect(() => {
    if (data) {
      setEfficiencies(data)
    }
  }, [data])

  const onInputChange = (key, arr) => {
    const parsedArr = arr.map(parseFloat)
    setEfficiencies(prev => ({...prev, [key]: parsedArr})) 
  }

  const handleSave = () => {
    save({
      path: 'options/efficiencies', 
      value: efficiencies
    })
  }

  return <div className='card col-span-2'>
      <h2 id='efficiencies'>Bonus d'efficience      
        <button className='btn ml-1' onClick={handleSave}>
          <FontAwesomeIcon icon={faFloppyDisk}/>
        </button>
        <SavingStatus status={savingStatus}/>
      </h2> 
    <div className='md:columns-2'>
      <table className='w-full'>
        <thead>
          <tr>
            <th className='w-1/3'>Nom</th>
            <th>Bonus d'Efficience</th>
          </tr>
        </thead>
        <tbody>
          {ROWS1.map(({label, key}) => <tr key={key}>
            <td>{label}</td>
            <td>
              <TagsInput 
                className='react-tagsinput -mb-1.5'
                value={efficiencies[key] || []} 
                validationRegex={/^-?\d+$/}
                inputProps={{placeholder: '%'}}
                onChange={arr => onInputChange(key, arr)} 
                />
            </td>
          </tr>)}
        </tbody>
      </table>
      <table className='w-full'>
        <thead>
          <tr>
            <th className='w-1/3'>Nom</th>
            <th>Bonus d'Efficience</th>
          </tr>
        </thead>
        <tbody>
          {ROWS2.map(({label, key}) => <tr key={key}>
            <td>{label}</td>
            <td>
              <TagsInput 
                className='react-tagsinput -mb-1.5'
                value={efficiencies[key] || []} 
                validationRegex={/^-?\d+$/}
                inputProps={{placeholder: '%'}}
                onChange={arr => onInputChange(key, arr)} 
                />
            </td>
          </tr>)}
        </tbody>
      </table>
    </div>
  </div>
}

export default Efficiencies