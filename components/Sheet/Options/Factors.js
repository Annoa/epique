import { useEffect, useState } from 'react'
import TagsInput from 'react-tagsinput'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFloppyDisk } from '@fortawesome/free-solid-svg-icons'

import { useSheetCtx } from '@contexts/sheetCtx'

import SavingStatus from '@components/SavingStatus'
import RessourceIcon from '@components/RessourceIcon'

const ROWS = [
  { label: 'Endurance', key: 'endurance'},
  { label: 'Volonté', key: 'volonte'},
  { label: 'Mana', key: 'mana'},
]

const Factors = () => {
  const { 
    data, save, savingStatus, sheet
  } = useSheetCtx({dataPath:'options/factors'})
  const stats = sheet._stats || {}
  const [factors, setFactors] = useState({})

  useEffect(() => {
    if (data) {
      setFactors(data)
    }
  }, [data])

  const onInputChange = (key, arr) => {
    const parsedArr = arr.map(parseFloat)
    setFactors(prev => ({...prev, [key]: parsedArr}))
  }

  const handleSave = () => {
    save({
      path: 'options/factors', 
      value: factors
    })
  }

  return <div className='card'>
    <h2 id='factors'>Facteurs      
      <button className='btn ml-1' onClick={handleSave}>
        <FontAwesomeIcon icon={faFloppyDisk}/>
      </button>
      <SavingStatus status={savingStatus}/>
    </h2> 
    <table className='w-full'>
      <thead>
        <tr>
          <th>Nom</th>
          <th>Total</th>
          <th className='w-full'>Bonus de facteur</th>
        </tr>
      </thead>
      <tbody>
        {ROWS.map(({key, label}) => <tr key={key}>
          <td className='whitespace-nowrap'>
            <RessourceIcon name={key}/> {label}
          </td>
          <td>
            {stats.factor?.[key]}
          </td>
          <td>
            <TagsInput 
              className='react-tagsinput -mb-1.5'
              value={factors[key] || []} 
              validationRegex={/^-?\d+$/}
              inputProps={{placeholder: '%'}}
              onChange={arr => onInputChange(key, arr)} 
              />
          </td>
        </tr>)}
      </tbody>
    </table>
  </div>
}

export default Factors