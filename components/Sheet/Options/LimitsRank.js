import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { 
  faBookmark, faDungeon, faFireFlameCurved, 
  faPuzzlePiece, faWandSparkles 
} from '@fortawesome/free-solid-svg-icons'
import SavingStatus from '@components/SavingStatus'
import { useSheetCtx } from '@contexts/sheetCtx'

const COLS = [
  {number: '1',  root: '14', skill: '16', feat: '35', item: '105', archetype: '225'},
  {number: '2',  root: '12', skill: '14', feat: '30', item: '90',  archetype: '200'},
  {number: '3',  root: '10', skill: '12', feat: '25', item: '75',  archetype: '175'},
  {number: '4',  root: '8',  skill: '10', feat: '20', item: '60',  archetype: '150'},
  {number: '5',  root: '6',  skill: '8',  feat: '15', item: '45',  archetype: '125'},
  {number: '6',  root: '4',  skill: '6',  feat: '10', item: '30',  archetype: '100'},
  {number: '6+', root: '2',  skill: '4',  feat: '8',  item: '15',  archetype: '75' },
]

// todo: icons constants
const ROWS = [
  {label: 'Racines', key: 'root', icon: faPuzzlePiece},
  {label: 'Compétences', key: 'skill', icon: faBookmark},
  {label: 'Dons', key: 'feat', icon: faFireFlameCurved},
  {label: 'Équipements', key: 'item', icon: faWandSparkles},
  {label: 'Archétypes', key: 'archetype', icon: faDungeon},
]

const LimitsRank = () => {
  const { 
    data = {}, save, savingStatus 
  } = useSheetCtx({dataPath:'options/limits'})

  const onChange = e => {
    const { name, value } = e.target
    console.log(name, value)
    save({
      path: `options/limits/${name}`,
      value
    })
  }
  
  return (
    <div className='card'>
      <h2 id='limits'>
        Rangs des limites
        <SavingStatus status={savingStatus}/>
      </h2>       
    <table className='w-full'>
      <thead>
        <tr>
          <th>Nom</th>
          {COLS.map(({ number }) => <th key={number}>Rang {number}</th>)}
        </tr>
      </thead>
      <tbody>
        {ROWS.map(({key, label, icon}) => <tr key={key}>
          <td className='whitespace-nowrap'>
            <FontAwesomeIcon icon={icon}/> {label}
          </td>
          {COLS.map(col => <td key={col.number} className='whitespace-nowrap'>
            <input 
              type='radio'
              name={key} 
              id={`${key}-${col.number}`}
              value={col[key]}
              checked={col[key] === data[key]}
              onChange={onChange}
              className='mr-1'
              />
            <label htmlFor={`${key}-${col.number}`}>1/{col[key]}</label>
          </td>)}
        </tr>)}
      </tbody>
    </table>
    </div>
  )
}

export default LimitsRank