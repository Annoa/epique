// import Symboles from '@assets/symboles'
import RessourceIcon from '@components/RessourceIcon'
import SavingStatus from '@components/SavingStatus'
import { useSheetCtx } from '@contexts/sheetCtx'

const COLS = [
  {number: '1',  recovery: '0'},
  {number: '2',  recovery: '2.5'},
  {number: '3',  recovery: '5'},
  {number: '4',  recovery: '10'},
  {number: '5',  recovery: '15'},
  {number: '6',  recovery: '25'},
  {number: '6+', recovery: '35'},
]

const ROWS = [
  {label: 'Endurance', key: 'endurance'},
  {label: 'Volonté', key: 'volonte'},
  {label: 'Mana', key: 'mana'},
]

const RecoveriesRank = () => {
  const { 
    data = {}, save, savingStatus 
  } = useSheetCtx({dataPath:'options/recoveries'})

  const onChange = e => {
    const {name, value} = e.target
    console.log(name, value, e.target)
    save({
      path: `options/recoveries/${name}`,
      value: value
    })
  }

  return <div className='card'>
    <h2 id='recoveries'>
      Rangs des récupérations
    <SavingStatus status={savingStatus}/>
    </h2>       
    <table className='w-full'>
      <thead>
        <tr>
          <th>Nom</th>
          {COLS.map(({ number }) => <th key={number} className='whitespace-nowrap'>
            Rang {number}
          </th>)}
        </tr>
      </thead>
      <tbody>
        {ROWS.map(({key, label}) => <tr key={key}>
          <td className='whitespace-nowrap'>
            <RessourceIcon name={key}/> {label}
          </td>
          {COLS.map(col => <td key={col.number} className='whitespace-nowrap'>
            <input 
              type='radio'
              name={key} 
              recoverytype={key}
              id={`${key}-${col.number}`}
              value={col.recovery}
              checked={col.recovery === data[key]}
              onChange={onChange}
              className='mr-1'
              />
            <label htmlFor={`${key}-${col.number}`}>{col.recovery}%</label>
          </td>)}
        </tr>)}
      </tbody>
    </table>
  </div>
  
}

export default RecoveriesRank