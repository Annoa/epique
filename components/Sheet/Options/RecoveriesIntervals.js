import RessourceIcon from '@components/RessourceIcon'
import SavingStatus from '@components/SavingStatus'
import { RECOVERIES, PRIMARY_RESSOURCES, RESSOURCES } from '@constants/epique'
import { useSheetCtx } from '@contexts/sheetCtx'
import { recoveriesIntervalModel } from '@lib/data/models/sheetModel'
import { useEffect } from 'react'

const RecoveriesIntervals = () => {
  const { 
    data = recoveriesIntervalModel, save, savingStatus 
  } = useSheetCtx({dataPath:'options/recoveries-intervals'})

  const onChange = e => {
    const {name, value} = e.target
    // console.log(name, value, e)
    save({
      path: `options/recoveries-intervals/${name}`,
      value: value
    })
  }

  useEffect(() => {console.log(data)}, [data])

  return <div className='card'>
    <h2 id='recoveries-intervals'>
      Intervalles des récupérations
    <SavingStatus status={savingStatus}/>
    </h2>       
    <table className='w-full'>
      <thead>
        <tr>
          <th>Nom</th>
          <th>Intervalle</th>
        </tr>
      </thead>
      <tbody>
        {PRIMARY_RESSOURCES.map(r => <tr key={r}>
          <td className='whitespace-nowrap'>
            <RessourceIcon name={r}/> {RESSOURCES[r]}
          </td>
          <td className=''>
            <select 
              name={r}
              key={data?.[r]}
              onChange={onChange} 
              defaultValue={data?.[r]}>
              {RECOVERIES.map(recovery => <option key={recovery} value={recovery}>
                {recovery}
              </option>)}
            </select>
          </td>
        </tr>)}
      </tbody>
    </table>
  </div>
  
}

export default RecoveriesIntervals