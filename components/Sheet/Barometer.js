import { Fragment } from 'react'

const Barometer = ({
  current = 0,
  thresholds = []
}) => {
  return <span className='whitespace-nowrap inline-flex p-0 rounded-md overflow-hidden border items-center border-ink-200 text-sm'>
    {thresholds.map((t, i) => <Fragment key={i}>
      {current>(thresholds[i-1] || -Infinity) && current<= t &&
        <span className='px-0.5 py-0 bg-white/20'>{current}</span>
      }
      <span className={`${
          i===0? 'bg-green-500/20'
        : i===1? 'bg-yellow-500/50'
        : i===2? 'bg-orange-500/50'
        : i===3? 'bg-red-500/50'
        : ''
      } px-0.5 py-0`}
    >
      {t}
      </span>
    </Fragment>)}
  </span>
}

export default Barometer