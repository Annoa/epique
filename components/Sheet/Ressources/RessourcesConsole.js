import { Fragment, useCallback, useReducer } from 'react'

import { RESSOURCES } from '@constants/epique'
import { useSheetCtx } from '@contexts/sheetCtx'
import { multipleAll } from '@lib/helpers/math'

import RessourceIcon from '@components/RessourceIcon'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCaretDown, faCaretUp } from '@fortawesome/free-solid-svg-icons'
import CountInput from '../CountInput'
import SavingStatus from '@components/SavingStatus'

const RessourcesConsole = () => {
  const { 
    data, 
    save,
    savingStatus
  } = useSheetCtx({dataPath:'ressources'})

  const [isOpen, toggleOpen] = useReducer((prevIsOpen) => !prevIsOpen, false)

  const saveCurrent = useCallback((current, ressource) => {
    save({
      path: `ressources/${ressource}/current`, 
      value: current || 0
    })
  }, [save])

  if (!data) return

  return <div className='absolute inset-x-0 bottom-0 flex justify-center lg:mr-[300px] z-[99]'>
    <div className='bg-parchment-img rounded-md shadow-lg'>
      <div className='bg-parchment-900/40 rounded-md shadow-lg py-1 px-3'>

        <div className='flex gap-2'>
          {Object.keys(RESSOURCES)
            .filter(id => data[id]?.base && id !== 'souffle').map(id => {
            const { 
              base, bonusFlat, percent, 
              reservation, convertion, current
            } = data[id]
            const max = Math.ceil((base+bonusFlat)*multipleAll(percent))
            return <div key={id} className='whitespace-nowrap'>
              <RessourceIcon name={id} className='mr-1'/>
              {current}
              /
              <span className=''>
                {max-reservation-convertion}
              </span>
            </div>
          })}

          <SavingStatus status={savingStatus}/>

          <button className='inline-btn' onClick={toggleOpen}>
            <FontAwesomeIcon icon={isOpen? faCaretDown:faCaretUp} className='mt-1'/>
          </button>
          
        </div>

        {isOpen && <div className='grid grid-cols-[auto_1fr] bg-white/20 mt-2'>
          {Object.keys(RESSOURCES)
            .filter(id => data[id]?.base && id !== 'souffle').map(id => {
            const { 
              base, bonusFlat, percent, 
              reservation, convertion, current
            } = data[id]
            const max = Math.ceil((base+bonusFlat)*multipleAll(percent))
            return <Fragment key={id}>
              <RessourceIcon name={id} className='mx-2'/>
              <CountInput
                value={current} 
                max={max-reservation-convertion} 
                min={0} 
                name={id}
                onChangeCb={saveCurrent}/>
            </Fragment>
          })}
        </div>}

      </div>
    </div>
  </div>
}

export default RessourcesConsole