import { useCallback } from 'react'

import { RESSOURCES, PRIMARY_RESSOURCES, RECOVERIES } from '@constants/epique'
import { multipleAll, coeffToPercent } from '@lib/helpers/math'

import CountInput from '../CountInput'
import TagsInput from 'react-tagsinput'
import RessourceIcon from '@components/RessourceIcon'

const RessourceItem = ({
  name,
  editing,
  value,
  updateItem,
  options,
  saveRessourceCurrent
}) => {
  const base = value?.base || 0
  const bonusFlat = value?.bonusFlat || 0
  const percent = value?.percent || []
  const reservation = value?.reservation || 0
  const convertion = value?.convertion || 0
  const current = value?.current || 0
  const desc = value?.desc || ''

  const max = Math.ceil((base+bonusFlat)*multipleAll(percent))

  const onChange = e => {
    const {name: fieldName, value} = e.target
    let val_ = value
    if (['fieldName'].includes(fieldName)) {
      val_ = value.trim()
    } else if(e.target.type === 'number') {
      val_ = parseInt(value)
    }
    updateItem(name, { [fieldName]: val_})
  }

  const saveCurrent = useCallback(current => {
    saveRessourceCurrent(name, current, desc)
  }, [name, saveRessourceCurrent, desc])


  const setPercent = arr => {
    const percent = arr.map(parseFloat)
    updateItem(name, { percent})
  }

  if (!editing && !base) {
    return null
  }

  return (
    <>
      <tr>
        <th className='bg-none border-none whitespace-nowrap'>
          <RessourceIcon name={name} className='mr-1'/>
          <span className='hidden md:inline'>{RESSOURCES[name]}</span>
        </th>
        <td>{editing?
          <input name='base' type='number' className='min-w-[80px]'
          value={base} onChange={onChange} />
          :
          base
        }</td>
        <td className='hidden md:table-cell'>{editing?
          <input name='bonusFlat' type='number' className='min-w-[80px]'
          value={bonusFlat} onChange={onChange} />
          :
          bonusFlat
        }</td>
        <td className='hidden md:table-cell'>{editing?
          <TagsInput
            value={percent} 
            validationRegex={/^-?\d+$/}
            inputProps={{placeholder: '%'}}
            className='react-tagsinput w-60'
            onChange={setPercent} />
          :
          coeffToPercent(percent)+'%'
        }</td>
        <td className='total'>{max}</td>
        <td>
          {name !== 'souffle' &&
          (
            editing? <input name='reservation' type='number' className='min-w-[80px]'
            value={reservation} onChange={onChange} />
            :
            reservation
          )
          }
        </td>
        <td className='hidden md:table-cell'>
          {PRIMARY_RESSOURCES.includes(name) &&
          (
            editing? <input name='convertion' type='number' className='min-w-[80px]'
            value={convertion} onChange={onChange} />
            : convertion
          )
          }
        </td>
        <td colSpan={3} className='bg-none'>
          {name !== 'souffle' && !editing && 
            <CountInput 
              value={current} 
              max={max-reservation-convertion} 
              min={0} 
              onChangeCb={saveCurrent}/>
          } 
          {max-reservation-convertion>0 && editing &&
            <b>{max-reservation-convertion}</b>
          }
        </td>
      </tr>
      <tr className={base<=0?'hidden':''}>
        <td className='bg-none'/>
        <td colSpan={8} className='text-left bg-none p-0'>
          {(desc || editing) && <textarea
            rows={2}
            name='desc'
            value={desc}
            onChange={onChange} 
            className={`w-full bg-white/50`}
          />}
        </td>
      </tr>
    </>
  )
}

export default RessourceItem