import { useCallback, useEffect, useReducer, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { RESSOURCES } from '@constants/epique'
import { useSheetCtx } from '@contexts/sheetCtx'
import { getRessourcesUsedXp } from '@lib/helpers/epique'

import SavingStatus from '@components/SavingStatus'
import RessourceItem from './RessourceItem'
import { faFloppyDisk, faPenToSquare, faXmark } from '@fortawesome/free-solid-svg-icons'



const Ressources = () => {
  const { 
    data = {}, 
    save, 
    sheet, 
    savingStatus 
  } = useSheetCtx({dataPath:'ressources'})
  const [editing, setEditing] = useState(false)
  const [value, setValue] = useState(data)

  useEffect(() => {
    setValue(data)
  }, [data, editing])

  const saveAll = () => {
    save({
      path: 'ressources', 
      value
    })
    setEditing(false)
  }

  const saveRessourceCurrent = useCallback((ressource, current, desc) => {
    save({
      path: `ressources/${ressource}`, 
      value: {
        current: current || 0,
        desc
      }
    })
  }, [save])

  const updateItem = (itemKey, val) => {
    setValue(prev => ({
      ...prev,
      [itemKey]: {...prev[itemKey], ...val}
    }))
  }

  return (
    <div className={`w-full ${editing?'col-span-2':''}`}>

      <h2 id='ressources' className='flex items-center'>
        Ressources
        <div className='badge-xp mx-2'>
          {getRessourcesUsedXp(sheet)} XP
        </div> 
        <button className='btn mr-1' onClick={saveAll}>
          <FontAwesomeIcon icon={faFloppyDisk}/>
        </button>
        {editing ?
        <button className='btn' onClick={() => setEditing(false)}>
          <FontAwesomeIcon icon={faXmark}/>
        </button>
        :
        <button className='btn' onClick={() => setEditing(true)}>
          <FontAwesomeIcon icon={faPenToSquare}/>
        </button>
        }

        <SavingStatus status={savingStatus}/>
      </h2>

      <div className='card bg-ink-500/20 overflow-x-auto'>
        <div className='background-parchment-img'/>
        {editing && <div>
          Estimation de la réservation de ressource : {sheet._stats?.mana} Mana, {sheet._stats?.volonte} Volonté, {sheet._stats?.endurance} Endurance  
        </div>}
        <table className='w-full text-base'>
          <thead>
            <tr>
              <th/>
              <th>Base</th>
              <th className='hidden md:table-cell'>-/+</th>
              <th className='hidden md:table-cell'>Mod.</th>
              <th>Total</th>
              <th>Res.</th>
              <th className='hidden md:table-cell'>Conv.</th>
              <th/>
              <th/>
              <th/>
            </tr>
          </thead>
          <tbody>
          {Object.keys(RESSOURCES).map(r => 
            <RessourceItem
              key={r}
              name={r}
              editing={editing}
              value={value[r]||{}}
              updateItem={updateItem}
              options={sheet.options}
              saveRessourceCurrent={saveRessourceCurrent}
            />
          )}
          </tbody>
        </table>
      </div>
    </div>
  )
}

export default Ressources