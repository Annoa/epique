import { useState } from 'react'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faShieldAlt } from '@fortawesome/free-solid-svg-icons'
import { useSheetCtx } from '@contexts/sheetCtx'

import StatsPanel from './StatsPanel'
import SummaryPanel from './SummaryPanel'
import PjModal from '@components/modals/PjModal'

const Sidebar = ({
  user,
  users,
  tab,
  type,
  perso
}) => {
  const { sheet } = useSheetCtx()
  const [modalPj, setModalPj] = useState()

  return <div>
    <div className='p-2 text-center'>
      {type === 'pj' && <button className='btn' 
        disabled={!user.isAdmin && user.id !== perso.userId}
        onClick={() => setModalPj(perso)}>
        <FontAwesomeIcon icon={faShieldAlt} className='mr-1'/>
        {perso.name}
      </button>}

    </div>
    <StatsPanel sheet={sheet}/>
    <SummaryPanel sheet={sheet} tab={tab}/>
    <PjModal
      pj={modalPj}
      setSelectedItem={setModalPj}
      users={users}/>
  </div>
}

export default Sidebar