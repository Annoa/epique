import { useEffect, useReducer, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { SHEET_TITLES, OPTIONS_SECTION, FIRST_TAB } from '@constants/routes'
import { 
  faBarsProgress, faBoltLightning, faBookBookmark, 
  faCoins, faFireFlameCurved, faHandHoldingMedical, 
  faHeart, faIdBadge, faPuzzlePiece, faShapes, 
  faWandSparkles, faChevronRight, faArrowUpRightDots
} from '@fortawesome/free-solid-svg-icons'
import styles from '@components/Layout/sidebar.module.css'


const SECTION_ICONS = {
  'txt-start': faIdBadge,
  'health': faHeart,
  'xp': faHandHoldingMedical,
  'ressources': faBarsProgress,
  'recoveries': faArrowUpRightDots,
  'roots': faPuzzlePiece,
  'abilities': faBookBookmark,
  'feats': faFireFlameCurved,
  'traits': faShapes,
  'capacities': faBoltLightning,
  'artefacts': faWandSparkles,
  'inventory': faCoins,
}

const SummaryItem = ({
  section,
  children
}) => (
  <div>
    {SECTION_ICONS[section] && 
      <span className='inline-block w-[1.5rem] text-center'>
        <FontAwesomeIcon icon={SECTION_ICONS[section]} className='mr-1'/>
      </span>
    }
    <a href={'#'+section}>{SHEET_TITLES[section]}</a>
    {children}
  </div>
)

const SummaryPanel = ({
  sheet,
  tab
}) => {
  const [isOpen, toggleIsOpen] = useReducer(prev => !prev, true)
  const [notesSummary, setNotesSummary] = useState([])

  useEffect(() => {
    if (tab === 'notes' && sheet?.notes?._html) {
      let parser = new DOMParser()
      const doc = parser.parseFromString(sheet.notes._html, 'text/html')
      const list = doc.querySelectorAll('h1,h2')
      setNotesSummary(Array.from(list))
    }
  }, [sheet?.notes?._html, tab])

  return (
    <div>
      <button onClick={toggleIsOpen} className={styles.panelTitle}>
        Sommaire
        <FontAwesomeIcon 
          icon={faChevronRight} 
          className={`transition-transform ml-1 ${isOpen?'rotate-90':''}`}
        />
      </button>
      <div className={`p-2 ${isOpen?'':'hidden'}`}>

        {!tab && FIRST_TAB.map(section => (
          <SummaryItem key={section} section={section}/>
        ))}

        {tab === 'capacities' && 
          sheet.capacities?.map((e,i) => (
            <div key={i}>
              <a href={'#'+e.name}>{e.name||'[sans titre]'}</a>
            </div>
          ))
        }
        
        {tab === 'equipments' && 
          <>
            {sheet.equipments?.map((e,i) => (
              <div key={i}>
                <a href={'#'+e.name}>{e.name||'[sans titre]'}</a>
              </div>
            ))}
            <SummaryItem section='inventory'/>
          </>
        }

        {tab === 'notes' && 
          notesSummary?.map((e,i) => (
            <div key={i} style={
              e.tagName==='H2'?{paddingLeft:'1.2rem', textIndent:'-.5rem'}:{}
            }>
              <a href={'#'+e.id}>{e.textContent||'[sans titre]'}</a>
            </div>
          ))
        } 

        {tab === 'options' && (
          OPTIONS_SECTION.map(({id, name}) => <div key={id}>
            <a href={'#'+id}>{name}</a>
          </div>)
        )}

      </div>
    </div>
  )
}

export default SummaryPanel