import { useReducer } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { 
  faBookBookmark, faChevronRight, 
  faDungeon, faFireFlameCurved, 
  faPuzzlePiece, faWandMagicSparkles 
} from '@fortawesome/free-solid-svg-icons'
import styles from '@components/Layout/sidebar.module.css'
import Barometer from '../Barometer'

const StatsPanel = ({
  sheet
}) => {
  const [isOpen, toggleIsOpen] = useReducer(prev => !prev, true)

  const stats = sheet._stats || {}

  if (!sheet._stats) return

  return <div>
    <button onClick={toggleIsOpen} className={styles.panelTitle}>
      Stats
      <FontAwesomeIcon 
        icon={faChevronRight} 
        className={`transition-transform ml-1 ${isOpen?'rotate-90':''}`}
      />
    </button>
    <div className={`p-2 ${isOpen?'':'hidden'}`}>

      {/* <div>
        <span className='badge-xp mr-1'>XP</span>
        utilisés : {stats.xp?.used}
      </div> */}
      {/* <div>
        <span className='badge-pc mr-1'>PC</span>
        utilisés : {stats.pc?.used+' '} 
      </div> */}
      {/* <div>
        <span className='badge-pc mr-1 opacity-0'>PC</span>
        = {Math.ceil(stats.pc?.used/10)}xp reste {}pc
      </div> */}
      {/* {stats.ps?.used>0 && <div>
        <span className='badge-souffle mr-1'>SOUFFLE</span>
        utilisés : {stats.ps?.used}
      </div>} */}
      <div>
        <span className='badge-ner mr-1'>NER</span>
        <Barometer 
          current={stats.ner?.used}
          thresholds={stats.ner?.thresholds}
        />
      </div>
      {/* <div>
        <span className='badge-xp mr-1'>Σ XP</span>
        utilisés : {stats.xp?.used + Math.ceil(stats.pc?.used/10)} 
      </div> */}
      <div>
        <FontAwesomeIcon className='mr-1' icon={faPuzzlePiece}/>
        Racines : {stats.roots?.nb}/{stats.roots?.max}
      </div>
      <div>
        <FontAwesomeIcon className='mr-1' icon={faBookBookmark}/>
        Compétences : {stats.abilities?.nb}/{stats.abilities?.max}
      </div>
      <div>
        <FontAwesomeIcon className='mr-1' icon={faFireFlameCurved}/>
        Dons : {stats.feats?.nb}/{stats.feats?.max}
      </div>
      <div>
        <FontAwesomeIcon className='mr-1' icon={faWandMagicSparkles}/>
        Équipements : {stats.equipments?.nb}/{stats.equipments?.max}
      </div>
      <div>
        <FontAwesomeIcon className='mr-1' icon={faDungeon}/>
        Archétypes : {stats.archetypes?.nb}/{stats.archetypes?.max}
      </div>
    </div>

  </div>

}

export default StatsPanel