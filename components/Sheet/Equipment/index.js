import dynamic from 'next/dynamic'
import { useCallback, useEffect, useReducer, useRef, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { EQUIPMENT_TYPES, EQUIPMENT_RESSOURCES } from '@constants/epique'
import { equipmentModel } from '@lib/data/models/sheetModel'
import useKeySave from '@hooks/useKeySave'
import { useSheetCtx } from '@contexts/sheetCtx'

import SavingStatus from '@components/SavingStatus'
import { Checkbox, NumberInput } from '@components/Fields'
import Alert from '@components/modals/Alert'
import Unkeeps from '../Capacity/Unkeeps'
import Stocks from './Stocks'
import RessourceIcon from '@components/RessourceIcon'

import { 
  faFloppyDisk, faMask, faPenToSquare, 
  faXmark, faTrashAlt, faCircleHalfStroke 
} from '@fortawesome/free-solid-svg-icons'

const TextField = dynamic(
  () => import('@components/TextField'), 
  { ssr:false }
)

const Equipment = ({
  editing,
  index,
  toggleEdit
}) => {
  const { 
    data = equipmentModel, 
    save, savingStatus, sheet
   } = useSheetCtx({dataPath: `equipments/${index}`})  
  const [isOpen, toggleOpen] = useReducer((prevIsOpen) => !prevIsOpen, false)
  const [value, setValue] = useState(data)
  const elemRef = useRef()

  useEffect(() => {
    setValue(data)
  }, [data, editing])

  useKeySave({elemRef, save, value, dataPath:`equipments/${index}`})

  const handleSave = useCallback(async () => {
    const saved = await save({
      path: `equipments/${index}`,
      value
    })
    if (saved) {
      toggleEdit()
    }
  }, [index, save, toggleEdit, value])

  const handleDelete = () => {
    const equipments = [...sheet.equipments]
    equipments.splice(index, 1)
    save({
      path: 'equipments',
      value: equipments,
      overwrite: true
    }).then(() => toggleEdit())
  }

  const onCheckboxChange = e => {
    const { name, checked } = e.target
    setValue(prev => ({
      ...prev,
      [name]: checked
    }))
  }

  const onStrChange = (key, str) => {
    setValue(prev => ({...prev, [key]: str}))
  }

  const onMaxStockChange = e => {
    const {name, value} = e.target
    let max = parseInt(value) || 0
    setValue(prev => ({
      ...prev,
      stock: {
        ...prev.stock,
        [name]: {...prev.stock[name], max}
      }
    }))
  }

  const onIntChange = e => {
    const { name, value } = e.target
    setValue(prev => ({
      ...prev,
      [name]: parseInt(value) || 0
    }))
  }

  return <div ref={elemRef}
    className={`card p-0 relative 
      ${editing?'':'cursor-pointer'} 
      ${!isOpen?'card-collapsed':''}
    `} 
    onClick={toggleOpen}
  >
    {editing?
      <>
        <div className='bg-ink-300/40 p-2'>

          <div className='flex items-center justify-between gap-1 mb-1'>
            <input 
              type='text' 
              value={value.name}
              placeholder="Nom de l'équipement"
              onChange={e => onStrChange('name', e.target.value)}
            />
            <SavingStatus status={savingStatus}/>
            <button className='btn ml-1' onClick={handleSave}>
              <FontAwesomeIcon icon={faFloppyDisk}/>
            </button>
            <button className='btn ml-1' onClick={toggleEdit}>
              <FontAwesomeIcon icon={faXmark}/>
            </button>
            <Alert
              title={`Supprimer "${data.name}" ?`}
              description="Attention : les champs en cours d'édition seront perdus."
              okTxt='Oui, supprimer'
              onOkClick={handleDelete}
              button={
                <button className='btn ml-1'>
                  <FontAwesomeIcon icon={faTrashAlt}/>
                </button>
              }
            />
          </div>

          <div className='flex items-center justify-between gap-1 mb-1'>
            <Checkbox 
              id='draft' 
              className='px-1 rounded bg-ink-300/30'
              label={<FontAwesomeIcon icon={faMask} />} 
              defaultChecked={value.draft} 
              onChange={onCheckboxChange}
            />

            <Checkbox 
              id='foci' 
              className='px-1 rounded bg-ink-300/30'
              label='Foci' 
              defaultChecked={value.isFoci} 
              onChange={onCheckboxChange}
            />

            <NumberInput
              id='ner'
              className='px-1 rounded bg-ink-300/30'
              label='NER'
              defaultValue={value.ner}
              onChange={onIntChange}
            />

            <NumberInput
              id='kg'
              className='px-1 rounded bg-ink-300/30'
              label='Kg'
              defaultValue={value.kg}
              onChange={onIntChange}
            />

            <select 
              onChange={e => onStrChange('type', e.target.value)} 
              defaultValue={value.type}>
              <option value='' label='Commun'/>
              {Object.keys(EQUIPMENT_TYPES).filter(t => t!=='foci').map(id => (
                <option value={id} key={id} label={EQUIPMENT_TYPES[id]}/>
              ))}
            </select>
          </div>

          <div className='w-full flex items-center justify-between gap-1 flex-wrap mb-1'>
            {Object.keys(EQUIPMENT_RESSOURCES).map(r => (
              <div key={r} className='nwhitespace-nowrap px-1 rounded bg-ink-300/30'>
                <label className='mr-1' htmlFor={r}>{EQUIPMENT_RESSOURCES[r]}</label>
                <input className='w-16' name={r} type='number' value={value.stock?.[r]?.max} onChange={onMaxStockChange} />
              </div>
            ))}
          </div>

          <Unkeeps 
            value={value.unkeeps}
            setValue={v => setValue(prev => ({...prev, unkeeps:v}))}/>

        </div>

        <div className='p-2'>
          <TextField
            content={value.desc}
            setContent={desc => onStrChange('desc', desc)}
          />    
        </div>
        
      </>
      :
      <>
        <h3 id={data?.name || 'Nom de l\'équipement'}>
          {data?.name || 'Nom de l\'équipement'}
          <SavingStatus status={savingStatus}/>
          <button className='btn ml-1' onClick={toggleEdit}>
            <FontAwesomeIcon icon={faPenToSquare}/>
          </button>
        </h3>

        <div className='p-2 flex gap-1 flex-wrap-reverse'>
          {(data.ner > 0 || data.draft) && 
            <div className={`badge-ner ${data.draft?'draft':''}`}>
              {data.draft && <FontAwesomeIcon icon={faMask} className='mr-1' />}
              {data.ner > 0 && `${data.ner} NER`}
            </div>
          }

          {data.type && <div className='badge bg-yellow-500'>
            { EQUIPMENT_TYPES[data.type] }
          </div>}

          {data.isFoci && <div className='badge bg-red-400'>Foci</div>} 

          {data.kg > 0 && <div className='badge'>{ data.kg }kg</div>}

          {data.unkeeps.map((e,i) => <div className='badge bg-indigo-400' key={i}>
            {e.operator}{e.nb}
            <RessourceIcon name={e.ressource} className='mx-1' fallback/>
            / {e.interval}
            {!!e.condition && 
              <FontAwesomeIcon icon={faCircleHalfStroke} className='ml-1'/>
            }
          </div>)}

        </div>

        <Stocks
          value={data.stock}
          index={index} 
          save={save}/>

        <div 
          className='inner-html-container p-2'
          dangerouslySetInnerHTML={{ __html: data._html }}
        />
      </>
    }
    
  </div>
}

export default Equipment