import CountInput from '@components/Sheet/CountInput'
import { EQUIPMENT_RESSOURCES } from '@constants/epique'
import { useCallback } from 'react'

const Stocks = ({
  value,
  save,
  index
}) => {
  const sum = Object.values(value).reduce((a,b) => a+b.max, 0)

  const handleSave = useCallback(async (current, ressource) => {
    await save({
      path: `equipments/${index}/stock/${ressource}/current`,
      value: current
    })
  }, [index, save])

  if (!sum) return null

  return <div className={`bg-gray-300 py-1 px-2 mx-[-3px] 
    shadow-lg text-gray-700 flex justify-evenly flex-wrap mb-2`}>
    {Object.keys(EQUIPMENT_RESSOURCES).map(r => {
      const current = value?.[r]?.current
      const max = value?.[r]?.max
      return (value?.[r]?.max>0 && 
      <div key={r} className='whitespace-nowrap text-center'>
        <span className='' htmlFor={r}>
          {EQUIPMENT_RESSOURCES[r]}
        </span>
        <CountInput
          name={r}
          value={current} 
          max={max} 
          min={0} 
          onChangeCb={handleSave}/>
      </div>
    )})}
  </div>
}

export default Stocks