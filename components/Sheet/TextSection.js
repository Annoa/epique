import dynamic from 'next/dynamic'
import { useEffect, useReducer, useState, useCallback, useRef } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { SHEET_TITLES } from '@constants/routes'
import { onParsedDivUpdate } from '@lib/helpers/parsers'
import { faFloppyDisk, faPenToSquare, faWeightHanging, faXmark } from '@fortawesome/free-solid-svg-icons'
import { useSheetCtx } from '@contexts/sheetCtx'
import useKeySave from '@hooks/useKeySave'

import SavingStatus from '@components/SavingStatus'

const TextField = dynamic(
  () => import('@components/TextField'), 
  { ssr:false }
)

const TextSection = ({
  parser,
  dataPath,
  className
}) => {
  const { data, save, savingStatus } = useSheetCtx({dataPath})
  const [editing, toggleEditig] = useReducer(prev => !prev, false)
  const [value, setValue] = useState(data)
  const elemRef = useRef()

  useEffect(() => {
    const { _html, ...d } = data || {}
    setValue(d)
  }, [data, editing])

  useKeySave({elemRef, save, value, dataPath, close:toggleEditig})

  const handleSave = useCallback(e => {
    save({path:dataPath, value})
    toggleEditig()
  }, [dataPath, save, value])

  const onDivUpdate = useCallback(node => {
    if (!parser) return
    onParsedDivUpdate({
      parser,
      node,
      setValue
    })
  }, [parser])

  return (
    <div className={className} ref={elemRef}>
      <h2 id={dataPath} className={`flex items-center ${
        dataPath==='notes'?'bg-ink-200 p-2 top-0 sticky z-[1]':''
      }`}>
        {/* {dataPath==='notes'&&<div className='background-parchment-img'/>} */}
        {SHEET_TITLES[dataPath]}
        {data?.xp > 0 &&
          <div className='badge-xp ml-1'>{data?.xp} XP</div>
        }
        {data?.ner > 0 &&
          <div className='badge-ner ml-1'>{data?.ner} NER</div>
        }
        {data?.kg > 0 &&
          <div className='badge-kg ml-1'>
            {data?.kg}
            <FontAwesomeIcon className='ml-1' icon={faWeightHanging} size='sm' />
          </div>
        }
        {editing ?
          <>
            <button className='btn ml-1' onClick={handleSave}>
              <FontAwesomeIcon icon={faFloppyDisk} size='lg'/>
            </button>
            <button className='btn ml-1' onClick={toggleEditig}>
              <FontAwesomeIcon icon={faXmark} size='lg'/>
            </button>
          </>
        :
          <button className='btn ml-1' onClick={toggleEditig}>
            <FontAwesomeIcon icon={faPenToSquare}/>
          </button>
        }
        <SavingStatus status={savingStatus}/>
      </h2>

      <div className='card p-2'>
        { editing ?
          <TextField
            content={value.desc}
            setContent={desc => setValue(prev => ({...prev, desc}))}
            // onKeyDown={handleKeySave}
            parser={parser}
            // sectionKey={sectionKey}
            onParsedDivUpdate={onDivUpdate}
          />
        :
          <div 
            className={`inner-html-container ${dataPath==='abilities'? 'md:columns-2':''}`}
            dangerouslySetInnerHTML={{ __html: data?._html }}
          />
        }
      </div>

      
    </div>
  )
}

export default TextSection