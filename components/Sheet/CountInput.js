import { useCallback, useEffect, useRef, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFloppyDisk, faMinus, faPlus } from '@fortawesome/free-solid-svg-icons'

import useLongPress from '@hooks/useLongPress'

const CountInput = ({
  value,
  max,
  min,
  onChangeCb,
  name
}) => {
  const [displayedValue, setDisplayedValue] = useState(value) 
  const [editing, setEditing] = useState(false)
  const timerId = useRef()

  useEffect(() => {
    setDisplayedValue(value)
  }, [value])

  const lgPressDec = useLongPress()
  const lgPressInc = useLongPress()

  const debouncedHandler = useCallback(newValue =>{
    clearTimeout(timerId.current)
    timerId.current = setTimeout(() => {
      onChangeCb(newValue, name)
    }, 2000)
    },
    [name, onChangeCb]
  )

  const saveAndClose = () => {
    onChangeCb(displayedValue, name)
    setEditing(false)
  }

  const onInputKeyDown = e => {
    if (e.key === 'Enter') {
      onInputChange(e)
      saveAndClose()
    } else if (e.key === 'Escape') {
      setEditing(false)
    }
  }

  useEffect(() => {
    if (lgPressDec.action && lgPressDec.event) {
      let delta =  1
      if (lgPressDec.event.shiftKey || lgPressDec.action === 'longpress') {
        delta = 5
      }
      setDisplayedValue(prev => {
        const newVal = Math.max(min??prev-delta, prev-delta)
        debouncedHandler(newVal)
        return newVal
      })
    }
  }, [debouncedHandler, lgPressDec.action, lgPressDec.event, min])

  useEffect(() => {
    if (lgPressInc.action && lgPressInc.event) {
      let delta =  1
      if (lgPressInc.event.shiftKey || lgPressInc.action === 'longpress') {
        delta = 5
      }
      setDisplayedValue(prev => {
        const newVal = Math.min(max??prev+delta, prev+delta)
        debouncedHandler(newVal)
        return newVal
      })
    }
  }, [debouncedHandler, lgPressInc.action, lgPressInc.event, max])


  const onInputChange = e => {
    let val = parseInt(e.target.value||0)
    val = Math.min(max??val, val)
    val = Math.max(min??val, val)
    setDisplayedValue(val)
  }

  return <div className='flex'>
    { editing ?
      <input type='number' className={`w-[94px] noArrows`}
        value={displayedValue} 
        onChange={onInputChange}
        min={min} max={max}
        onKeyDown={onInputKeyDown}
        autoFocus
      />
    :
    <>
      <button className='btn' disabled={displayedValue < min} {...lgPressDec.handlers}>
        <FontAwesomeIcon icon={faMinus} />
      </button>
      <div onClick={()=> setEditing(true)}
        className='font-mono bg-parchment-100 px-2 w-16 text-center cursor-pointer'>
        {displayedValue}
      </div>
      <button className='btn' disabled={displayedValue >= max} {...lgPressInc.handlers}>
        <FontAwesomeIcon icon={faPlus} />
      </button>
    </>
    }
    { editing &&
      <button className='btn' onClick={saveAndClose}>
        <FontAwesomeIcon icon={faFloppyDisk} />
      </button>
    }
    <div className='grow font-mono'>/{max}</div>
  </div>
}

export default CountInput