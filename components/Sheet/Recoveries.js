import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { useSheetCtx } from '@contexts/sheetCtx'

import SavingStatus from '@components/SavingStatus'
import { faClock, faCircleQuestion, faCircleHalfStroke } from '@fortawesome/free-solid-svg-icons'
import { RESSOURCES } from '@constants/epique'
import RessourceIcon from '@components/RessourceIcon'
import { Popover, PopoverContent, PopoverTrigger } from '@components/Popover'


const Recovery = ({
  ressource,
  recovery
}) => {
  const { total, src } = recovery || {}
  return <div>
    {total>=0? '+':''}{total}
    <RessourceIcon name={ressource} className='ml-1'/>
    <Popover>
      <PopoverTrigger>
        <FontAwesomeIcon icon={faCircleQuestion} className='ml-1 text-ink-500'/>
      </PopoverTrigger>
      <PopoverContent>
        {src.map((e,i) => {
        const {operator, nb, name, ressource} = e
        return <div key={i}>
          {operator}{nb} {RESSOURCES[ressource]} ({name})
        </div>})}
      </PopoverContent>
    </Popover>
  </div>
}

const Recoveries = () => {
  const { 
    save, 
    sheet, 
    savingStatus 
  } = useSheetCtx({dataPath:'ressources'})

  const recoveries = sheet._stats?.recoveries || {}

  return (
    <div className='w-full'>

      <h2 id='recoveries' className='flex items-center'>
        Récupérations & entretiens
        <button className='btn ml-2 opacity-0'>
          <FontAwesomeIcon icon={faClock}/>
        </button>
        <SavingStatus status={savingStatus}/>
      </h2>

      <div className='card bg-ink-500/20 p-2'>
        <div className='background-parchment-img'/>

        <div className='grid grid-cols-3 gap-2'>
          {Object.keys(recoveries).map(interval => <div 
            key={interval} 
            className='text-center bg-white/20'>
            <h3 className='block m-0'>{interval}</h3>
            <div>
              {Object.keys(recoveries[interval].always).map(ressource => <Recovery 
                key={ressource}
                ressource={ressource}
                recovery={recoveries[interval].always[ressource]}/>)}
            </div>

            {recoveries[interval].conditionals.length>0 && <div className=''>
              {recoveries[interval].conditionals.map((recovery, i) => <div 
                key={i} 
                className='inline-block mx-1 px-1 my-1 bg-white/30 rounded-md'>
              {/* <input type='checkbox' disabled/> {recovery.operator}{recovery.nb} */}
              {recovery.operator}{recovery.nb}
              <RessourceIcon name={recovery.ressource} className='ml-1'/>
              <Popover>
                <PopoverTrigger>
                  <FontAwesomeIcon icon={faCircleHalfStroke} className='ml-1 text-ink-500'/>
                </PopoverTrigger>
                <PopoverContent>
                  {recovery.condition} ({recovery.name})
                </PopoverContent>
              </Popover>
              
              </div>)}
            </div>}

          </div>)}
        </div>
      </div>
    </div>
  )
}

export default Recoveries