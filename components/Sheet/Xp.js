import { useEffect, useReducer, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { useSheetCtx } from '@contexts/sheetCtx'

import { 
  faAngleDoubleUp, faFloppyDisk,
  faPenToSquare, faTurnUp, faXmark 
} from '@fortawesome/free-solid-svg-icons'
import { xpRecyclingBySource } from '@constants/epique'
import SavingStatus from '@components/SavingStatus'

const Level = ({level}) => {
  if (!level) return ''
  return <>
    <div className='hidden md:inline-block'>
      {
        level==='P'? '(Primaire)'
        : level==='S'? '(Secondaire)'
        : '(Tertiaire)'
      }
    </div>
    <div className='inline-block md:hidden'>
      ({level})
    </div>
  </>
}

const XpItem = ({
  name,
  id,
  value = {},
  setValue,
  arrow
}) => {
  const {current, max, level} = value

  const onChange = e => {
    const {name: fieldName, value} = e.target
    setValue(prev => ({
      ...prev,
      [id]: {...prev[id], [fieldName]: parseInt(value)}
    }))
  }

  const updateBoth = () => {
    const inc = id === 'pc'? 10 : 1
    setValue(prev => ({
      ...prev,
      [id]: {
        ...prev[id], 
        current: prev[id].current + inc,
        max: prev[id].max + inc
      }
    }))
  }
  
  return <div className='flex'>
    <div className='grow shrink truncate'>
      {arrow && <FontAwesomeIcon icon={faTurnUp} className='rotate-90 mx-2'/> }
      {name} <Level level={level}/>
    </div>
    <div className='font-normal whitespace-nowrap shrink-0'>
      <input type='number' name='current' value={current||0} className='w-[4rem]' onChange={onChange}/>
       / 
      <input type='number' name='max' value={max||0} className='w-[4rem]' onChange={onChange}/>
      <button className={`btn ml-1 ${id==='pc'?'btn-ink':''}`} onClick={updateBoth}>
        <FontAwesomeIcon icon={faAngleDoubleUp}/>
      </button>
    </div>
  </div>
}

const EditingXpItem = ({
  name,
  id,
  value,
  setValue
}) => {
  const onCheckChange = e => {
    setValue(prev => {
      if (e.target.checked) {
        return {
          ...prev,
          [id]: {current:0, max: 0, level:'T'}
        }
      } else {
        const newValue = {...prev}
        delete newValue[id]
        return newValue
      }
    })
  }

  const onSelectChange = e => {
    setValue(prev => ({
      ...prev,
      [id]: {...prev[id], level: e.target.value}
    }))
  }
 
  return <div className='flex even:bg-ink-500/10 gap-x-2 px-1'>
    <div className={`grow ${value?'font-semibold':''}`}>
      {name}
      {value && <span className='ml-1'>
        ({value.max})
      </span>}
    </div>
    <div>
      {value && <select className='w-auto'
        onChange={onSelectChange} value={value?.level||'T'}>
        <option value='P'>Primaire</option>
        <option value='S'>Secondaire</option>
        <option value='T'>Tertiaire</option>
      </select>}
    </div>
    <div>
      <input 
        type='checkbox' 
        checked={!!value} 
        disabled={value?.max>0}
        onChange={onCheckChange} />
    </div>
  </div>
}


const Xp = ({
  xpList
}) => {
  const { 
    save, 
    data, 
    savingStatus,
    sheet
  } = useSheetCtx({dataPath:'xp2'})
  const [editing, toggleEditig] = useReducer(prev => !prev, false)
  const [value, setValue] = useState({})
  const stats = sheet._stats || {}

  useEffect(() => {
    if (data) {
      setValue(data)
    }
  }, [data, editing])

  const saveXp = () => {
    save({
      path: `xp2`, 
      value,
      overwrite: editing
    })
    if (editing) {
      toggleEditig()
    }
  }

  let total = 0
  let xpSource1 = [], xpOther = [], xpSource2 = []
  Object.keys(data||{}).forEach(id => {
    if (id === 'note') return
    if (id !== 'pc') {
      total += data[id].max
    }
    if (id === 'generique' || id === 'pc') return
    if (xpRecyclingBySource[1].includes(id)) {
      xpSource1.push(id)
    } else if (xpRecyclingBySource[2].includes(id)) {
      xpSource2.push(id)
    } else {
      xpOther.push(id)
    }
  })

  const used = [
    `Xp utilisés: ${stats.xp?.used || 0}`,
    `Pc utilisés: ${stats.pc?.used || 0}`,
  ]
  if (stats.ps?.used>0) {
    used.push(`Ps utilisés: ${stats.ps?.used}`)
  }

  return (
    <div className='w-full'>
      <h2 id='xp' className='flex items-center'>
        Xp
        <div className='badge-xp ml-1'>Σ {total} XP</div>
        <button className='btn mx-1' onClick={saveXp}>
          <FontAwesomeIcon icon={faFloppyDisk}/>
        </button>
        {editing ?
          <button className='btn' onClick={toggleEditig}>
            <FontAwesomeIcon icon={faXmark}/>
          </button>
        :
        <button className='btn mx-1' onClick={toggleEditig}>
          <FontAwesomeIcon icon={faPenToSquare}/>
        </button>
        }
        <SavingStatus status={savingStatus}/>
      </h2>

      <div className='card bg-ink-500/20'>
        <div className='background-parchment-img'/>
          {editing ?
            <div>
              {Object.keys(xpList).map(id => (
                <EditingXpItem key={id} {...xpList[id]} value={value[id]} setValue={setValue}/>
              ))}
            </div>
            :
            <>
              <div className='card mb-0 z-10'>
                <XpItem setValue={setValue} id='generique' name='Générique' value={value.generique} />

                {xpSource1.map(id => (
                  <XpItem setValue={setValue} key={id} {...xpList[id]} value={value[id]} arrow/>
                ))}

                {xpOther.map(id => (
                  <XpItem setValue={setValue} key={id} {...xpList[id]} value={value[id]}/>
                ))}
              </div>

              {xpSource2?.length > 0 && <div className='card my-0 p-0'>
                <div className='bg-ink-500/10 p-[2px]'>
                {xpSource2.map(id => (
                  <XpItem setValue={setValue} key={id} {...xpList[id]} value={value[id]} arrow/>
                ))}
                </div>
              </div>
              }
            </>
          }
          <div className='card shadow-none bg-transparent my-0 p-0'>
            {!editing && <div className='bg-ink-500/10 p-[2px]'>
              <XpItem setValue={setValue} id='pc' name='Pc' value={value.pc} />
            </div>}
            {(value?.note || editing) && <textarea
              rows={2}
              value={value.note}
              onChange={e => setValue(prev => ({
                ...prev,
                note: e.target.value
              }))}
              className={`w-full bg-white/50`}
            />}
            <div className='italic text-sm'>
              {used.join(', ')}
            </div>
          </div>
      </div>
    </div>
  )
}

export default Xp