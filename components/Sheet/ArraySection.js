import { useState } from 'react'
import { SHEET_TITLES } from '@constants/routes'
import { useSheetCtx } from '@contexts/sheetCtx'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCirclePlus, faSort } from '@fortawesome/free-solid-svg-icons'

import SavingStatus from '@components/SavingStatus'
import ReorderModal from '@components/modals/ReorderModal'
import { capacityModel, equipmentModel } from '@lib/data/models/sheetModel'
import TextSection from './TextSection'
import RessourcesConsole from './Ressources/RessourcesConsole'

const ArraySection = ({
  dataPath,
  renderItem
}) =>  {
  const { data = [], save, savingStatus } = useSheetCtx({dataPath})
  const [editingItems, setEditingItems] = useState([])

  const toggleEdit = i => 
    setEditingItems(prev => {
      const isOpen = prev.includes(i)
      if (isOpen) {
        return prev.filter(j => j!==i)
      } else {
        return [...prev, i]
      }
    })

  const handleAddItem = () => {
    save({
      path: dataPath, 
      value: dataPath === 'capacities'?
        capacityModel
        : dataPath === 'equipments' ?
        equipmentModel
        : {},
      push: true
    })
    // setEditingItems(prev => [...prev, data.length])
  }

  return <div id={dataPath}>
    <h2 className='flex items-center'>
      {SHEET_TITLES[dataPath]}
      {/* todo badge computedValue */}
      <ReorderModal
        dataPath={dataPath}
        data={data}
        save={save}
        button={
          <button 
            disabled={editingItems.length} 
            className='btn ml-1'>
            <FontAwesomeIcon icon={faSort} />
          </button>
        }
      />

      <SavingStatus status={savingStatus}/>
    </h2>

    <div className='cards-container'>
      {data.map((item, i) => (
        renderItem({
          key: i,
          editing: editingItems.includes(i),
          index: i, 
          toggleEdit: () => toggleEdit(i),
        })
      ))}
      <div className='flex items-center justify-center'>
        <button onClick={handleAddItem} className='btn btn-gray text-base py-4 px-8 m-2'>
          <FontAwesomeIcon icon={faCirclePlus} className='mr-1'/>
          Ajouter
        </button>
      </div>
    </div>

    {dataPath === 'equipments' && 
      <TextSection dataPath='inventory' parser='inventory'/>
    }

    <RessourcesConsole/>
  </div>

}

export default ArraySection