import React from 'react'
import * as Dialog from '@radix-ui/react-dialog'
import styles from './modal.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faXmark } from '@fortawesome/free-solid-svg-icons'
import { twMerge } from 'tailwind-merge'

const noop = () => {}

const Modal = ({
  button,
  title,
  description,
  onOkClick,
  okTxt,
  children,
  form,
  open,
  onOpenChange,
  style = {},
  className = {}
}) => {
  const okHandler = onOkClick || (() => {})

  return <Dialog.Root open={open} onOpenChange={onOpenChange}>
    {button && <Dialog.Trigger asChild>
      { button }
    </Dialog.Trigger>}
    <Dialog.Portal>
      <Dialog.Overlay className={styles.DialogOverlay} />
      <Dialog.Content className={styles.DialogContent}>

        {title && <Dialog.Title className={styles.DialogTitle}>
          {title}
        </Dialog.Title>
        }

        {description && 
          <Dialog.Description className={styles.DialogDescription}>
            {description}
          </Dialog.Description>
        }

        <div 
          className={twMerge(styles.DialogBody, className.body)} 
          style={style.body}>
          {children}

          {okTxt && <div className={styles.DialogFooter}>
            { onOpenChange ?
            <button type='submit' form={form}
              className='btn btn-emerald' 
              onClick={okHandler || noop}>
              {okTxt}
            </button>
            :
            <Dialog.Close asChild>
              <button type='submit' form={form}
                className='btn btn-emerald' autoFocus
                onClick={okHandler || noop}>
                {okTxt}
              </button>
            </Dialog.Close>
            }
          </div>
          }

        </div>

        <Dialog.Close asChild>
          <button type='button' autoFocus
            className={styles.IconButton} 
            aria-label='Close'>
            <FontAwesomeIcon icon={faXmark}/>
          </button>
        </Dialog.Close>
      </Dialog.Content>
    </Dialog.Portal>
  </Dialog.Root>
}

export default Modal
