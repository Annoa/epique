import { Fragment, useEffect, useState } from 'react'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowDown, faArrowUp, faFloppyDisk } from '@fortawesome/free-solid-svg-icons'

import Modal from './Modal'
import { SHEET_TITLES } from '@constants/routes'

// const TITLE = {
//   'capacities': 'Ordre des'
// }

const ReorderModal = ({
  dataPath,
  button,
  data,
  save
}) => {
  const [value, setValue] = useState([])

  useEffect(() => {
    setValue(data)
  }, [data])

  const onOkClick = () => {
    save({
      path: dataPath, 
      value,
      overwrite:true
    })
  }

  const up = i => {
    if (i < 1) return
    let newValue = [...value]
    let tmp = newValue[i-1]
    newValue[i-1] = newValue[i]
    newValue[i] = tmp
    setValue(newValue)
  }

  const down = i => {
    if (i >= value.length-1) return
    let newValue = [...value]
    let tmp = newValue[i+1]
    newValue[i+1] = newValue[i]
    newValue[i] = tmp
    setValue(newValue)
  }
  
  return <Modal
    button={button}
    title={SHEET_TITLES[dataPath]}
    description="Changer l'ordre d'affichage"
    okTxt={<><FontAwesomeIcon icon={faFloppyDisk}/> Sauvegarder</>}
    onOkClick={onOkClick}
  >

    <div>
      {value.map((v,i) => (
        <div className='flex even:bg-parchment-50/70' key={i}>
          <div className='mr-1 grow'>{v.name}</div>
          <div>
            <button 
              className={`btn mr-1 ${i>=value.length-1?'invisible':''} `}
              onClick={() => down(i)}
            >
              <FontAwesomeIcon icon={faArrowDown}/>
            </button>
            <button 
              className={`btn ${i<=0?'invisible':''} `}
              onClick={() => up(i)}
            >
              <FontAwesomeIcon icon={faArrowUp} />
            </button>
          </div>
        </div>
      ))}
    </div>

  </Modal>
}

export default ReorderModal
