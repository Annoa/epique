import { Fragment, useRef, useState } from 'react'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFloppyDisk, faPencil, faPlus, faTrashAlt, faXmark } from '@fortawesome/free-solid-svg-icons'

import Modal from './Modal'
import { checkStatus } from '@lib/helpers/fetch'
import { input2timestamp, timestamp2display, timestamp2input } from '@lib/helpers/date'



const JournalModal = ({
  button,
  group,
  entries,
  setEntries
}) => {
  const [editingEntryId, setEditingEntryId] = useState()
  const inputsRef = useRef({})
  const newInputsRef = useRef({})
  const url = `/api/journal/${group.id}`

  const save = (method, id) => {
    const currRef = (method === 'POST' ? newInputsRef : inputsRef).current
    const body = {
      id,
      title: currRef.title?.value,
      timestamp: input2timestamp(currRef.date?.value)
    }
    fetch(url, {
      method,
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body)
    })
    .then(checkStatus)
    .then(res => {
      setEditingEntryId()
      setEntries(res)
    })
    .catch(err => console.log('error', err))
  }

  
  return <Modal
    button={button}
    title={'Entrées du journal '+group.name}
  >
    <div>
      
      <div className='grid grid-cols-[min-content_1fr_min-content] mt-4 mb-2 gap-1'>
        {entries.map(({id, timestamp, title}) => <Fragment key={id}>
          { editingEntryId === id ?
          <>
            <input className='shrink-0' type='date' 
              ref={node => inputsRef.current.date = node}
              name='date' defaultValue={timestamp2input(timestamp)}/>

            <input className='mx-1' type='text' autoComplete='off'
              name='title' defaultValue={title} 
              ref={node => inputsRef.current.title = node}/>

            <div className='whitespace-nowrap mx-1'>
              <button className='inline-btn' onClick={() => save('PUT', id)}>
                <FontAwesomeIcon size='lg' icon={faFloppyDisk}/>
              </button>

              <button className='inline-btn' onClick={() => setEditingEntryId()}>
                <FontAwesomeIcon size='lg' icon={faXmark}/>
              </button>
            </div>
          </>
          :
          <>
            <div className='whitespace-nowrap'>{ timestamp2display(timestamp) }</div>
            <div className='truncate'>{title}</div>

            <div className='whitespace-nowrap'>
              <button 
                className={`inline-btn ${editingEntryId?'invisible pointer-events-none':''}`}
                onClick={() => setEditingEntryId(id)}>
                <FontAwesomeIcon icon={faPencil}/>
              </button>
              <button 
                className={`inline-btn ${editingEntryId?'invisible pointer-events-none':''}`}
                onClick={() => save('DELETE', id)}>
                <FontAwesomeIcon icon={faTrashAlt}/>
              </button>
            </div>

          </>
          }        
        </Fragment>
        )}

        <input className='shrink-0' type='date' 
          ref={node => newInputsRef.current.date = node}
          name='date' defaultValue={timestamp2input(Date.now())}/>

        <input className='mx-1' type='text' autoComplete='off'
          name='title' defaultValue='Titre' 
          ref={node => newInputsRef.current.title = node}/>

        <button className='btn ml-1' onClick={() => save('POST')}
          disabled={editingEntryId} >
          <FontAwesomeIcon icon={faPlus}/>
        </button>
      </div>

    </div>
  </Modal>
}

export default JournalModal
