import { useRouter } from 'next/router'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFloppyDisk } from '@fortawesome/free-solid-svg-icons'

import Modal from './Modal'
import { checkStatus } from '@lib/helpers/fetch'

const GroupModal = ({
  group = {},
  isNew,
  setSelectedItem
}) => {
  const router = useRouter()

  const onSubmit = e => {
    e.preventDefault()
    const form = e.target
    const formData = new FormData(form)
    const body = Object.fromEntries(formData)
    const url = `/api/group/${isNew ? '' : group.id}`
    console.log('body', url, body, e.target)
    fetch(url, {
      method: isNew? 'POST':'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body)
    })
    .then(checkStatus)
    .then(() => {
      router.replace(router.asPath)
      setSelectedItem({})
    })
    .catch(err => console.log('error', err))
  }

  const onOpenChange = isOpen => {
    if (!isOpen) setSelectedItem({})
  }
  
  return <Modal
    title={isNew? 'Nouveau groupe' : group.name}
    form='group-info-form'
    open={group.id || isNew}
    onOpenChange={onOpenChange}
    okTxt={<><FontAwesomeIcon icon={faFloppyDisk}/> Sauvegarder</>}
  >
    <form onSubmit={onSubmit} id='group-info-form' className='mb-4'>
      <div className='grid grid-cols-2 mt-4 gap-1'>
        <label htmlFor='name'>id*</label>
        <input autoComplete='off' disabled={!isNew}
          id='id' name='id' placeholder='Id du groupe'
          type='text' defaultValue={group?.id}/>
        <label htmlFor='name'>name*</label>
        <input autoComplete='off'
          id='name' name='name' placeholder='Nom du groupe'
          type='text' defaultValue={group?.name}/>
      </div>
    </form>
  </Modal>
}

export default GroupModal
