import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFloppyDisk } from '@fortawesome/free-solid-svg-icons'

import Modal from './Modal'
import { checkStatus } from '@lib/helpers/fetch'

const PjModal = ({
  isAdmin,
  pj = {},
  users,
  isNew,
  setSelectedItem
}) => {
  const router = useRouter()
  const [userId, setUserId] = useState()

  useEffect(() => {
    setUserId(pj?.userId||'')
  }, [pj?.userId])

  const onSubmit = e => {
    e.preventDefault()
    const form = e.target
    const formData = new FormData(form)
    const body = Object.fromEntries(formData)
    body.allowedUsers = []
    for (let key in body) {
      if (key.startsWith('user-')) {
        const uid = key.split('user-')[1]
        // if (!u.isAdmin && u.id === pj.userId)
        body.allowedUsers.push(uid)
        delete body[key]
      }
    }
    // console.log('body', body)
    const url = `/api/pj/${isNew ? '' : pj.id}`
    fetch(url, {
      method: isNew? 'POST':'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body)
    })
    .then(checkStatus)
    .then(() => {
      router.replace(router.asPath)
      setSelectedItem({})
    })
    .catch(err => console.log('error', err))
  }
  
  const onOpenChange = isOpen => {
    if (!isOpen) setSelectedItem({})
  }

  return <Modal
    title={pj.name}
    form='sheet-info-form'
    open={pj.id || isNew}
    onOpenChange={onOpenChange}
    okTxt={<><FontAwesomeIcon icon={faFloppyDisk}/> Sauvegarder</>}
  >
    <form onSubmit={onSubmit} id='sheet-info-form'>
      <div className='grid grid-cols-2 mt-4 gap-1'>

        {isNew && <>
          <label htmlFor='id'>id*</label>
          <input id='id' name='id' type='text' required
            autoComplete='off' defaultValue={pj.id} />
        </>}

        <label htmlFor='shortName'>Appélation*</label>
        <input id='shortName' name='shortName' required autoComplete='off'
          type='text' defaultValue={pj.shortName}/>
        
        <label htmlFor='name'>Nom complet*</label>
        <input id='name' name='name' required autoComplete='off'
          type='text' defaultValue={pj.name}/>
        
        {isAdmin && <>
          <label htmlFor='userId'>userId</label>
          <select id='userId' name='userId' value={userId} onChange={e => setUserId(e.target.value)}>
            <option value=''></option>
            {users.map(u => <option key={u.id} value={u.id}>
              {u.id}
            </option>)}
          </select>
        </>}
      </div>
      
      <div className='text-gray-600 mt-4'>
        Qui a droit de lire et modifier la fiche ?
      </div>

      <div className='columns-2 mb-4'>
        {users?.map(u => <div key={u.id}>
          <input id={u.id} 
            key={userId}
            name={'user-'+u.id} 
            className='mr-1'
            type='checkbox' 
            defaultChecked={
              pj.allowedUsers?.includes(u.id) || u.isAdmin || u.id === userId
            }
            disabled={u.isAdmin || u.id === userId}
            />
          <label htmlFor={u.id}>{u.pseudo}</label>
        </div>)}
      </div>
    </form>
  </Modal>
}

export default PjModal
