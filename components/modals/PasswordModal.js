import CryptoJS from 'crypto-js'
import sha256 from 'crypto-js/sha256'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEye, faEyeSlash, faFloppyDisk } from '@fortawesome/free-solid-svg-icons'

import Modal from './Modal'
import { checkStatus } from '@lib/helpers/fetch'
import { useReducer, useState } from 'react'

const PasswordModal = ({
  user = {},
  setSelectedItem
}) => {
  const [password, setPassword] = useState('')
  const [showPwd, toggleShowPwd] = useReducer(prev => !prev, false)

  const onSubmit = e => {
    e.preventDefault()
    const str = sha256(password).toString(CryptoJS.enc.Base64)
    fetch(`/api/user/${user.id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({password:str})
    })
    .then(checkStatus)
    .then(res => {
      console.log('res', res)
      // router.replace(router.asPath)
      setSelectedItem({})
    })
    .catch(err => console.log('error', err))
  }

  const onOpenChange = isOpen => {
    if (!isOpen) setSelectedItem({})
  }
  
  return <Modal
    title={user.pseudo}
    open={user.id}
    form='pwd-form'
    onOpenChange={onOpenChange}
    okTxt={<><FontAwesomeIcon icon={faFloppyDisk}/> Sauvegarder</>}
  >
    <form id='pwd-form' onSubmit={onSubmit} className='my-4'>
      <div>
        <label htmlFor='password'>
          Nouveau mot de passe
        </label>
        <div className='flex'>
          <input className='mr-1' 
            value={password} 
            name='password'
            type={showPwd?'text':'password'} 
            autoComplete='off'
            required
            onChange={e => setPassword(e.target.value)}/>
            <button className='inline-btn' type='button' onClick={toggleShowPwd}>
              <FontAwesomeIcon icon={showPwd?faEyeSlash:faEye} />
            </button>
        </div>
      </div>
    </form>
  </Modal>
}

export default PasswordModal
