import * as AlertDialog from '@radix-ui/react-alert-dialog'
import { twMerge } from 'tailwind-merge'
import styles from './modal.module.css'

const Alert = ({
  button,
  title,
  description,
  onOkClick,
  okTxt,
  open,
  onOpenChange,
  children
}) => (
  <AlertDialog.Root open={open} onOpenChange={onOpenChange}>
    <AlertDialog.Trigger asChild>
      { button }
    </AlertDialog.Trigger>

    <AlertDialog.Portal>
      <AlertDialog.Overlay 
        className={twMerge(styles.DialogOverlay, styles.AlertOverlay)} />
      <AlertDialog.Content 
        className={twMerge(styles.DialogContent, styles.AlertContent)}>

        <AlertDialog.Title className={styles.DialogTitle}>
          { title }
        </AlertDialog.Title>
        
        <div className='px-6 pb-6'>
          <AlertDialog.Description className={styles.DialogDescription}>
            { description }
          </AlertDialog.Description>

          { children }

          <div className='flex justify-end'>
            <AlertDialog.Cancel asChild>
              <button className='btn btn-gray mr-4'>
                Annuler
              </button>
            </AlertDialog.Cancel>
            <AlertDialog.Action asChild>
              <button className='btn btn-red' onClick={onOkClick}>
                {okTxt}
              </button>
            </AlertDialog.Action>
          </div>
        </div>

      </AlertDialog.Content>
    </AlertDialog.Portal>

  </AlertDialog.Root>
)

export default Alert
