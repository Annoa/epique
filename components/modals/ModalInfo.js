import Modal from './Modal'
import { useEffect, useState } from 'react'


const ModalInfo = ({
  info,
  setInfo,
  pnjs = []
}) => {
  const [title, setTitle] = useState()
  const [content, setContent] = useState()
  
  useEffect(() => {
    if (info?.pnj) {
      const pnj = pnjs.find(p => p.id === info.pnj)
      if (pnj) {
        setTitle(pnj.name)
        const desc = Object.values(pnj?.groupDesc)[0]
        setContent(desc)
      }
    }
  }, [info, pnjs])

  // const onOpenChange = open => {
  //   if (!open) {
  //     setTitle()
  //     setContent()
  //     setInfo()
  //   }
  // }

  return <Modal
    title={title}
    open={info && content}
    onOpenChange={setInfo}
  >
    {content}
  </Modal>
}

export default ModalInfo
