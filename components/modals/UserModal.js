import { useRouter } from 'next/router'
import { useState } from 'react'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFloppyDisk } from '@fortawesome/free-solid-svg-icons'
import { checkStatus } from '@lib/helpers/fetch'

import Modal from './Modal'

const UserModal = ({
  user = {},
  groups,
  isNew,
  setSelectedItem
}) => {
  const router = useRouter()
  const [open, setOpen] = useState(false)

  const onSubmit = e => {
    e.preventDefault()
    const form = e.target
    const formData = new FormData(form)
    const body = Object.fromEntries(formData)
    body.isAdmin = !!body.isAdmin
    body.groupIds = []
    for (let key in body) {
      if (key.startsWith('group-')) {
        const gid = key.split('group-')[1]
        body.groupIds.push(gid)
        delete body[key]
      }
    }
    const url = `/api/user/${isNew ? '' : user.id}`
    fetch(url, {
      method: isNew? 'POST':'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body)
    })
    .then(checkStatus)
    .then(() => {
      router.replace(router.asPath)
      setSelectedItem({})
    })
    .catch(err => console.log('error', err))
  }

  const onOpenChange = isOpen => {
    if (!isOpen) setSelectedItem({})
  }
  
  return <Modal
    title={user.pseudo}
    form='user-form'
    open={user.id || isNew}
    onOpenChange={onOpenChange}
    okTxt={<><FontAwesomeIcon icon={faFloppyDisk}/> Sauvegarder</>}
  >
    <form onSubmit={onSubmit} id='user-form' className='mb-4'>
      <div className='grid grid-cols-2 mt-4 gap-1'>
        {isNew && <>
          <label htmlFor='id'>id*</label>
          <input id='id' name='id' type='text' required
            autoComplete='off' defaultValue={user.id} />
        </>}

        <label htmlFor='pseudo'>pseudo</label>
        <input id='pseudo' name='pseudo' type='text' required
          defaultValue={user.pseudo} autoComplete='off'/>
        
        <label htmlFor='name'>isAdmin</label>
        <span>
          <input id='isAdmin' name='isAdmin'
            type='checkbox' defaultChecked={user.isAdmin}/>
        </span>

        <label htmlFor='pseudo'>discordId</label>
        <input id='discordId' name='discordId' type='text' 
          defaultValue={user.discordId} autoComplete='off'/>
      </div>

      <div className='my-4'>
        {groups?.map(g => <div key={g.id}>
          <input id={g.id} name={'group-'+g.id} className='mr-1'
            type='checkbox' defaultChecked={user.groupIds?.includes(g.id)}/>
          <label htmlFor={g.id}>{g.name}</label>
        </div>)}
      </div>
    </form>
  </Modal>
}

export default UserModal
