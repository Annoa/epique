import dynamic from 'next/dynamic'
import { useCallback, useEffect, useRef, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit, faFileLines, faXmark } from '@fortawesome/free-solid-svg-icons'
import DeleteAdminButton from '@components/DeleteAdminAlert'
import useKeySave from '@hooks/useKeySave'

const TextField = dynamic(
  () => import('@components/TextField'), 
  { ssr:false }
)


const AdminPnj = ({
  pnj = {},
  api
}) => {
  const elemRef = useRef()
  const [editingDesc, setEditingDesc] = useState(false)
  const [desc, setDesc] = useState('')

  useEffect(() => {
    setDesc(pnj.adminDesc)
  }, [pnj.adminDesc])

  const save = useCallback((desc, cb) => {
    api({
      method: 'PUT',
      id: pnj.id,
      body: {
        adminDesc: desc
      },
      cb: cb?() => setEditingDesc(false):null
    })
  }, [api, pnj.id])

  useKeySave({
    elemRef,
    close: setEditingDesc,
    save,
    value: desc
  })

  const addSheet = () => {
    api({
      method: 'PUT',
      id: pnj.id,
      body: {
        hasSheet: true
      }
    })
  }

  const deleteSheet = deleteAllFiles => {
    api({
      method: 'PUT',
      id: pnj.id,
      body: {
        hasSheet: false,
        deleteAllFiles
      }
    })
  }

  return <div ref={elemRef}>
    

    { editingDesc ?
      <TextField
        content={desc}
        setContent={setDesc}
    />
    :
      <div 
        className={`inner-html-container ${!pnj?._htmlAdminDesc?'italic':''}`}
        dangerouslySetInnerHTML={{ 
          __html: pnj?._htmlAdminDesc || '(Aucune description)'  
        }}
      />
    }

    <div className='flex justify-end items-center'>
      {editingDesc? 
        <>
          <button className='btn mr-2' onClick={() => setEditingDesc(false)}>
            <FontAwesomeIcon icon={faXmark}/>
          </button>
          <button className='btn btn-green'
            onClick={() => save(desc, true)}>
            <FontAwesomeIcon icon={faEdit} className='mr-1'/>
            Sauvegarder
          </button>
        </>
        :
        <button className='btn' onClick={() => setEditingDesc(true)}>
          <FontAwesomeIcon icon={faEdit} className='mr-1'/>
          Editer la description
        </button>
      }
    </div>

    <hr className='my-4'/>

    <div className='flex items-center justify-between mt-4 gap-1'>
      <div>
        {pnj.hasSheet? 
          <DeleteAdminButton
            deleteAllFilesOption
            deleteFunc={deleteSheet}
            label={`Supprimer la fiche`}/>
          :
          <button className='btn btn-green' onClick={addSheet}>
            <FontAwesomeIcon icon={faFileLines} className='mr-1'/>
            Créer une fiche
          </button>
        }
      </div>
      <div>
        <DeleteAdminButton
          data='pnj'
          id={pnj.id}
          deleteAllFilesOption={pnj.hasSheet}
          label={`Supprimer le pnj`}/>
      
      </div>
    </div>

    

  </div>
}

export default AdminPnj