import Link from 'next/link'
import dynamic from 'next/dynamic'
import { useEffect, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFileLines, faFloppyDisk, faPencil, faTrashAlt, faXmark } from '@fortawesome/free-solid-svg-icons'
import TagsInput from 'react-tagsinput'

const TextField = dynamic(
  () => import('@components/TextField'), 
  { ssr:false }
)


const EditPnj = ({
  pnj = {},
  groups
}) => {
  const [tags, setTags] = useState([])

  useEffect(() => {
    setTags(pnj.tags?.split(','))
  }, [pnj])

  return <form>
    <div className='grid grid-cols-[auto_auto] mt-4 gap-1'>

      <label className='mb-6'>Fiche</label>
      <div className='text-center'>
        {pnj.hasSheet? 
          <>
            Oui
            <button className='btn btn-red ml-2'>
              <FontAwesomeIcon icon={faTrashAlt} className=' mr-1'/>
              Supprimer
            </button>
          </>
          :
          <>
            Non
            <button className='btn btn-green ml-2'>
              <FontAwesomeIcon icon={faFileLines} className='mr-1'/>
              Créer une fiche
            </button>
          </>
        }
      </div>

      <label htmlFor='id'>id*</label>
      <input autoComplete='off' disabled required
        id='id' name='id' placeholder='id'
        type='text' defaultValue={pnj.id}/>

      <label htmlFor='name'>name*</label>
      <input autoComplete='off' required
        id='name' name='name' placeholder='name'
        type='text' defaultValue={pnj.name}/>

      <label htmlFor='shortName'>shortName*</label>
      <input autoComplete='off' required
        id='shortName' name='shortName' placeholder='shortName'
        type='text' defaultValue={pnj.shortName}/>

      <label htmlFor='authGroupIds'>authGroupIds</label>
      <div>
        {groups?.map(({id}) => <label className='block' key={id}>
          <input type='checkbox' name='authGroupIds' 
            defaultChecked={pnj.authGroupIds?.includes(id)}
            className='mr-1'/>
          {id}
        </label>)}
      </div>

      <label htmlFor='shortName'>tags</label>
      <TagsInput
        className='react-tagsinput -mb-1.5'
        value={tags} 
        inputProps={{
          placeholder: 'tags',
          list: 'tags-datalist'
        }}
        onChange={setTags} />

    </div>
    <div className='mt-6 flex justify-end'>
      <button className='btn btn-green'>
        <FontAwesomeIcon icon={faFloppyDisk}/> Sauvegarder
      </button>
    </div>
  </form>
}

export default EditPnj