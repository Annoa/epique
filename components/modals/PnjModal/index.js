import { useEffect, useState } from 'react'

import { faEye, faEyeSlash, faFileLines } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Link from 'next/link'
import Modal from '../Modal'
import AdminPnj from './AdminPnj'
import ReadPnj from './ReadPnj'


const PnjModal = ({
  pnj,
  close,
  defaultGid,
  user,
  tabs,
  api
}) => {
  const onOpenChange = isOpen => !isOpen && close()
  const [gid, setGid] = useState(defaultGid)

  useEffect(() => {
    setGid(defaultGid)
  }, [defaultGid])

  return <Modal
    title={(user.isAdmin && pnj?.hasSheet)?
      <Link href={`/sheet/pnj/${pnj.id}`} className='w-full font-bold text-ink-800'>
        <FontAwesomeIcon icon={faFileLines} className='text-ink-300 mr-1'/>
        {pnj.shortName}
      </Link>
    : pnj?.shortName}
    open={!!pnj?.name}
    onOpenChange={onOpenChange}
    className={{body:'m-0'}}
  >

  {tabs && <div className='bg-ink-500/10 flex items-center cursor-pointer border-b text-center'>  
    {tabs.map(tab => <div key={tab.id}
      className={`${gid===tab.id?'text-ink-900 font-bold':''} grow p-1`}
      onClick={() => setGid(tab.id)}>
      {tab.name === 'ALL'? 'Admin' : tab.name}
      {tab.name !== 'ALL' && <FontAwesomeIcon 
        icon={pnj?.authGroupIds?.includes(tab.id)?faEye:faEyeSlash}
        className='ml-1 opacity-70'/>}
    </div>)}
  </div>}

    <div className='p-6 pt-2'>
      {gid ?
        <ReadPnj 
          pnj={pnj} 
          gid={gid} 
          api={api} 
          isAdmin={user.isAdmin}/>
      : user.isAdmin ?
        <AdminPnj 
          pnj={pnj}
          api={api}/>
      : null
      }
    </div>
  </Modal>
}

export default PnjModal