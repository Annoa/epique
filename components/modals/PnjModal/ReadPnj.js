import { useCallback, useEffect, useRef, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit, faFloppyDisk, faPencil, faXmark } from '@fortawesome/free-solid-svg-icons'
import dynamic from 'next/dynamic'
import useKeySave from '@hooks/useKeySave'

const TextField = dynamic(
  () => import('@components/TextField'), 
  { ssr:false }
)


const ReadPnj = ({
  pnj = {},
  gid,
  isAdmin,
  api
}) => {
  const cmtsRef = useRef()
  const descRef = useRef()
  const [editingCmts, setEditingCmts] = useState(false)
  const [editingDesc, setEditingDesc] = useState(false)
  const [comments, setComments] = useState('')
  const [desc, setDesc] = useState('')

  useEffect(() => {
    setComments(pnj.groupComments?.[gid])
    setDesc(pnj.groupDesc?.[gid])
  }, [pnj, gid])

  const saveComments = useCallback((comments, cb) => {
    api({
      method: 'PUT',
      id: pnj.id,
      body: {
        groupComments: {[gid]: comments}
      },
      cb: cb? () => setEditingCmts(false):null
    })
  }, [api, gid, pnj.id])

  const saveDesc = useCallback((desc, cb) => {
    api({
      method: 'PUT',
      id: pnj.id,
      body: {
        groupDesc: {[gid]: desc}
      },
      cb: cb?() => setEditingDesc(false):null
    })
  }, [api, gid, pnj.id])

  useKeySave({
    elemRef: cmtsRef,
    close: setEditingCmts,
    save: saveComments,
    value: comments
  })

  useKeySave({
    elemRef: descRef,
    close: setEditingDesc,
    save: saveDesc,
    value: desc
  })

  return <div>
    { editingDesc ?
      <div ref={descRef}>
        <TextField
          content={desc}
          setContent={setDesc}
        />
      </div>
    :
      <div 
        className={`inner-html-container ${!pnj?._htmlDesc?.[gid]?'italic':''}`}
        dangerouslySetInnerHTML={{ 
          __html: pnj?._htmlDesc?.[gid] || '(Aucune description)'  
        }}
      />
    }
    {isAdmin && <div className='flex justify-end items-center mb-4'>
      {editingDesc? 
      <>
        <button className='btn mr-2' 
          onClick={() => setEditingDesc(false)}>
          <FontAwesomeIcon icon={faXmark}/>
        </button>
        <button className='btn btn-green'
          onClick={() => saveDesc(desc, true)}>
          <FontAwesomeIcon icon={faEdit} className='mr-1'/>
          Sauvegarder
        </button>
      </>
      :
      <button className='btn' onClick={() => setEditingDesc(true)}>
        <FontAwesomeIcon icon={faEdit} className='mr-1'/>
        Editer la description
      </button>
      }
    </div>}
    <div className='card bg-white/30 m-0 p-2'>
      <div className='background-parchment-img'/>
      <h3 className='font-sans ml-0 mt-0 text-lg font-medium flex items-center'>
        Commentaires
        { editingCmts ? 
        <>
          <button className='inline-btn ml-1'
            onClick={() => saveComments(comments, true)}>
            <FontAwesomeIcon icon={faFloppyDisk}/>
          </button>
          <button className='inline-btn ml-1'
            onClick={() => setEditingCmts(false)}>
            <FontAwesomeIcon icon={faXmark}/>
          </button>
        </>
        :
        <button className='inline-btn ml-1' 
          onClick={() => setEditingCmts(true)}>
          <FontAwesomeIcon icon={faPencil}/>
        </button>
        }
      </h3>
      { editingCmts ?
        <div ref={cmtsRef}>
          <TextField
            content={comments}
            setContent={setComments}
          />
        </div>
      :
        <div 
          className='inner-html-container'
          dangerouslySetInnerHTML={{ __html: pnj?._htmlComments?.[gid] }}
        />
      }
    </div>
  </div>
}

export default ReadPnj