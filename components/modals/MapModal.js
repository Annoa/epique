import { useRouter } from 'next/router'
import { useState } from 'react'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFloppyDisk } from '@fortawesome/free-solid-svg-icons'

import Modal from './Modal'
import { checkStatus } from '@lib/helpers/fetch'

const MapModal = ({
  map = {},
  groups,
  isNew,
  setSelectedItem
}) => {
  const router = useRouter()

  const onSubmit = e => {
    e.preventDefault()
    const form = e.target
    const formData = new FormData(form)
    let body = Object.fromEntries(formData)
    body.width = parseInt(body.width) || 1000
    body.height = parseInt(body.height) || 1000
    body.authGroupIds = []
    for (let key in body) {
      if (key.startsWith('group-')) {
        const gid = key.split('group-')[1]
        body.authGroupIds.push(gid)
        delete body[key]
      }
    }
    const url = `/api/map/${isNew ? '' : map.id}`
    body = JSON.stringify(body)
    console.log('body', url, body)
    fetch(url, {
      method: isNew? 'POST':'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body
    })
    .then(checkStatus)
    .then(() => {
      router.replace(router.asPath)
      setSelectedItem({})
    })
    .catch(err => console.log('error', err))
  }

  const onOpenChange = isOpen => {
    if (!isOpen) setSelectedItem({})
  }
  
  return <Modal
    title={map.name}
    form='map-form'
    open={map.id || isNew}
    onOpenChange={onOpenChange}
    okTxt={<><FontAwesomeIcon icon={faFloppyDisk}/> Sauvegarder</>}
  >
    <form onSubmit={onSubmit} id='map-form' className='mb-4'>
      <div className='grid grid-cols-2 mt-4 gap-1'>

        <label htmlFor='id'>id*</label>
        <input id='id' name='id' type='text' required
          autoComplete='off' defaultValue={map.id} />

        <label htmlFor='pseudo'>name*</label>
        <input id='name' name='name' type='text' required
          autoComplete='off' defaultValue={map.name}/>

        <label htmlFor='bg'>bg</label>
        <input id='bg' name='bg' type='text'
          autoComplete='off' defaultValue={map.bg}/>

        <label htmlFor='width'>width (px)</label>
        <input id='width' name='width' type='number'
          autoComplete='off' defaultValue={map.width||1000}/>

        <label htmlFor='height'>height (px)</label>
        <input id='height' name='height' type='number'
          autoComplete='off' defaultValue={map.height||1000}/>
      </div>

      <div className='my-4'>
        {groups?.map(g => <div key={g.id}>
          <input id={g.id} name={'group-'+g.id} className='mr-1'
            type='checkbox' defaultChecked={map.authGroupIds?.includes(g.id)}/>
          <label htmlFor={g.id}>{g.name}</label>
        </div>)}
      </div>
    </form>
  </Modal>
}

export default MapModal
