// WIP

import Router from 'next/router'
import { useEffect, useState } from 'react'

const UnsavedChangesAlert = ({
  unsavedChanges
}) => {
  const [open, setOpen] = useState(false)
  const [confirm, setConfirm] = useState(false)
  const [url, setUrl] = useState()
  useEffect(() => {
    if (unsavedChanges) {
      const routeChangeStart = () => {
        if (!confirm) {
          Router.events.emit('routeChangeError')
          throw 'Abort route change. Please ignore this error.'
        }
      }
      Router.events.on('routeChangeStart', routeChangeStart)

      return () => {
        Router.events.off('routeChangeStart', routeChangeStart)
      }
    }
  }, [unsavedChanges])

}

export default UnsavedChangesAlert