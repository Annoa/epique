import * as Primitives from '@radix-ui/react-tabs';
import styles from './tabs.module.css'

const Tabs = ({
  triggers,
  tabs,
  orientation
}) => (
  <Primitives.Root className={styles['TabsRoot']} defaultValue="tab1">
    <Primitives.List className={styles['TabsList']} aria-label={"coucou"}>
      <Primitives.Trigger className={styles['TabsTrigger']} value="tab1">
        Account
      </Primitives.Trigger>
      <Primitives.Trigger className={styles['TabsTrigger']} value="tab2">
        Password
      </Primitives.Trigger>
    </Primitives.List>
    <Primitives.Content className={styles['TabsContent']} value="tab1">
      <p>tab1</p>
      <p>Make changes to your account here.</p>
      <p>Coucou</p>
      <p>Make changes to your account here.</p>
      <p>Coucou</p>
      <p>Make changes to your account here.</p>
      <p>Coucou</p>
    </Primitives.Content>
    <Primitives.Content className={styles['TabsContent']} value="tab2">
      <p>tab2</p>
      <p>Make changes to your account here.</p>
      <p>Coucou</p>
      <p>Make changes to your account here.</p>
      <p>Coucou</p>
      <p>Make changes to your account here.</p>
      <p>Coucou</p>
    </Primitives.Content>
  </Primitives.Root>
)

export default Tabs
