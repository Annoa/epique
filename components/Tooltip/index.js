import React from 'react'
import * as RadixTooltip from '@radix-ui/react-tooltip'
import styles from './tooltip.module.css'

const Tooltip = ({
  content,
  children
}) => {
  return (
    <RadixTooltip.Provider>
      <RadixTooltip.Root>

        <RadixTooltip.Trigger asChild>
          <span>
          { children }
          </span>
        </RadixTooltip.Trigger>

        <RadixTooltip.Portal>
          <RadixTooltip.Content className={styles.TooltipContent} sideOffset={5}>
            {content}
            <RadixTooltip.Arrow className={styles.TooltipArrow} />
          </RadixTooltip.Content>
        </RadixTooltip.Portal>

      </RadixTooltip.Root>
    </RadixTooltip.Provider>
  )
}

export default Tooltip
