import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { SAVING, SAVED, ERROR } from '@constants/app'
import { faCheck, faExclamation, faHourglass } from '@fortawesome/free-solid-svg-icons'

const SavingStatus = ({
  status
}) => {
  let icon, color
  if (status === SAVING) {
    icon = faHourglass
    color = 'gray'
  } else if (status === SAVED) {
    icon = faCheck
    color = 'green'
  } else if (status === ERROR) {
    icon = faExclamation
    color = 'red'
  } else {
    return null
  }
  return <FontAwesomeIcon 
    className={`mx-1 ${status===SAVING?'animate-spin':''}`}
    icon={icon} 
    style={{color}}
  />
}

export default SavingStatus