/* eslint-disable react/no-unescaped-entities */
import { forwardRef } from 'react'
import * as RadixAccordion from '@radix-ui/react-accordion'
import styles from './accordion.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronDown } from '@fortawesome/free-solid-svg-icons'

const Accordion = ({
  children,
  ...props
}) => (
  <RadixAccordion.Root 
    className={styles.AccordionRoot} 
    type='multiple'
    {...props}
    >
    { children }
  </RadixAccordion.Root>
)

const AccordionTrigger = forwardRef(({ 
  children, className, disabled, ...props 
}, forwardedRef) => (
  <RadixAccordion.Header className={styles.AccordionHeader}>
    <RadixAccordion.Trigger
      className={`${styles.AccordionTrigger} ${className?className:''}`}
      {...props}
      ref={forwardedRef}
    >
      {children}
      <FontAwesomeIcon 
        icon={faChevronDown} 
        className={`${styles.AccordionChevron} ${disabled?'invisible':''}`}
        aria-hidden />
    </RadixAccordion.Trigger>
  </RadixAccordion.Header>
))

AccordionTrigger.displayName = 'AccordionTrigger'

const AccordionContent = forwardRef(({ children, className, ...props }, forwardedRef) => (
  <RadixAccordion.Content
    className={`${styles.AccordionContent} ${className?className:''}`}
    {...props}
    ref={forwardedRef}
  >
    <div className={styles.AccordionContentText}>{children}</div>
  </RadixAccordion.Content>
))

AccordionContent.displayName = 'AccordionContent'

export const AccordionItem = ({
  title, 
  value, 
  children, 
  disabled,
  className = {}
}) => (
  <RadixAccordion.Item 
    disabled={disabled}
    className={`${styles.AccordionItem}`} 
    value={value||title}>
    <AccordionTrigger disabled={disabled} className={className.trigger}>
      {title}
    </AccordionTrigger>
    <AccordionContent className={className.content}>
      {children}
    </AccordionContent>
  </RadixAccordion.Item>
)
export default Accordion
