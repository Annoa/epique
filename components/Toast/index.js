import { faCircleInfo, faSquareCheck, faWarning } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import * as ToastPrimitive from '@radix-ui/react-toast'
import styles from './toast.module.css'


export const ToastViewport = () => (
  <ToastPrimitive.Viewport className={styles['ToastViewport']} />
)

export const Toast = ({ 
  title, 
  content = 'Un contenu', 
  open,
  setOpen,
  cta,
  alert,
  success
}) => <ToastPrimitive.Root className={`
   ${styles['ToastRoot']} ${alert?styles['ToastAlert']:''} ${success?styles['ToastSuccess']:''}
  `} open={open} onOpenChange={setOpen}>
  <div className='grow'>
    {title && <ToastPrimitive.Title className={styles['ToastTitle']}>
      <FontAwesomeIcon className='mr-1'
        icon={alert?faWarning:success?faSquareCheck:faCircleInfo}/>
      {title}
    </ToastPrimitive.Title>}
    <ToastPrimitive.Description>
      {content}
    </ToastPrimitive.Description>
  </div>
  {cta && (
    <ToastPrimitive.Action 
    className={styles['ToastAction']} asChild>
      {cta}
    </ToastPrimitive.Action>
  )}
  {/* <ToastPrimitive.Close 
    className={styles['ToastClose']} 
    aria-label='Close'>
    <span aria-hidden>×</span>
  </ToastPrimitive.Close> */}
</ToastPrimitive.Root>

export default Toast