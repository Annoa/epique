import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import list from './list'

const Icon = ({ name, fallback, ...props }) => {
  const icon = list[name] || list[fallback] || list.unknown
  return <FontAwesomeIcon icon={icon} {...props}/>
}

export default Icon