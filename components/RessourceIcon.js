import Endurance from '@public/images/symboles/endurance.svg'
import Mana from '@public/images/symboles/mana.svg'
import Volonte from '@public/images/symboles/volonte.svg'
import Ichor from '@public/images/symboles/ichor.svg'
import Extia from '@public/images/symboles/extia.svg'
import Vitae from '@public/images/symboles/vitae.svg'
import Souffle from '@public/images/symboles/souffle.svg'
import Transcendance from '@public/images/symboles/transcendance.svg'
import Essence from '@public/images/symboles/essence.svg'
import Neant from '@public/images/symboles/neant.svg'
import Chaos from '@public/images/symboles/chaos.svg'
import Aether from '@public/images/symboles/aether.svg'
import Gnose from '@public/images/symboles/gnose.svg'
import Geis from '@public/images/symboles/geis.svg'
import Tooltip from './Tooltip'
import { RESSOURCES } from '@constants/epique'

const Symboles = {
  'endurance': Endurance,
  'mana': Mana,
  'volonte': Volonte,
  'ichor': Ichor,
  'extia': Extia,
  'vitae': Vitae,
  'souffle': Souffle,
  'transcendance': Transcendance,
  'essence': Essence,
  'neant': Neant,
  'chaos': Chaos,
  'aether': Aether,
  'gnose': Gnose,
  'geis': Geis,
}

const RessourceIcon = ({
  name,
  size, 
  className, 
  fallback
}) => {
  const size_ = size || '1em'
  const Symbole = Symboles[name]
  if (!Symbole) return fallback? RESSOURCES[name]:null
  const title = RESSOURCES[name]
  return <Tooltip content={title}>
    <Symbole 
      className={`inline-block align-baseline
      ${className?className:''}`}
      height={size_} width={size_}
      fill='currentColor'
      shapeRendering='geometricPrecision'
      // stroke='currentColor'
      // strokeWidth={3}
  />
    </Tooltip>

}

export default RessourceIcon