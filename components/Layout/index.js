import { useReducer, useState } from 'react'
import Head from 'next/head'
import { InfoModalProvider } from '@contexts/infoModalCtx'

import Nav from './Nav'
import Sidebar from './Sidebar'
import LayoutTabs from './LayoutTabs'
import ScrollButton from './ScrollButon'

const APP_TITLE = 'Épique'

const Layout = ({ 
  children,
  title,
  className,
  sidebar,
  tabsProps,
  ...props 
}) => {
  const [sidebarOpen, toggleSidebarOpen] = useReducer(prev => !prev, false)
  const [mainRef, setMainRef] = useState()
  
  
  return <InfoModalProvider authData={props}>

    <Head>
      <title>{title || APP_TITLE}</title>
    </Head>

    <header className={`fixed w-full z-[99] ${
      sidebar?'lg:pr-[300px]':''
      }`}>
      <Nav {...props} />
      {tabsProps? <LayoutTabs {...tabsProps}/> : null}
    </header>

      <div className='h-screen w-screen overflow-hidden flex'>

        <main ref={setMainRef}
          className={`grow h-full overflow-x-hidden overflow-y-auto
          ${className? className : 'p-2 pb-4'}
          ${tabsProps? 'pt-[70px]' : 'pt-[40px]'}
          ${sidebar? 'mr-[-300px] lg:mr-0' : ''}
        `}>          
          { children }
          <ScrollButton container={mainRef}/>
        </main>

      {sidebar && 
        <Sidebar 
          className={`w-[300px] z-[100] shrink-0 ${
            sidebarOpen?'':'translate-x-full lg:translate-x-0'
          }`}
          toggleIsOpen={toggleSidebarOpen}
          isOpen={sidebarOpen}
        >
          {sidebar}
        </Sidebar>
      }
    </div>
    
  </InfoModalProvider>
}

export default Layout