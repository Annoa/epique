import { useEffect, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowUp } from '@fortawesome/free-solid-svg-icons'

const ScrollButton = ({
  container
}) => {
  const [isHidden, setIsHidden] = useState(true)

  const onClick = () => {
    const elem = container || window
    elem.scrollTo({ top: 0, behavior: 'smooth' })
  }

  useEffect(() => {
    const elem = container || window
    const onScroll = e => {
      setIsHidden(elem.scrollTop < 500)
    }
    elem.addEventListener('scroll', onScroll)
    return () => {
      elem.removeEventListener('scroll', onScroll)
    } 
  }, [setIsHidden, container])

  return (
    <div className={`scrollbutton fixed btn rounded-full shadow-lg
      right-4 bottom-4 z-[101] w-10 h-10 flex items-center justify-center
      cursor-pointer 
      ${isHidden?'hidden':''}
    `} onClick={onClick}>
      <FontAwesomeIcon icon={faArrowUp} size='2x'/>
    </div>
  )
}

export default ScrollButton