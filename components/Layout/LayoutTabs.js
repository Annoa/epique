import Link from 'next/link'
import { useRouter } from 'next/router'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const getTab = (url, rootPath) => {
  let tab = url.split('#')[0]
  tab = tab.split('?')[0]
  tab = tab.replace(rootPath, '')
  return tab
}

const Tabs = ({
  rootPath, 
  tabs // [{icon, path, name}]
}) => {
  const router = useRouter()

  let currentTab = getTab(router.asPath, rootPath)

  return <div className='flex justify-center mt-1'>
  {tabs.map(({path, icon, name}) => (
    <Link 
      key={path}
      className={`btn m-1 ${currentTab === path? 'bg-ink-300':''} `} 
      href={`${rootPath}${path}`}
      >
      {icon && <FontAwesomeIcon icon={icon}/>}
      <span className={`${icon?'hidden pl-1':''} md:inline`}>{name}</span>
    </Link>
  ))}
</div>
}

export default Tabs