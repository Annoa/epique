import Link from 'next/link'

import * as NavigationMenu from '@radix-ui/react-navigation-menu'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import styles from './nav.module.css'
import { faEdit } from '@fortawesome/free-solid-svg-icons'


const ListMenu = ({
  icon,
  title,
  items,
  rootPath,
  adminEdit
}) => items?.length > 0 &&
<NavigationMenu.Item>
  <NavigationMenu.Trigger className={styles.NavigationMenuTrigger}>
    <Link href={rootPath}>
      <FontAwesomeIcon icon={icon} />
      <span className='hidden md:inline pl-1'>{title}</span>
    </Link>
  </NavigationMenu.Trigger>
  <NavigationMenu.Content className={styles.NavigationMenuContent}>
    <ul className={styles.List}>
      {items.map(e => <li key={e.id} className="flex">
          <Link 
            href={rootPath+'/'+e.id} 
            className={`${styles.ListItemLink}`}>
            {e.name}
          </Link>
          {adminEdit && <Link 
            href={rootPath+'/'+e.id+'/edit'} 
            className={`${styles.ListItemLink} grow-0`}>
            <FontAwesomeIcon icon={faEdit} />
          </Link>}
        </li>
      )}
    </ul>
  </NavigationMenu.Content>
</NavigationMenu.Item>

export default ListMenu