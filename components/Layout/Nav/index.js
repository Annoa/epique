import Link from 'next/link'
import * as NavigationMenu from '@radix-ui/react-navigation-menu'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { 
  faScrewdriverWrench, faIdBadge, faBook, faUser,
  faCompass, faMasksTheater, faQuestion, faFeather,
} from '@fortawesome/free-solid-svg-icons'
import { faWikipediaW } from '@fortawesome/free-brands-svg-icons'

import { RULES } from '@constants/routes'
import styles from './nav.module.css'
import ListMenu from './ListMenu'

const rules = Object.keys(RULES).map(e => ({id:e, name:RULES[e]}))

const Nav = ({ user, pjs, maps, pnjs, groups }) => {

  // const defaultSheetId = !user? null : pjs.find(s => s.userId === user.id)?.id
  const journals = !user? [] 
    : user.isAdmin ? groups
    : user.groupIds.map(id => ({id, name:groups.find(g => g.id === id)?.name}))

  return <NavigationMenu.Root className={styles.NavigationMenuRoot}>
      <NavigationMenu.List className={styles.NavigationMenuList}>
        <div className='background-parchment-img rounded-md'/>

        <NavigationMenu.Item>
          <Link className={styles.NavigationMenuLink} href={user?'/profile':'/signin'}>
            <FontAwesomeIcon icon={faUser} />
            <span className='hidden md:inline pl-1'>
              {user?.pseudo || 'Connexion'}
            </span>
          </Link>
        </NavigationMenu.Item>
        

        { user?.isAdmin &&
          <NavigationMenu.Item className='hidden md:inline'>
            <Link className={styles.NavigationMenuLink} href='/admin'>
              <FontAwesomeIcon icon={faScrewdriverWrench} />
              <span className='pl-1'>Administration</span>
            </Link>
          </NavigationMenu.Item>
        }

        {pjs?.length > 1 ?
          <ListMenu
            icon={faIdBadge}
            title='Fiches'
            items={pjs}
            rootPath='/sheet/pj'
          />
        : pjs?.length === 1 ?
          <NavigationMenu.Item>
            <Link className={styles.NavigationMenuLink} href={'/sheet/'+pjs[0].id}>
              <FontAwesomeIcon icon={faIdBadge} />
              <span className='hidden md:inline pl-1'>Fiche</span>
            </Link>
          </NavigationMenu.Item>
        :
        null}
  
        {/* {journals?.length > 1 ?
          <ListMenu
            icon={faFeather}
            title='Campagnes'
            items={journals}
            rootPath='/journal'
          />
        : journals?.length === 1 ?
          <NavigationMenu.Item>
            <Link className={styles.NavigationMenuLink} href={'/journal/'+journals[0].id}>
              <FontAwesomeIcon icon={faFeather} /> 
              <span className='hidden md:inline pl-1'>Campagne</span>
            </Link>
          </NavigationMenu.Item>
        :
        null} */}

        <ListMenu
          icon={faCompass}
          title='Cartes'
          items={maps}
          rootPath='/map'
          adminEdit={user?.isAdmin}
        />

        {pnjs?.length > 0 && <NavigationMenu.Item>
          <Link className={styles.NavigationMenuLink} href='/pnjs'>
            <FontAwesomeIcon icon={faMasksTheater} /> 
            <span className='hidden md:inline pl-1'>Pnjs</span>
          </Link>
        </NavigationMenu.Item>}

        <ListMenu
          icon={faBook}
          title='Règles'
          items={rules}
          rootPath='/rules'
        />

        {/* <NavigationMenu.Item>
          <Link className={styles.NavigationMenuLink} href='/help'>
            <FontAwesomeIcon icon={faQuestion} /> 
          </Link>
        </NavigationMenu.Item> */}

        <NavigationMenu.Item>
          <a 
            className={styles.NavigationMenuLink} href='http://wiki-epique.fr/Home'
            target="_blank" rel="noopener noreferrer"
          >
            <FontAwesomeIcon icon={faWikipediaW} /> 
          </a>
        </NavigationMenu.Item>
      
      <NavigationMenu.Indicator className={styles.NavigationMenuIndicator}>
        <div className={styles.Arrow} />
      </NavigationMenu.Indicator>

    </NavigationMenu.List>

    <div className={styles.ViewportPosition}>
      <NavigationMenu.Viewport className={styles.NavigationMenuViewport}/>
    </div>
  </NavigationMenu.Root>
}

export default Nav