import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft, faArrowRight} from '@fortawesome/free-solid-svg-icons'

import styles from './sidebar.module.css'

const Sidebar = ({
  isOpen,
  toggleIsOpen,
  className, 
  children
}) => {
  return <aside className={`
  drop-shadow-2xl transition-transform relative
   ${className?className:''}
  `}>
    <div className='background-parchment-img'/>
    <button 
      className={`btn ${styles.sidebarButton}`}
      onClick={toggleIsOpen}>
      <FontAwesomeIcon 
        icon={isOpen?faArrowRight:faArrowLeft}
        size='2x'
      />
    </button>
    <div className={styles.sidebar}>
      { children }
    </div>
  </aside>
}

export default Sidebar