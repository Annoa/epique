export const Checkbox = ({
  id,
  onChange,
  label,
  name,
  defaultChecked,
  className,
  disabled,
  checked
}) => <div className={`whitespace-nowrap ${className||''}`}>
  <input 
    id={id} 
    name={name||id} 
    type="checkbox"
    defaultChecked={defaultChecked}
    onChange={onChange}
    disabled={disabled}
    checked={checked}
  />
  <label htmlFor={id}> {label} </label>
</div>

export const NumberInput = ({
  id,
  onChange,
  label,
  name,
  defaultValue,
  className,
}) => <div className={`whitespace-nowrap ${className||''}`}>
  <label htmlFor={id}> {label} </label>
  <input 
    id={id} 
    name={name||id} 
    type="number"
    className="w-16"
    onChange={onChange}
    defaultValue={defaultValue}
  />
</div>

export const Select = ({
  id,
  label,
  onChange,
  options,
  value,
  className
}) => <div className={`whitespace-nowrap ${className||''}`}>
  {label && <label htmlFor={id}> {label} </label>}
  <select value={value} name={id} id={id} onChange={onChange}>
    {options?.map(({value, label}) => <option key={value} value={value}>{label}</option>)}
  </select>
</div>

export const Radio = ({
  name,
  options,
  onChange,
  className,
}) => <div className={className} onChange={onChange}>
  {options?.map(({value, label}) => <label key={value} className='whitespace-nowrap'>
    <input type='radio' value={value} name={name}/> {label}
  </label>)}
</div>