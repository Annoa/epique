import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import styles from './datacard.module.css'
import { faStar } from '@fortawesome/free-solid-svg-icons'
import { 
  faStar as faEmptyStar 
} from '@fortawesome/free-regular-svg-icons'
import Tags from './Tags'

const DataCard = ({
  name,
  desc,
  id,
  branchs,
  onClick,
  isFavorite,
  addFavorite,
  removeFavorite,
  favorite,
  clipboard,
  highlighted,
  ...props
}) => {
  

  return <article className={`card 
    ${highlighted?'border-2 border-yellow-500':''}`}>
    <h2 id={id}>
      {name}
      {favorite && 
        <FontAwesomeIcon
          size='sm'
          className='cursor-pointer ml-1'
          onClick={() => isFavorite? removeFavorite({id, name}):addFavorite({id, name})}
          icon={isFavorite? faStar : faEmptyStar} 
        />
      }
    </h2>
    <Tags {...props}/>
    <div className={`inner-html-container p-2 ${styles.desc}`}
      dangerouslySetInnerHTML={{ __html: desc }}
    />

    { branchs?.length > 0 &&
      // <div className={`capacity-branches grid-${branchs?.length%2?'3':'2'}`}>
      <div className='cards-container'>
        {branchs.sort((a,b) => a.name > b.name).map(b => (
          <div key={b.name} className='card bg-parchment-500/40'>
            <h3>
              {b.name}
            </h3>
            <Tags {...b}/>
            <div className={`inner-html-container p-2 ${styles.desc}`}
              dangerouslySetInnerHTML={{ __html: b.desc }}
            />
          </div>)
        )}
      </div>
    }
  </article>
}

export default DataCard