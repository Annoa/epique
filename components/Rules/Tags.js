import { RESSOURCES } from '@constants/epique'

const BG = {
  'Endurance': 'rgb(81, 165, 197)',
  'Importante': 'rgb(255, 210, 77)',
  'Volonté': 'rgb(209, 197, 219)',
  'Modérée': 'rgb(180, 132, 132)',
  'Légère': 'rgb(182, 207, 174)',
  'Minimale': 'rgb(145, 161, 178)',
  'Mana': 'rgb(255, 170, 128)',
  'Élevé': 'rgb(255, 210, 77)',
  'Matériaux': 'rgb(182, 207, 174)',
  'R': 'rgb(145, 161, 178)',
  'C': 'rgb(145, 161, 178)',
  'U': 'rgb(145, 161, 178)',
  'Q': 'rgb(145, 161, 178)',
  'Foci': 'rgb(180, 132, 132)',
  'E': 'rgb(145, 161, 178)',
  'Ingrédient': 'rgb(182, 207, 174)',
  'Arme': 'rgb(182, 207, 174)',
  'Maniement:2': 'rgb(201, 173, 106)',
  'Maniement:1': 'rgb(201, 173, 106)',
  'Périapte': 'rgb(255, 210, 77)',
  'Armure': 'rgb(182, 207, 174)',
  'Bourdon': 'rgb(182, 207, 174)',
  'Bijoux': 'rgb(182, 207, 174)',
  'Consommable': 'rgb(182, 207, 174)',
  'Charme': 'rgb(255, 153, 255)',
  'Implant': 'rgb(182, 207, 174)',
  'Divers': 'rgb(182, 207, 174)',
  'Cumulatif': 'rgb(182, 207, 174)',
  'Factorisable': 'rgb(180, 132, 132)',
  'JdS|Endurance|': 'rgb(201, 173, 106)',
  'JdS+|Endurance,Volonté|': 'rgb(201, 173, 106)',
  'Instantané': 'rgb(102, 204, 255)',
  'JdS|Endurance,Volonté|': 'rgb(201, 173, 106)',
  'JdS?|Endurance,Mana|': 'rgb(201, 173, 106)',
  'JdS-|Endurance|': 'rgb(201, 173, 106)',
  'JdS+|Mana|': 'rgb(201, 173, 106)',
  'JdS+|Mana,Volonté|': 'rgb(201, 173, 106)',
  'JdS|Volonté|': 'rgb(201, 173, 106)',
  'JdS?|Volonté|': 'rgb(201, 173, 106)',
  'JdS+|Volonté|': 'rgb(201, 173, 106)',
  'Mana:1': 'rgb(255, 170, 128)',
  'Volonté:1': 'rgb(209, 197, 219)',
  'Endurance:1': 'rgb(81, 165, 197)',
  'JdS?|Endurance,Volonté|': 'rgb(201, 173, 106)',
  'Mana:2': 'rgb(255, 170, 128)',
  'Volonté:2': 'rgb(209, 197, 219)',
  'M': 'rgb(145, 161, 178)',
  'Endurance:2': 'rgb(81, 165, 197)',
  'JdS-|Volonté|': 'rgb(201, 173, 106)',
  'JdS+|Endurance,Mana|': 'rgb(201, 173, 106)'
}

const defaultColor = 'rgb(176, 176, 176)'

const Tag = ({
  label
}) => (
  <div className='badge ml-1 mb-1'
    style={{backgroundColor:BG[label]||defaultColor}}>
    {label}
  </div>
)

// todo : ressources (from constant)

/* 

  Endurance,
  Mana,
  Volonté,

  {typeof Endurance !== 'undefined' && <Tag label={
      `Endurance${typeof Endurance === 'number' && Endurance > 0?':'+Endurance:''}`
    }/>}
    {typeof Mana !== 'undefined' && <Tag label={
      `Mana${typeof Mana === 'number' && Mana > 0?':'+Mana:''}`
    }/>}
    {typeof Volonté !== 'undefined' && <Tag label={
      `Volonté${typeof Volonté === 'number' && Volonté > 0?':'+Volonté:''}`
    }/>}

*/

const RESSOURCES_NAMES = Object.values(RESSOURCES)

const Tags = ({
  generique,
  rarete,
  factorisable,
  cumulative,
  instant,
  tags,
  charme,
  periapte,
  artefact,
  foci,
  maniement,
  type,
  depense, 
  cost,
  ...props
}) => {
  return <>
    {generique && <Tag label='Générique'/>}
    {rarete && <Tag label={rarete}/>}
    {Object.keys(props).filter(p => RESSOURCES_NAMES.includes(p))
      .map(ressource => <Tag key={ressource} label={
      `${ressource}${typeof props[ressource] === 'number'?(':'+props[ressource]):''}`
    }/>)}
    {factorisable && <Tag label='Factorisable'/>}
    {instant && <Tag label='Instantané'/>}
    {cumulative && <Tag label='Cumulatif'/>}
    {charme && <Tag label='Charme'/>}
    {periapte && <Tag label='Périapte'/>}
    {artefact && <Tag label='Artefact'/>}
    {foci && <Tag label='Foci'/>}
    {typeof maniement !== 'undefined' && <Tag label={
      `Maniement${typeof maniement === 'number' && maniement > 0?':'+maniement:''}`
    }/>}
    {depense && <Tag label={depense}/>}
    {/* Laisser ces deux à la fin */}
    {tags?.length > 0 && 
      tags.map(tag => <Tag key={tag} label={tag}/>)
    }
    {type && <Tag label={type}/>}
    {cost && <Tag label={cost}/>}
  </>
}

export default Tags