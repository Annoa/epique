import { faArrowsRotate, faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import GenericDataFilter from './GenericDataFilter'
import RootsDataFilter from './RootsDataFilter'

const DataFilter = props => {
  const { onChange, dataSet, isAdmin } = props

  const onRevalidateClick = () => {
    fetch(`/api/revalidate/${dataSet}`)
    .then(res => res.json())
    .then(res => console.log('revalidate', res))
    .catch(err => console.log('error', err))
  }

  return <section className='bg-ink-500/20 p-2'>
    <div className='flex'>
      <div className='relative grow'>
        <input 
          className='w-full h-6'
          type='text' 
          name='name'
          onChange={onChange} 
          autoComplete='off'/>
        <FontAwesomeIcon className='absolute top-1/2 -translate-y-1/2 right-2' 
          icon={faMagnifyingGlass}/>
      </div>
      {isAdmin && <div className='ml-2 flex-grow-0'>
        <button className='btn' onClick={onRevalidateClick}>
          <FontAwesomeIcon icon={faArrowsRotate}/>
        </button>
      </div>}
    </div>
    {dataSet === 'capacities' ?
      <RootsDataFilter {...props}/>
    :
      <GenericDataFilter {...props}/>
    }
  </section>
}

export default DataFilter