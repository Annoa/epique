
import { Checkbox } from '@components/Fields'
import { EQUIPMENT_TYPES, PRIMARY_RESSOURCES, RESSOURCES } from '@constants/epique'
import { faStar } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

// todo: constant epic
const RESSOURCES_ADV = [ 
  'Ichor', 
  'Extia', 
  'Vitae', 
  'Aether', 
  'Chaos', 
  'Néant', 
  'Geis', 
  'Gnose', 
  'Essence',
  'Transcendance', 
]



const RootsDataFilter = ({
  rules,
  onChange
}) => {
  
  return <div className='flex justify-evenly flex-wrap items-start'>
  
  <div className='flex flex-col mt-1'>
    <fieldset className=''>
      <legend>Ressources</legend>
      {PRIMARY_RESSOURCES.map(ressource => <Checkbox
        name='ressource' 
        key={ressource}
        id={RESSOURCES[ressource]} 
        label={RESSOURCES[ressource]} 
        onChange={onChange}/>
      )}
    </fieldset>
    <Checkbox id='single' label={<i>Simple</i>} onChange={onChange}/>
    <Checkbox id='compose' label={<i>Composé</i>} onChange={onChange}/>
  </div>

  <fieldset className='grid grid-cols-[auto_auto] gap-x-2 mt-1'>
    <legend>Ressources avancées</legend>
    {RESSOURCES_ADV.map(ressource => <Checkbox
      name='ressource' 
      key={ressource}
      id={ressource} 
      label={ressource} 
      onChange={onChange}/>
    )}
  </fieldset>

  <fieldset className='mt-1'>
    <legend>Rareté</legend>
    {['C', 'U', 'R', 'M'].map(e => <Checkbox
      name='rarete' 
      key={e}
      id={e} 
      label={e} 
      onChange={onChange}/>
    )}
  </fieldset>

  <div className='grid grid-cols-[auto_auto] gap-x-2 mt-1'>
    <Checkbox id='generic' label='Branche générique' onChange={onChange}/>
    <Checkbox id='instant' label='Instantané' onChange={onChange}/>
    <Checkbox id='jds' label='Jet de sauvegarde' onChange={onChange}/>
    <Checkbox id='cumulative' label='Cumulatif' onChange={onChange}/>
    <Checkbox id='factorisable' label='Factorisable' onChange={onChange}/>
    <Checkbox 
      id='isFavorite' 
      label={<FontAwesomeIcon icon={faStar} />} 
      onChange={onChange}/>
  </div>

</div>
}

export default RootsDataFilter