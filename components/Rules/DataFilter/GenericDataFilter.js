import { Checkbox } from '@components/Fields'
import { FAV_RULES } from '@constants/app'
import { EQUIPMENT_TYPES, RESSOURCES } from '@constants/epique'
import { faStar } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const GenericDataFilter = ({
  dataSet,
  rules,
  onChange
}) => {
  const { 
    types, tiers, depenses, ressources, 
    equipmentType, rarete
  } = rules
  
  return <div className='flex justify-evenly flex-wrap items-start'>

    { ressources && 
      <fieldset className='grid grid-cols-1 mt-1'>
        <legend>Ressources</legend>
        {ressources.map(ressource => <Checkbox
          name='ressource' 
          key={ressource}
          id={ressource} 
          label={RESSOURCES[ressource]} 
          onChange={onChange}/>
        )}
      </fieldset>
    }

    { rarete && 
      <fieldset className='grid grid-cols-2 gap-x-2 mt-1'>
        <legend>Rareté</legend>
        {rarete.map(r => <Checkbox
          name='rarete' 
          key={r}
          id={r} 
          label={r} 
          onChange={onChange}/>
        )}
      </fieldset>
    }

    {equipmentType && 
      <fieldset className='grid grid-cols-1 mt-1'>
        {equipmentType.map(type => <Checkbox
          name='equipmentType' 
          key={type}
          id={type} 
          label={EQUIPMENT_TYPES[type]} 
          onChange={onChange}/>
        )}
      </fieldset>
    }

    {tiers && 
      <fieldset className='grid grid-cols-2 mt-1'>
        <legend>Tier</legend>
        {tiers.map(type => <Checkbox
          name='tier' 
          key={type}
          id={type} 
          label={type} 
          onChange={onChange}/>
        )}
      </fieldset>
    }

    { depenses && 
      <fieldset className='grid grid-cols-1 gap-x-2 mt-1'>
        <legend>Dépenses</legend>
        {depenses.map(depense => <Checkbox
          name='depense' 
          key={depense}
          id={depense} 
          label={depense} 
          onChange={onChange}/>
        )}
      </fieldset>
    }

    {types && 
    <div className='whitespace-nowrap mt-1'>
      <label htmlFor='type'>Type</label>
      <select className='inline-block ml-1' name='type' id='type' 
        onChange={onChange}>
        <option value=''>--</option>
        {types?.map(type => <option 
          key={type} value={type}>
            { type }
          </option>
        )}
      </select>
    </div>
    }

    {FAV_RULES.includes(dataSet) && <Checkbox 
      id='isFavorite' 
      label={<FontAwesomeIcon icon={faStar} />} 
      onChange={onChange}/>
    }
</div>
}

export default GenericDataFilter