- icone volonté !
- tableau edition de ressource responsive
- aura, indicateur NER
- pnj => juste name, et tu peux mettre "machin dit le truc ?"
- update Sheet : https://nextjs.org/docs/pages/building-your-application/routing/api-routes#catch-all-api-routes ?
- rules: signaler les données comme vieilles

Améliorations
- pnj => aura, public ou non
* Mettre les branches génériques à part.
* Déplacer le bouton favori

Bug:
* Édition des PNJS : le contenu de la modale ne se mets pas à jour
* Implémenter l’affichage des aura pour les PNJ qui ont des fiches de PNJ
- Options : 
  -> pb affichage mobile => table/2 columns ? prob d'affichage chez Tristan

Fiche
  - ajouter un bouton de MAJ temp. dans entretien/récupération
    -> mettre en rouge si ressource au delas de min/max ?

- navigation
  - hover: animation icone ?
- wiki
  - iframe ?
  - #wiki-{url}
- api aws = test me
- bot discord => NOT

.frame.selected (ma fiche)

- serveur : faire un dossier api avec le check des droits qui utilise
  /serveur/api: appelle serveur/data après check de droits 
  /serveur/data


https://nextjs.org/docs/advanced-features/middleware
-> getUserCookie
-> bot https://github.com/jzxhuang/nextjs-discord-bot/blob/main/middlewares/discord-interaction.ts

https://github.com/jackmerrill/nextjs-discord-slash-commands